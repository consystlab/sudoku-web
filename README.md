# Sudoku

An interactive solver of 9x9 Sudoku puzzles. Provides two constraint
models, binary and non-binary, five (5) consistency algorithms for
each model, as well as an exhaustive backtrack-search procedure with
two types of lookahead for each model.  Instances can be entered
manually or from pictures.

Presented online at https://sudoku.unl.edu For more information, read
"Solving Sudoku with Consistency: A Visual and Interactive Approach,"
Ian Howell, Robert Woodward, Berthe Y. Choueiry, Christian Bessiere,
IJCAI 2019, pages 5829--5831.
http://consystlab.unl.edu/Documents/Papers/IH-IJCAI18.pdf

## Development

This project is built on Node v8.X. Two global packages are required;
install them with:

    npm install -g webpack webpack-dev-server

Then, complete installation of the rest of the dependencies with:

    npm install

To run the development server run `npm start`. The development server
will open on http://localhost: 8080 and will automatically recompile the source
code whenever a file is saved.

This app is written using the React-Redux framework. There are many
good ways to get started with this paradigm, however, I would
recommend Codecademy's tutorials, starting with [React
101](https://www.codecademy.com/learn/react-101) and then progressing
to [React 102](https://www.codecademy.com/learn/react-102). While
React can do quite a lot on its own, it is much better to use Redux
and ImmutableJS to handle the application state. More info can be
found on the integration and usage of these libraries
[here](https://redux.js.org/docs/recipes/UsingImmutableJS.html)


### Tools

* Eslint - A linting mechanism to keep a consistent style across all JavaScript files.
* Webpack - A JavaScript compiler
* WebpackDevServer - A simple server that watches for changes in source files and recompiles source live.

### Notes

For algorithmic work:

1. Strongly Connected Components: https://en.wikipedia.org/wiki/Tarjan%27s_strongly_connected_components_algorithm
2. Maximum Matching: https://en.wikipedia.org/wiki/Hopcroft%E2%80%93Karp_algorithm#Pseudocode

## Deployment

To deploy to the webserver, run `./deploy.sh` in the root directory. You
will need the sudoku username and password to do so. To change what is
uploaded, modify the directories and files within that shell script.
