import Action from './Action';

class RemoveAction extends Action {
  constructor(method, // : Propagations
    eliminated, // : sudoku-solver/models/RemoveValues
  ) {
    super();
    this.method = method;
    this.eliminated = eliminated;
  }

  perform(state) {
    this.eliminated.forEach((variable, removed) => {
      removed.forEach(removedValue => {
        variable.domain.remove(removedValue);
      });
    });
    return state;
  }

  undo(state) {
    this.eliminated.forEach((variable, removed) => {
      removed.forEach(removedValue => {
        variable.domain.restore(removedValue);
      });
    });
    return state;
  }
}

export default RemoveAction;
