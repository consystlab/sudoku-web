import Immutable from 'immutable';

import CellStates from 'enumerations/CellStates';
import Action from 'models/Action';

class InconsistentAction extends Action {
  constructor(variables) {
    super();
    this.variables = variables;
    this.firstPass = true;
    this.previousCellStates = {};
  }

  perform(state) {
    if (this.firstPass) {
      this.firstPass = false;
      this.variables.forEach(variable => {
        this.previousCellStates[variable.id] = state.get('cellStates').get(variable.id);
      });
      this.oldErrorVariables = state.get('errorVariables');
    }
    return state.withMutations(s => {
      s.set('errorVariables', new Immutable.List(this.variables));
      this.variables.forEach(variable => {
        s.set('cellStates', s.get('cellStates').set(variable.id, CellStates.Inconsistent));
      });
    });
  }

  undo(state) {
    return state.withMutations(s => {
      s.set('errorVariables', this.oldErrorVariables);
      this.variables.forEach(variable => {
        s.set('cellStates', s.get('cellStates').set(variable.id, this.previousCellStates[variable.id]));
      });
    });
  }
}

export default InconsistentAction;
