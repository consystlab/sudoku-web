export default class PuzzleModel {
  constructor({
    id,
    type = 'Base',
    source = '',
    mindifflvl = 1,
    difflvl = 1,
    maxdifflvl = 5,
    name = '',
    origname = '',
    description = '',
    values,
    solutions = 0,
    consistency,
    clues = 0,
  }) {
    this.id = id;
    this.type = type;
    this.source = source;
    this.mindifflvl = mindifflvl;
    this.difflvl = difflvl;
    this.maxdifflvl = maxdifflvl;
    this.name = name;
    this.origname = origname;
    this.description = description;
    this.values = values;
    this.solutions = solutions;
    this.consistency = consistency;
    this.clues = clues;
  }

  static parse(fields) {
    const nums = fields[9].split('').map(v => parseInt(v, 10));
    const values = [];
    nums.forEach((num, i) => {
      const x = i % 9;
      const y = (i - x) / 9;
      if (values[y] == null) {
        values[y] = [];
      }
      values[y][x] = num;
    });
    return new PuzzleModel({
      id: parseInt(fields[0], 10),
      type: fields[1],
      source: fields[2],
      mindifflvl: parseInt(fields[3], 10),
      difflvl: parseInt(fields[4], 10),
      maxdifflvl: parseInt(fields[5], 10),
      name: fields[6],
      origname: fields[7],
      description: fields[8],
      values,
      solutions: parseInt(fields[10], 10),
      consistency: fields[11],
      clues: parseInt(fields[12], 10),
    });
  }
}
