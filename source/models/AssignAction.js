import Action from './Action';

class AssignAction extends Action {
  constructor(variable, assigned) {
    super();
    this.variable = variable;
    this.assigned = assigned;
    this.oldDomain = [];
    this.first = true;
  }

  perform(state) {
    if (this.first) {
      this.variable.unassign();
      this.oldDomain = this.variable.domain.current().filter(x => x !== this.assigned);
      this.first = false;
    }
    this.oldDomain.forEach(d => this.variable.domain.remove(d));
    this.variable.assign(this.assigned);
    return state;
  }

  undo(state) {
    this.variable.unassign();
    this.oldDomain.forEach(d => this.variable.domain.restore(d));
    return state;
  }
}

export default AssignAction;
