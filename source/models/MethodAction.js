import { Propagations } from 'consyst';

import Action from './Action';

class MethodAction extends Action {
  constructor(oldMethod, newMethod) {
    super();
    this.oldMethod = oldMethod;
    this.newMethod = newMethod;
    if ([Propagations.None,
      Propagations.BC,
      Propagations.BFC,
      Propagations.BRFL,
      Propagations.NBFC,
      Propagations.NBRFL,
    ].every(prop => prop !== this.newMethod)) {
      throw new RangeError(`Invalid newMethod given: ${this.newMethod}`);
    }
  }

  perform(state) {
    return state.set('solverMethod', this.newMethod);
  }

  undo(state) {
    return state.set('solverMethod', this.oldMethod);
  }

  getMethodString() {
    switch (this.newMethod) {
    case Propagations.None:
      return 'None, Good luck!';
    case Propagations.BC:
      return 'Back Checking';
    case Propagations.BFC:
      return 'Binary Forward Checking';
    case Propagations.BRFL:
      return 'Binary Real Full Lookahead';
    case Propagations.NBFC:
      return 'Nonbinary Forward Checking';
    case Propagations.NBRFL:
      return 'Nonbinary Real Full Lookahead';
    default:
    }
    throw new Error(`Action::ChooseMethodAction::getMethodString::Invalid method given: ${this.newMethod}`);
  }
}

export default MethodAction;
