class Action {
  perform() {
    throw new Error('Abstract class method Action:perform not overwritten');
  }
  undo() {
    throw new Error('Abstract class method Action:undo not overwritten');
  }
}

export default Action;
