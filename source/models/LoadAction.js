import CellStates from 'enumerations/CellStates';
import Action from './Action';

class LoadAction extends Action {
  constructor(variables, puzzle) {
    super();
    this.variables = variables;
    this.puzzle = puzzle;
  }

  perform(state) {
    return state.withMutations(s => {
      this.puzzle.values.forEach((line, y) => {
        line.forEach((num, x) => {
          if (num !== 0) {
            this.variables[y][x].domain.original().forEach(value => {
              if (value !== num) {
                this.variables[y][x].domain.remove(value);
              }
            });
            this.variables[y][x].assign(num);
            s.set('cellStates', s.get('cellStates').set(`${y}_${x}`, CellStates.PuzzleLoaded));
          }
        });
      });
    });
  }

  undo(state) {
    return state.withMutations(s => {
      this.puzzle.values.forEach((line, y) => {
        line.forEach((num, x) => {
          if (num !== 0) {
            this.variables[y][x].unassign();
            this.variables[y][x].domain.restore();
            s.set('cellStates', s.get('cellStates').set(`${y}_${x}`, CellStates.Normal));
          }
        });
      });
    });
  }
}

export default LoadAction;
