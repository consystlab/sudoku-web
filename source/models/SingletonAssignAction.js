import Action from './Action';

class SingletonAssignAction extends Action {
  constructor(assignments, // { [key: string]: [Variable, number] }
  ) {
    super();
    this.assignments = assignments;
  }

  perform(state) {
    Object.keys(this.assignments).forEach(key => {
      this.assignments[key][0].assign(this.assignments[key][1]);
    });
    return state;
  }

  undo(state) {
    Object.keys(this.assignments).forEach(key => {
      this.assignments[key][0].unassign();
    });
    return state;
  }
}

export default SingletonAssignAction;
