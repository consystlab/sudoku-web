export default (state) => {
  const preState = state;
  const currentIndex = preState.board.get('currentIndex');
  const currentChildIndex = preState.board.get('currentChildIndex');
  const maxChildIndex = preState.board.get('histories').get(currentIndex - 1).get('actions').length;
  for (let i = currentChildIndex; i < maxChildIndex; i++) {
    preState.board = preState.board.get('histories').get(currentIndex - 1).get('actions')[i].perform(preState.board);
  }
  return preState;
};
