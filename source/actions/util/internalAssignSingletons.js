import SingletonAssignAction from 'models/SingletonAssignAction';
import SingletonAssignHistoryRecord from 'records/SingletonAssignHistoryRecord';

export default (dispatch, state) => {
  const sudoku = state.board.get('sudoku');
  const assignments = {}; // { [key: string]: [CellContainer, number]}
  const variablesAffected = []; // Array<Variable>
  sudoku.variables.forEach(variable => {
    if (variable.domain.size() === 1 && !variable.isAssigned()) {
      assignments[variable.id] = [variable, variable.domain.first()];
      variablesAffected.push(variable);
    }
  });
  const singletonAssignAction = new SingletonAssignAction(assignments);
  const singletonAssignHistoryRecord = new SingletonAssignHistoryRecord({
    actions: [singletonAssignAction],
    variablesAffected,
    assignments,
  });

  dispatch({
    type: 'ADD_HISTORY',
    history: singletonAssignHistoryRecord,
  });
};
