
const cleanSudokuCopy = (state) => {
  const sudoku = state.board.get('sudoku').copy();

  sudoku.variables.forEach(variable => {
    if (!variable.isAssigned()) {
      variable.domain.restore();
    } else {
      const assignment = variable.getAssignment();
      variable.unassign();
      variable.domain.current().forEach(value => {
        if (value !== assignment) {
          variable.domain.remove(value);
        }
      });
    }
  });
  return sudoku;
};

const findMinimumConsistency = (state) => {
  const consistencies = [
    { name: 'AC', fcn: 'binaryArcConsistency' },
    { name: 'GAC', fcn: 'generalArcConsistency' },
    { name: 'SAC', fcn: 'singletonArcConsistency' },
    { name: 'SAC + GAC', loop: ['generalArcConsistency', 'singletonArcConsistency'] },
    { name: 'SGAC', fcn: 'singletonGeneralArcConsistency' },
    { name: 'POAC', fcn: 'poac' },
    { name: 'SGAC + POAC', loop: ['singletonGeneralArcConsistency', 'poac'] },
    { name: 'POGAC', fcn: 'pogac' },
    { name: 'BiSAC', fcn: 'bisac' },
    { name: 'POGAC + BiSAC', loop: ['pogac', 'bisac'] },
    { name: 'BiSGAC', fcn: 'bisgac' },
    { name: 'SSAC', fcn: 'ssac' },
    { name: 'BiSGAC + SSAC', loop: ['bisgac', 'ssac'] },
    { name: 'SSGAC', fcn: 'ssgac' },
  ];
  for (let i = 0; i < consistencies.length; i++) {
    const consistency = consistencies[i];
    const sudoku = cleanSudokuCopy(state);
    let consistent = true;
    if (consistency.fcn) {
      consistent = sudoku[consistency.fcn]().consistent;
    } else {
      let loop1Removal;
      let loop2Removal;
      do {
        loop1Removal = sudoku[consistency.loop[0]]();
        loop2Removal = sudoku[consistency.loop[1]]();
        consistent = consistent && loop1Removal.consistent && loop2Removal.consistent;
      } while (
        (loop1Removal.removeHistory.length > 0 || loop2Removal.removeHistory.length > 0)
        && consistent
      );
    }
    if (!consistent) {
      return `Inconsistent on ${consistency.name}`;
    }
    if (sudoku.variables.every(variable => variable.domain.size() === 1)) {
      return consistency.name;
    }
  }

  return 'Unsolved';
};

export default findMinimumConsistency;
