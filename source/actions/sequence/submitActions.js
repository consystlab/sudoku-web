import appSettings from 'appSettings'; //eslint-disable-line

import {
  FCGAC,
  BCSSP,
  DomWDegOrdering,
  SearchResults,
} from 'consyst';

import SubmitStates from 'enumerations/SubmitStates';

import findMinimumConsistency from 'actions/util/findMinimumConsistency';
import getValueString from 'util/getValueString';

import {
  getPuzzles,
} from 'actions/sequence/puzzleActions';

import {
  setSubmitState,
  preloadSubmitPuzzle,
} from 'actions/atomic/submitPuzzleActions';

export const submitPuzzle = () => (dispatch, getState) => {
  const state = getState();

  if (state.submitPuzzle.get('puzzleName') === '') {
    dispatch(setSubmitState(SubmitStates['NO_NAME']));
    return;
  }
  if (state.submitPuzzle.get('minDiffLvlError')) {
    dispatch(setSubmitState(SubmitStates['MIN_DIFF_ERROR']));
    return;
  }
  if (state.submitPuzzle.get('diffLvlError')) {
    dispatch(setSubmitState(SubmitStates['DIFF_ERROR']));
    return;
  }
  if (state.submitPuzzle.get('maxDiffLvlError')) {
    dispatch(setSubmitState(SubmitStates['MAX_DIFF_ERROR']));
    return;
  }
  const minDiff = parseInt(state.submitPuzzle.get('minDiffLvl'), 10);
  const diff = parseInt(state.submitPuzzle.get('diffLvl'), 10);
  const maxDiff = parseInt(state.submitPuzzle.get('maxDiffLvl'), 10);
  if (minDiff > diff) {
    dispatch(setSubmitState(SubmitStates['DIFF_LESSER']));
    return;
  }
  if (diff > maxDiff) {
    dispatch(setSubmitState(SubmitStates['DIFF_GREATER']));
    return;
  }

  const sudoku = state.board.get('sudoku');
  const maxSolutions = state.solverSettings.get('maxSearchResults');

  const solutionResult = BCSSP(
    sudoku,
    FCGAC,
    DomWDegOrdering,
    false,
    maxSolutions,
    true,
  );

  if (solutionResult[0] === SearchResults.IMPOSSIBLE
  || solutionResult[0] === SearchResults.INCONSISTENT_BEFORE_SEARCH) {
    dispatch(setSubmitState(SubmitStates['INCONSISTENT_PUZZLE']));
    return;
  } else if (solutionResult[0] === SearchResults.TOO_MANY) {
    dispatch(setSubmitState(SubmitStates['TOO_MANY_SOLUTIONS']));
    return;
  }

  const minimumConsistency = findMinimumConsistency(state);
  if (minimumConsistency.toLowerCase().indexOf('inconsistent') > -1) {
    dispatch(setSubmitState(SubmitStates['INCONSISTENT_PUZZLE']));
    return;
  }

  const numOfSolutions = solutionResult[1].length;
  const puzzleValues = getValueString(state.board);

  const xhr = new XMLHttpRequest();
  if (appSettings.environment === 'PRODUCTION') {
    xhr.open('POST', appSettings.solverPuzzleURL, true);
    xhr.setRequestHeader('Content-type', 'application/json');
    xhr.onreadystatechange = () => {
      if (xhr.readyState === XMLHttpRequest.DONE) {
        switch (xhr.status) {
        case 200:
          if (xhr.responseText === 'Puzzle with same name exists') {
            dispatch(setSubmitState(SubmitStates['NAME_EXISTS']));
          } else if (xhr.responseText === 'Puzzle with same assignments exists') {
            dispatch(setSubmitState(SubmitStates['PUZZLE_EXISTS']));
          } else {
            dispatch(setSubmitState(SubmitStates['PUZZLE_ACCEPTED']));
            dispatch(getPuzzles());
          }
          break;

        default:
          dispatch(setSubmitState(SubmitStates['UNKOWN_ERROR']));
        }
      }
    };
    xhr.send(JSON.stringify({
      query: 'INSERT_NEW_PUZZLE',
      newPuzzle: {
        puzzleName: state.submitPuzzle.get('puzzleName'),
        sudokuType: state.submitPuzzle.get('sudokuType'),
        source: state.submitPuzzle.get('source'),
        minDiffLvl: minDiff,
        diffLvl: diff,
        maxDiffLvl: maxDiff,
        origName: state.submitPuzzle.get('origName'),
        description: state.submitPuzzle.get('description'),
        solutions: numOfSolutions,
        consistency: minimumConsistency,
        puzzleValues,
      },
    }));
  } else {
    dispatch(setSubmitState(SubmitStates['PUZZLE_SUBMITTED']));
  }
};

export const checkPuzzleNames = () => (dispatch, getState) => {
  const state = getState();
  if (appSettings.environment === 'PRODUCTION') {
    const puzzleName = state.submitPuzzle.get('puzzleName');
    const xhrValues = new XMLHttpRequest();
    xhrValues.open('POST', appSettings.solverPuzzleURL, true);
    xhrValues.setRequestHeader('Content-type', 'application/json');
    xhrValues.onreadystatechange = () => {
      if (xhrValues.readyState === XMLHttpRequest.DONE) {
        switch (xhrValues.status) {
        case 200:
          if (xhrValues.responseText !== '') {
            dispatch(setSubmitState(SubmitStates['NAME_EXISTS']));
            const puzzle = state.puzzle.get('puzzles').find(p => p.name === puzzleName);
            if (puzzle) {
              dispatch(preloadSubmitPuzzle(puzzle));
            }
            dispatch(preloadSubmitPuzzle(parseInt(xhrValues.responseText, 10)));
          }
          break;

        default:
          dispatch(setSubmitState(SubmitStates['CONTACT_SERVER_ERROR']));
        }
      }
    };
    xhrValues.send(JSON.stringify({
      query: 'CHECK_VALUES',
      puzzleName,
    }));
  }
};

export const checkPuzzle = () => (dispatch, getState) => {
  const state = getState();
  if (appSettings.environment === 'PRODUCTION') {
    const puzzleValuesArray = [];
    for (let i = 0; i < 9; i++) {
      for (let j = 0; j < 9; j++) {
        const variable = state.board.get('variables')[i][j];
        if (variable.isAssigned()) {
          puzzleValuesArray.push(variable.getAssignment());
        } else {
          puzzleValuesArray.push(0);
        }
      }
    }
    const puzzleValues = puzzleValuesArray.join('');

    const xhrValues = new XMLHttpRequest();
    xhrValues.open('POST', appSettings.solverPuzzleURL, true);
    xhrValues.setRequestHeader('Content-type', 'application/json');
    xhrValues.onreadystatechange = () => {
      if (xhrValues.readyState === XMLHttpRequest.DONE) {
        switch (xhrValues.status) {
        case 200:
          if (xhrValues.responseText !== '') {
            dispatch(setSubmitState(SubmitStates['PUZZLE_EXISTS']));
            const puzzleId = parseInt(xhrValues.responseText, 10);
            const puzzle = state.puzzle.get('puzzles').find(p => p.id === puzzleId);
            if (puzzle) {
              dispatch(preloadSubmitPuzzle(puzzle));
            }
          } else {
            checkPuzzleNames()(dispatch, getState);
          }
          break;

        default:
          dispatch(setSubmitState(SubmitStates['CONTACT_SERVER_ERROR']));
        }
      }
    };
    xhrValues.send(JSON.stringify({
      query: 'CHECK_VALUES',
      puzzleValues,
    }));
  }
};
