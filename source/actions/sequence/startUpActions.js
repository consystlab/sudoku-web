import appSettings from 'appSettings'; //eslint-disable-line

import {
  addBeginHistory,
  generateBoard,
} from 'actions/atomic/boardActions';
import {
  getStartupPuzzles,
} from 'actions/sequence/puzzleActions';

export const startUp = () => (dispatch) => {
  dispatch(generateBoard());
  dispatch(addBeginHistory());
  dispatch(getStartupPuzzles(appSettings.solverPuzzleURL, appSettings.environment));
};
