import {
  Propagations,
  ValuesRemoved,
} from 'consyst';
import InconsistentAction from 'models/InconsistentAction';
import preAddHistory from 'actions/util/preAddHistory';
import findMinimumConsistencyUtil from 'actions/util/findMinimumConsistency';
import internalAssignSingletons from 'actions/util/internalAssignSingletons';
import verifyAC from 'util/verifyAC';
import AssignAction from 'models/AssignAction';
import AssignHistoryRecord from 'records/AssignHistoryRecord';
import RemoveAction from 'models/RemoveAction';
import RemoveHistoryRecord from 'records/RemoveHistoryRecord';
import SearchCount from 'enumerations/SearchCount';

import {
  addHistory,
  historySeek,
} from 'actions/atomic/boardActions';
import {
  toggleAutoSingleton,
} from 'actions/atomic/controlPanelActions';
import {
  setConsistency,
} from 'actions/atomic/submitPuzzleActions';

import {
  setSolvingMethod,
} from 'actions/sequence/filteringActions';
import {
  performFcSearch,
} from 'actions/sequence/searchActions';

const performConsistency = (consistency, subset, rowColOrGroup = null) => (dispatch, getState) => {
  const state = preAddHistory(getState());
  const sudoku = state.board.get('sudoku');
  const errors = verifyAC(sudoku);
  if (errors.size > 0) {
    return;
  }

  let removeVarSequence = [];
  switch (consistency) {
  case Propagations.AC:
  case Propagations.ROW_AC:
  case Propagations.COL_AC:
  case Propagations.GRO_AC:
    removeVarSequence = sudoku.binaryArcConsistency(subset).removeHistory;
    break;
  case Propagations.GAC:
  case Propagations.ROW_GAC:
  case Propagations.COL_GAC:
  case Propagations.GRO_GAC:
    removeVarSequence = sudoku.generalArcConsistency(subset).removeHistory;
    break;
  case Propagations.SAC:
    removeVarSequence = sudoku.singletonArcConsistency(subset).removeHistory;
    break;
  case Propagations.SGAC:
    removeVarSequence = sudoku.singletonGeneralArcConsistency(subset).removeHistory;
    break;
  case Propagations.POAC:
    removeVarSequence = sudoku.poac().removeHistory;
    break;
  case Propagations.POGAC:
    removeVarSequence = sudoku.pogac().removeHistory;
    break;
  case Propagations.BiSAC:
    removeVarSequence = sudoku.bisac().removeHistory;
    break;
  case Propagations.BiSGAC:
    removeVarSequence = sudoku.bisgac().removeHistory;
    break;
  case Propagations.SSAC:
    removeVarSequence = sudoku.ssac().removeHistory;
    break;
  case Propagations.SSGAC:
    removeVarSequence = sudoku.ssgac().removeHistory;
    break;
  default:
  }

  const remaining = {};
  const variablesAffected = new Set();

  const removeActionSequence = removeVarSequence.map(removed => {
    removed.forEach((variable, removedValues) => {
      variablesAffected.add(variable);
      if (!remaining[variable.id]) {
        remaining[variable.id] = {
          variable,
          remaining: new Set(variable.domain.current()),
        };
      }

      removedValues.forEach(value => {
        remaining[variable.id].remaining.delete(value);
      });
    });
    return new RemoveAction(consistency, removed);
  });

  sudoku.variables.forEach(v => {
    if (v.domain.size() === 0) {
      errors.add(v);
    }
  });
  const errorAction = errors.size ? new InconsistentAction(errors) : null;
  const actions = errors.size ? [...removeActionSequence, errorAction] : removeActionSequence;

  const removeHistoryRecord = new RemoveHistoryRecord({
    actions,
    removeActions: removeActionSequence,
    variablesAffected: Array.from(variablesAffected),
    remaining,
    method: consistency,
    rowColOrGroup,
    errors,
  });

  dispatch(addHistory(removeHistoryRecord));

  if (state.controlPanel.get('autoSingleton')) {
    internalAssignSingletons(dispatch, state);
  }
};

const generateAssignHistoryRecord = (sudoku, assignActions, method) => {
  const variables = assignActions.map(action => action.variable);

  let removeVarSequence = [];
  let removeVarSequences = [];

  assignActions.forEach(assignAction => assignAction.perform());

  switch (method) {
  case Propagations.BC:
    break;

  case Propagations.BFC:
    removeVarSequences = variables.map(v => {
      const bfcVariables = Array.from(v.binaryNeighbors);
      bfcVariables.unshift(v);
      return sudoku.binaryForwardChecking(bfcVariables).removeHistory;
    });
    break;

  case Propagations.BRFL:
    removeVarSequence = sudoku.binaryArcConsistency().removeHistory;
    break;

  case Propagations.NBFC:
    removeVarSequences = variables.map(v =>
      sudoku.generalForwardChecking(v.generalConstraints).removeHistory
    );
    break;

  case Propagations.NBRFL:
    removeVarSequence = sudoku.generalArcConsistency().removeHistory;
    break;

  default:
    break;
  }

  if (removeVarSequences.length !== 0) {
    removeVarSequence = removeVarSequences.reduce((acc, current) => {
      Array.prototype.push.apply(acc, current);
      return acc;
    }, []);
  }

  const remaining = {};
  const variablesAffected = new Set(variables);

  const removeActions = removeVarSequence.map(removed => {
    removed.forEach((removeVariable, removedValues) => {
      variablesAffected.add(removeVariable);
      if (!remaining[removeVariable.id]) {
        remaining[removeVariable.id] = {
          variable: removeVariable,
          remaining: new Set(removeVariable.domain.current()),
        };
      }

      removedValues.forEach(v => {
        remaining[removeVariable.id].remaining.delete(v);
      });
    });
    return new RemoveAction(method, removed);
  });

  const actions = [...assignActions, ...removeActions];

  let errors = new Set();
  if (method !== Propagations.None) {
    errors = verifyAC(sudoku);
    sudoku.variables.forEach(v => {
      if (v.domain.size() === 0) {
        errors.add(v);
      }
    });
    const errorAction = errors.size ? new InconsistentAction(errors) : null;
    if (errorAction) actions.push(errorAction);
  }

  return new AssignHistoryRecord({
    actions,
    assignActions,
    removeActions,
    variablesAffected: Array.from(variablesAffected),
    method,
    remaining,
    errors,
  });
};

export const assignVariableValue = (variableId, value) => (dispatch, getState) => {
  const state = preAddHistory(getState());
  const sudoku = state.board.get('sudoku');
  const variable = sudoku.variables.find(v => v.id === variableId);

  const assigned = variable.assign(value);
  const method = state.board.get('solverMethod');
  if (!assigned) {
    return;
  }
  let assignActions = [];
  let modifying = false;
  let performState = state.board;
  const histories = state.board.get('histories');
  const currentIndex = state.board.get('currentIndex');
  const currentChildIndex = state.board.get('currentChildIndex');
  const oldRecord = histories.get(currentIndex - 1);
  if (oldRecord.name === 'AssignHistoryRecord') {
    // Undo all actions in the old record before modifying.
    modifying = true;
    if (currentChildIndex > 0) {
      for (let i = currentChildIndex - 1; i >= 0; i--) {
        performState = histories.get(currentIndex - 1).get('actions')[i].undo(performState);
      }
    }
    assignActions = oldRecord.assignActions.slice();
    performState = assignActions.reduce((currentState, action) => action.perform(currentState), performState);
  }
  assignActions.push(new AssignAction(variable, value));
  const assignHistoryRecord = generateAssignHistoryRecord(
    sudoku,
    assignActions,
    method
  );

  if (modifying) {
    const olderRecord = histories.get(currentIndex - 2);
    dispatch(historySeek(currentIndex - 1, olderRecord.actions.length));
  }
  dispatch(addHistory(assignHistoryRecord));

  if (state.controlPanel.get('autoSingleton')) {
    internalAssignSingletons(dispatch, state);
  }
};

export const unassignVariable = (variableId) => (dispatch, getState) => {
  const state = preAddHistory(getState());
  const sudoku = state.board.get('sudoku');
  const variable = sudoku.variables.find(v => v.id === variableId);

  const assigned = variable.isAssigned();
  if (!assigned) {
    return;
  }

  const histories = state.board.get('histories');
  const currentIndex = state.board.get('currentIndex');
  let assignedIndex = currentIndex;
  let foundIndex = false;
  let history;
  do {
    assignedIndex -= 1;
    history = histories.get(assignedIndex);
    if (history.name === 'AssignHistoryRecord') {
      const assignment = history.assignActions.find(action => action.variable === variable);
      if (assignment) {
        foundIndex = true;
      }
    } else if (history.name === 'SingletonAssignHistoryRecord') {
      const assignment = history.assignments[variableId];
      if (assignment) {
        return;
      }
    } else if (history.name === 'LoadHistoryRecord') {
      const assignment = history.assignments[variableId];
      if (assignment) {
        return;
      }
    }
  } while (!foundIndex && assignedIndex > 0);

  const path = [];
  for (let i = assignedIndex + 1; i < currentIndex; i++) {
    path.push(histories.get(i));
  }

  const currentChildIndex = state.board.get('currentChildIndex');
  let performState = state.board;

  // Track back to right before the assignment was created.
  if (currentChildIndex > 0) {
    for (let i = currentChildIndex - 1; i >= 0; i--) {
      performState = histories.getIn([currentIndex - 1, 'actions'])[i].undo(performState);
    }
  }
  for (let i = currentIndex - 1; i > assignedIndex; i--) {
    performState = histories.getIn([i - 1, 'actions'])
      .reduce((s, a) => a.undo(s), performState);
  }
  performState = performState.set('currentChildIndex', histories.getIn([assignedIndex - 1, 'actions']).length);
  // The previous is equivalent to dispatch(historySeek(assignedIndex - 1, olderRecord.actions.length));
  // which we have to do anyway since this doesn't affect the actual state.

  const assignActions = history.assignActions.filter(x => x.variable !== variable);
  performState = assignActions.reduce((currentState, action) => action.perform(currentState), performState);

  const newHistoryRecord = generateAssignHistoryRecord(
    sudoku,
    assignActions,
    history.method
  );

  const olderRecord = histories.get(assignedIndex - 1);
  dispatch(historySeek(assignedIndex, olderRecord.actions.length));
  if (newHistoryRecord.assignActions.length > 0) {
    dispatch(addHistory(newHistoryRecord));
  }

  path.forEach(record => {
    switch (record.name) {
    case 'AssignHistoryRecord':
      dispatch(addHistory(generateAssignHistoryRecord(sudoku, record.assignActions, record.method)));
      break;

    case 'MethodHistoryRecord':
      dispatch(setSolvingMethod(record.method));
      break;

    case 'RemoveHistoryRecord':
      let set = null;
      const rowColOrGroup = record.rowColOrGroup;
      switch (record.method) {
      case Propagations.ROW_AC:
        set = state.board.get('arcRows').get(rowColOrGroup);
        break;
      case Propagations.COL_AC:
        set = state.board.get('arcCols').get(rowColOrGroup);
        break;
      case Propagations.GRO_AC:
        set = state.board.get('arcGroups').get(rowColOrGroup);
        break;
      case Propagations.ROW_GAC:
        set = state.board.get('gacRows').get(rowColOrGroup);
        break;
      case Propagations.COL_GAC:
        set = state.board.get('gacCols').get(rowColOrGroup);
        break;
      case Propagations.GRO_GAC:
        set = state.board.get('gacGroups').get(rowColOrGroup);
        break;
      default:
      }
      dispatch(performConsistency(record.method, set, rowColOrGroup));
      break;

    case 'SingletonAssignHistoryRecord':
      internalAssignSingletons(dispatch, state);
      break;

    case 'SolutionsHistoryRecord':
      dispatch(performFcSearch(record.searchFor === SearchCount.One));
      break;

    default:
      throw new Error(`Did not handle record's name: ${record.name}`);
    }
  });
};

export const removeUserValue = (removed, variable) => dispatch => {
  if (!variable.domain.contains(removed)) {
    return;
  }
  const removeAction = new RemoveAction(
    Propagations.USER,
    new ValuesRemoved({
      [variable.id]: {
        variable,
        removed: new Set([removed]),
      },
    }),
  );
  const remaining = {
    [variable.id]: {
      variable,
      remaining: new Set(variable.domain.current()),
    },
  };
  const removeHistoryRecord = new RemoveHistoryRecord({
    actions: [removeAction],
    removeActions: [removeAction],
    variablesAffected: [variable],
    remaining,
    method: Propagations.USER,
  });
  dispatch(addHistory(removeHistoryRecord));
};

export const performArc = () => performConsistency(Propagations.AC);
export const performGac = () => performConsistency(Propagations.GAC);
export const performSac = () => performConsistency(Propagations.SAC);
export const performSgac = () => performConsistency(Propagations.SGAC);
export const performPoac = () => performConsistency(Propagations.POAC);
export const performPogac = () => performConsistency(Propagations.POGAC);
export const performBisac = () => performConsistency(Propagations.BiSAC);
export const performBisgac = () => performConsistency(Propagations.BiSGAC);
export const performSsac = () => performConsistency(Propagations.SSAC);
export const performSsgac = () => performConsistency(Propagations.SSGAC);

export const performArcRow = index => (dispatch, getState) => {
  const row = getState().board.get('arcRows').get(index);
  return performConsistency(Propagations.ROW_AC, row, index)(dispatch, getState);
};
export const performArcCol = index => (dispatch, getState) => {
  const col = getState().board.get('arcCols').get(index);
  return performConsistency(Propagations.COL_AC, col, index)(dispatch, getState);
};
export const performArcGroup = index => (dispatch, getState) => {
  const group = getState().board.get('arcGroups').get(index);
  return performConsistency(Propagations.GRO_AC, group, index)(dispatch, getState);
};

export const performGacRow = index => (dispatch, getState) => {
  const row = getState().board.get('gacRows').get(index);
  return performConsistency(Propagations.ROW_GAC, row, index)(dispatch, getState);
};
export const performGacCol = index => (dispatch, getState) => {
  const col = getState().board.get('gacCols').get(index);
  return performConsistency(Propagations.COL_GAC, col, index)(dispatch, getState);
};
export const performGacGroup = index => (dispatch, getState) => {
  const group = getState().board.get('gacGroups').get(index);
  return performConsistency(Propagations.GRO_GAC, group, index)(dispatch, getState);
};

export const assignSingletons = () => (dispatch, getState) => {
  const state = preAddHistory(getState());
  if (state.controlPanel.get('autoSingleton')) {
    dispatch(toggleAutoSingleton());
  }
  return internalAssignSingletons(dispatch, state);
};

export const findMinimumConsistency = () => (dispatch, getState) => {
  const state = getState();
  dispatch(setConsistency(findMinimumConsistencyUtil(state)));
};
