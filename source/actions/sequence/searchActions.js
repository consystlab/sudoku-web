import Immutable from 'immutable';
import { addHistory } from 'actions/atomic/boardActions';
import {
  setSubmitSolutionsRecord,
  setSubmitMaxSolutions,
} from 'actions/atomic/submitPuzzleActions';

import {
  FCGAC,
  // FC,
  BCSSP,
  DomWDegOrdering,
  // LexicoSingletonOrdering,
} from 'consyst';

import SearchCount from 'enumerations/SearchCount';

import SolutionRecord from 'records/SolutionRecord';
import SolutionsHistoryRecord from 'records/SolutionsHistoryRecord';

const fcSearch = (sudoku, maxSolutions, singleSolution = true, cleanSlate = false) => {
  const originalAssignmentsList = sudoku.variables.filter(variable => variable.isAssigned());
  const originalAssignments = originalAssignmentsList.reduce((acc, current) =>
    acc.set(current.toString(), true)
  , Immutable.Map());

  const solutionResult = BCSSP(
    sudoku,
    FCGAC,
    // FC,
    DomWDegOrdering,
    // LexicoSingletonOrdering,
    singleSolution,
    maxSolutions,
    cleanSlate
  );

  const solutions = solutionResult[1].map(sol => new SolutionRecord({
    assignments: Immutable.Map(sol),
  }));

  const cellValues = {};
  solutions.forEach(solution => {
    solution.assignments.forEach(assignment => {
      if (!cellValues[assignment[0].id]) {
        cellValues[assignment[0].id] = new Set();
      }
      cellValues[assignment[0].id].add(assignment[1]);
    });
  });
  const differingAllCells = {};
  Object.entries(cellValues).forEach(entry => {
    if (entry[1].size > 1) {
      differingAllCells[entry[0]] = true;
    }
  });

  const differingPairwiseCells = [];
  for (let i = 0; i < solutions.length - 1; i++) {
    differingPairwiseCells[i] = {};
    solutions[i].assignments.forEach(assignment => {
      if (solutions[i + 1].assignments.get(assignment[0].toString())[1] !== assignment[1]) {
        differingPairwiseCells[i][assignment[0].toString()] = true;
      }
    });
  }

  const solutionsHistoryRecord = new SolutionsHistoryRecord({
    solutions,
    status: solutionResult[0],
    searchFor: singleSolution ? SearchCount.One : SearchCount.All,
    maxSolutions,
    differingAllCells: Immutable.Map(differingAllCells),
    differingPairwiseCells: Immutable.fromJS(differingPairwiseCells),
    originalAssignments,
  });
  return solutionsHistoryRecord;
};

export const performFcSearch = (singleSolution = true) => (dispatch, getState) => {
  const state = getState();
  const sudoku = state.board.get('sudoku');
  const maxSolutions = state.solverSettings.get('maxSearchResults');

  const solutionsHistoryRecord = fcSearch(sudoku, maxSolutions, singleSolution);
  dispatch(addHistory(solutionsHistoryRecord));
};

export const submitSearch = () => (dispatch, getState) => {
  const state = getState();
  const sudoku = state.board.get('sudoku');
  const maxSolutions = state.solverSettings.get('maxSaveResults');

  const solutionsHistoryRecord = fcSearch(sudoku, maxSolutions, false, true);
  dispatch(setSubmitSolutionsRecord(solutionsHistoryRecord));
  dispatch(setSubmitMaxSolutions(maxSolutions));
};
