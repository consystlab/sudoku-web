import { Propagations } from 'consyst';

import verifyAC from 'util/verifyAC';
import preAddHistory from 'actions/util/preAddHistory';
import internalAssignSingletons from 'actions/util/internalAssignSingletons';

import MethodAction from 'models/MethodAction';
import RemoveAction from 'models/RemoveAction';
import InconsistentAction from 'models/InconsistentAction';
import MethodHistoryRecord from 'records/MethodHistoryRecord';

import {
  addHistory,
  SET_SOLVING_METHOD,
} from 'actions/atomic/boardActions';

export const setSolvingMethod = (method) => (dispatch, getState) => {
  const state = preAddHistory(getState());
  const sudoku = state.board.get('sudoku');

  if (state.board.get('errorVariables').size !== 0) return;

  let removeVarSequence = [];
  switch (method) {
  case Propagations.None:
    break;

  case Propagations.BC:
    break;

  case Propagations.BFC:
    removeVarSequence = sudoku.variables.reduce((acc, variable) => {
      if (variable.isAssigned()) {
        acc.push(...sudoku.binaryForwardChecking([variable, ...Array.from(variable.binaryNeighbors)]).removeHistory);
      }
      return acc;
    }, []);
    break;

  case Propagations.BRFL:
    removeVarSequence = sudoku.binaryArcConsistency().removeHistory;
    break;

  case Propagations.NBFC:
    removeVarSequence = sudoku.variables.reduce((acc, variable) => {
      if (variable.isAssigned()) {
        acc.push(...sudoku.generalForwardChecking(variable.generalConstraints).removeHistory);
      }
      return acc;
    }, []);
    break;

  case Propagations.NBRFL:
    removeVarSequence = sudoku.generalArcConsistency().removeHistory;
    break;

  default:
    throw new Error(`actions/controlPanelActions:setSolvingMethod: New method out of bounds: ${method}`);
  }

  const errors = verifyAC(sudoku);
  sudoku.variables.forEach(v => {
    if (v.domain.size() === 0) {
      errors.add(v);
    }
  });
  const errorAction = errors.size ? new InconsistentAction(errors) : null;

  const methodSetAction = new MethodAction(state.controlPanel.get('solverMethod'), method);
  const removeActionSequence = removeVarSequence.map((removed) =>
     new RemoveAction(Propagations.BFC, removed)
  );
  const actions = [methodSetAction, ...removeActionSequence];
  if (errorAction) actions.push(errorAction);

  const methodHistoryRecord = new MethodHistoryRecord({
    actions,
    setAction: methodSetAction,
    removeActions: removeActionSequence,
    method,
  });

  dispatch(addHistory(methodHistoryRecord));

  dispatch({
    type: SET_SOLVING_METHOD,
    method,
  });

  if (state.controlPanel.get('autoSingleton')) {
    internalAssignSingletons(dispatch, state);
  }
};
