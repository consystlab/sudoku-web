import appSettings from 'appSettings'; //eslint-disable-line
import {
  parsePuzzles,
} from 'actions/atomic/puzzleActions';

export const getStartupPuzzles = (puzzleURL, environment) => (dispatch) => {
  $.get(puzzleURL, (puzzleData) => {
    if (environment === 'PRODUCTION') {
      dispatch(parsePuzzles(JSON.parse(puzzleData)));
    } else {
      dispatch(parsePuzzles(puzzleData));
    }
  });
};

export const getPuzzles = () => (dispatch) => {
  const puzzleURL = appSettings.solverPuzzleURL;
  const environment = appSettings.environment;
  $.get(puzzleURL, (puzzleData) => {
    if (environment === 'PRODUCTION') {
      dispatch(parsePuzzles(JSON.parse(puzzleData)));
    } else {
      dispatch(parsePuzzles(puzzleData));
    }
  });
};
