export const SET_PROPAGATION_POSITION = 'SET_PROPAGATION_POSITION';
export const SET_PROPAGATION_STYLE = 'SET_PROPAGATION_STYLE';

export const setPropagationPosition = (propagation, position) => ({
  type: SET_PROPAGATION_POSITION,
  propagation,
  position,
});

export const setPropagationStyle = (propagation, style) => ({
  type: SET_PROPAGATION_STYLE,
  propagation,
  style,
});
