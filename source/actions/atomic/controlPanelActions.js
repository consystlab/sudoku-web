export const SET_CONTROL_SECTION = 'SET_CONTROL_SECTION';
export const TOGGLE_MINIS_VISIBLE = 'TOGGLE_MINIS_VISIBLE';
export const CALL_REMOVE_DIALOG = 'CALL_REMOVE_DIALOG';
export const HIDE_REMOVE_DIALOG = 'HIDE_REMOVE_DIALOG';
export const TOGGLE_AUTO_SINGLETON = 'TOGGLE_AUTO_SINGLETON';

export const setControlSection = index => ({
  type: SET_CONTROL_SECTION,
  index,
});

export const toggleMinisVisible = () => ({
  type: TOGGLE_MINIS_VISIBLE,
});

export const toggleAutoSingleton = () => ({
  type: TOGGLE_AUTO_SINGLETON,
});

export const callRemoveDialog = (clientRect, row, col) => ({
  type: CALL_REMOVE_DIALOG,
  clientRect,
  row,
  col,
});

export const hideRemoveDialog = () => ({
  type: HIDE_REMOVE_DIALOG,
});
