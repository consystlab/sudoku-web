export const SET_PUZZLE_NAME = 'SET_PUZZLE_NAME';
export const SET_SUDOKU_TYPE = 'SET_SUDOKU_TYPE';
export const SET_SOURCE = 'SET_SOURCE';
export const SET_MIN_DIFF_LVL = 'SET_MIN_DIFF_LVL';
export const SET_DIFF_LVL = 'SET_DIFF_LVL';
export const SET_MAX_DIFF_LVL = 'SET_MAX_DIFF_LVL';
export const SET_ORIG_NAME = 'SET_ORIG_NAME';
export const SET_DESCRIPTION = 'SET_DESCRIPTION';
export const SET_PUZZLE_VALUES = 'SET_PUZZLE_VALUES';
export const SET_SUBMIT_SOLUTIONS_RECORD = 'SET_SUBMIT_SOLUTIONS_RECORD';
export const SET_CONSISTENCY = 'SET_CONSISTENCY';
export const SET_SUBMIT_MAX_SOLUTIONS = 'SET_SUBMIT_MAX_SOLUTIONS';
export const SET_SUBMIT_STATE = 'SET_SUBMIT_STATE';
export const PRELOAD_SUBMIT_PUZZLE = 'PRELOAD_SUBMIT_PUZZLE';

export const setPuzzleName = (name) => ({
  type: SET_PUZZLE_NAME,
  name,
});

export const setSudokuType = (sudokuType) => ({
  type: SET_SUDOKU_TYPE,
  sudokuType,
});

export const setSource = (source) => ({
  type: SET_SOURCE,
  source,
});

export const setMinDiffLvl = (level) => ({
  type: SET_MIN_DIFF_LVL,
  level,
});

export const setDiffLvl = (level) => ({
  type: SET_DIFF_LVL,
  level,
});

export const setMaxDiffLvl = (level) => ({
  type: SET_MAX_DIFF_LVL,
  level,
});

export const setOrigName = (name) => ({
  type: SET_ORIG_NAME,
  name,
});

export const setDescription = (description) => ({
  type: SET_DESCRIPTION,
  description,
});

export const setPuzzleValues = (puzzleValues) => ({
  type: SET_PUZZLE_VALUES,
  puzzleValues,
});

export const setSubmitSolutionsRecord = (solutionsRecord) => ({
  type: SET_SUBMIT_SOLUTIONS_RECORD,
  solutionsRecord,
});

export const setConsistency = (consistency) => ({
  type: SET_CONSISTENCY,
  consistency,
});

export const setSubmitMaxSolutions = (max) => ({
  type: SET_SUBMIT_MAX_SOLUTIONS,
  max,
});

export const setSubmitState = (state) => ({
  type: SET_SUBMIT_STATE,
  state,
});

export const preloadSubmitPuzzle = (puzzle) => ({
  type: PRELOAD_SUBMIT_PUZZLE,
  puzzle,
});
