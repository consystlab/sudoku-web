export const ADD_BEGIN_HISTORY = 'ADD_BEGIN_HISTORY';
export const GENERATE_BOARD = 'GENERATE_BOARD';
export const RESET_CELL_DOMAINS = 'RESET_CELL_DOMAINS';
export const ASSIGN_VARIABLE_VALUE = 'ASSIGN_VARIABLE_VALUE';
export const UNASSIGN_VARIABLE = 'UNASSIGN_VARIABLE';
export const FLASH_ERRORS = 'FLASH_ERRORS';
export const ADD_HISTORY = 'ADD_HISTORY';
export const HISTORY_SEEK = 'HISTORY_SEEK';
export const SET_SOLVING_METHOD = 'SET_SOLVING_METHOD';

export const addBeginHistory = () => ({
  type: ADD_BEGIN_HISTORY,
});

export const generateBoard = (width, height, groupx, groupy) => ({
  type: GENERATE_BOARD,
  width,
  height,
  groupx,
  groupy,
});

export const resetCellDomains = () => ({
  type: RESET_CELL_DOMAINS,
});

export const addHistory = history => ({
  type: ADD_HISTORY,
  history,
});

export const historySeek = (index, childIndex) => ({
  type: HISTORY_SEEK,
  index,
  childIndex,
});

export const unassignVariable = variableId => ({
  type: UNASSIGN_VARIABLE,
  variableId,
});
