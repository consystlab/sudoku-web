export const SUDOKU_IMAGE_UPLOADED = 'SUDOKU_IMAGE_UPLOADED';
export const SET_CANVAS_DATA = 'SET_CANVAS_DATA';

export const sudokuImageUploaded = (array) => ({
  type: SUDOKU_IMAGE_UPLOADED,
  array,
});

export const setCanvasData = (data) => ({
  type: SET_CANVAS_DATA,
  data,
});
