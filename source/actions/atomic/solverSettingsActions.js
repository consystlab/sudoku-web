export const TOGGLE_SHOW_EXPLANATIONS = 'SET_SHOW_EXPLANATIONS';
export const SET_MAX_SAVE_RESULTS_TEXT = 'SET_MAX_SAVE_RESULTS_TEXT';
export const SET_MAX_SEARCH_RESULTS_TEXT = 'SET_MAX_SEARCH_RESULTS';
export const SET_PUZZLE_URL = 'SET_PUZZLE_URL';
export const SET_ENVIRONMENT = 'SET_ENVIRONMENT';
export const SET_SOLUTIONS_DIFF = 'SET_SOLUTIONS_DIFF';

export const toggleShowExplanations = () => ({
  type: TOGGLE_SHOW_EXPLANATIONS,
});

export const setMaxSaveResults = (maxSaveResultsText) => ({
  type: SET_MAX_SAVE_RESULTS_TEXT,
  maxSaveResultsText,
});

export const setMaxSearchResults = (maxSearchResultsText) => ({
  type: SET_MAX_SEARCH_RESULTS_TEXT,
  maxSearchResultsText,
});

export const setPuzzleURL = (puzzleURL) => ({
  type: SET_PUZZLE_URL,
  puzzleURL,
});

export const setEnvironment = (environment) => ({
  type: SET_ENVIRONMENT,
  environment,
});

export const setSolutionsDiff = (diff) => ({
  type: SET_SOLUTIONS_DIFF,
  diff,
});
