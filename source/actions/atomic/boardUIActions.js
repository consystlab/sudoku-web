export const SET_ROW_HOVERED = 'SET_ROW_HOVERED';
export const SET_COL_HOVERED = 'SET_COL_HOVERED';
export const SET_GROUP_HOVERED = 'SET_GROUP_HOVERED';
export const SET_VARIABLES_HOVERED = 'SET_VARIABLES_HOVERED';
export const SET_FOCUSED_CELL_ID = 'SET_FOCUSED_CELL_ID';

export const setRowHovered = row => ({
  type: SET_ROW_HOVERED,
  row,
});

export const setColHovered = col => ({
  type: SET_COL_HOVERED,
  col,
});

export const setGroupHovered = group => ({
  type: SET_GROUP_HOVERED,
  group,
});

export const setVariablesHovered = variables => ({
  type: SET_VARIABLES_HOVERED,
  variables,
});

export const setFocusedCellId = (cellId, level) => ({
  type: SET_FOCUSED_CELL_ID,
  cellId,
  level,
});
