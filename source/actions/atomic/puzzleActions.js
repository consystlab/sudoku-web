export const PARSE_PUZZLES = 'PARSE_PUZZLES';
export const LOAD_PUZZLE = 'LOAD_PUZZLE';
export const SET_PUZZLE_FILTER = 'SET_PUZZLE_FILTER';
export const UPDATE_SORT_FIELD = 'UPDATE_SORT_FIELD';
export const SET_SELECTOR_VISIBILITY = 'SET_SELECTOR_VISIBILITY';
export const SET_COLUMN_VISIBILITY = 'SET_COLUMN_VISIBILITY';

export const parsePuzzles = lines => ({
  type: PARSE_PUZZLES,
  lines,
});

export const loadPuzzle = puzzle => ({
  type: LOAD_PUZZLE,
  puzzle,
});

export const updateSortField = field => ({
  type: UPDATE_SORT_FIELD,
  field,
});

export const setPuzzleFilter = (puzzleFilter, value) => ({
  type: SET_PUZZLE_FILTER,
  puzzleFilter,
  value,
});

export const setSelectorVisibility = (visible) => ({
  type: SET_SELECTOR_VISIBILITY,
  visible,
});

export const setColumnVisibility = (column, visible) => ({
  type: SET_COLUMN_VISIBILITY,
  column,
  visible,
});
