export const HISTORY_MOUSE_OVER = 'HISTORY_MOUSE_OVER';
export const HISTORY_MOUSE_OUT = 'HISTORY_MOUSE_OUT';
export const TOGGLE_CHILDREN_EXPANDED = 'TOGGLE_CHILDREN_EXPANDED';
export const CONVERT_LOAD_HISTORY = 'CONVERT_LOAD_HISTORY';

export const historyMouseOver = index => ({
  type: HISTORY_MOUSE_OVER,
  index,
});

export const historyMouseOut = () => ({
  type: HISTORY_MOUSE_OUT,
});

export const toggleChildrenExpanded = index => ({
  type: TOGGLE_CHILDREN_EXPANDED,
  index,
});

export const convertLoadHistory = index => ({
  type: CONVERT_LOAD_HISTORY,
  index,
});
