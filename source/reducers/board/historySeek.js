
import calculateDomains from './util/calculateDomains';

export default (state, action) => {
  let performState = state;
  const histories = state.get('histories');
  const currentIndex = state.get('currentIndex');
  const currentChildIndex = state.get('currentChildIndex');

  if (action.index < currentIndex) {
    if (currentChildIndex > 0) {
      for (let i = currentChildIndex - 1; i >= 0; i--) {
        performState = histories.get(currentIndex - 1).get('actions')[i].undo(performState);
      }
    }
    for (let i = currentIndex - 1; i > action.index; i--) {
      performState = histories.get(i - 1).get('actions').reduce((s, a) => a.undo(s), performState);
    }
    performState = performState.set('currentChildIndex', histories.get(action.index - 1).get('actions').length);
  } else if (action.index > currentIndex) {
    if (currentChildIndex < histories.get(currentIndex - 1).get('actions').length) {
      for (let i = currentChildIndex; i < histories.get(currentIndex - 1).get('actions').length; i++) {
        performState = histories.get(currentIndex - 1).get('actions')[i].perform(performState);
      }
    }
    for (let i = currentIndex; i < action.index; i++) {
      performState = histories.get(i).get('actions').reduce((s, a) => a.perform(s), performState);
    }
    performState = performState.set('currentChildIndex', histories.get(action.index - 1).get('actions').length);
  }

  if (typeof action.childIndex === 'number') {
    for (let i = performState.get('currentChildIndex'); i > action.childIndex; i--) {
      performState = histories.get(action.index - 1).get('actions')[i - 1].undo(performState);
    }
    for (let i = performState.get('currentChildIndex'); i < action.childIndex; i++) {
      performState = histories.get(action.index - 1).get('actions')[i].perform(performState);
    }
    performState = performState.set('currentChildIndex', action.childIndex);
  }

  return calculateDomains(performState.set('currentIndex', action.index));
};
