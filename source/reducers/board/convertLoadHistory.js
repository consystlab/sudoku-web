import Immutable from 'immutable';

import AssignAction from 'models/AssignAction';
import AssignHistoryRecord from 'records/AssignHistoryRecord';

import calculateDomains from './util/calculateDomains';

export default (state, action) => {
  const histories = state.get('histories');
  const currentIndex = state.get('currentIndex');
  const currentChildIndex = state.get('currentChildIndex');
  let performState = state;
  const loadHistoryRecord = histories.get(action.index);

  if (
    (currentIndex - 1) > action.index
    || (currentIndex - 1) === action.index && currentChildIndex === 1
  ) {
    performState = loadHistoryRecord.actions[0].undo(state);
  }

  const assignActions = Object.values(loadHistoryRecord.assignments)
    .map(([variable, assignment]) => new AssignAction(variable, assignment));
  const assignHistoryRecord = new AssignHistoryRecord({
    actions: assignActions.slice(),
    assignActions,
    variablesAffected: Object.values(loadHistoryRecord.assignments)
      .map(([variable]) => variable),
  });

  if (
    (currentIndex - 1) > action.index
    || (currentIndex - 1) === action.index && currentChildIndex === 1
  ) {
    performState = assignActions.reduce(
      (s, a) => a.perform(s),
      performState
    );
  }

  const newChildIndex = (currentIndex - 1) === action.index ?
    currentChildIndex && assignActions.length
    : currentChildIndex;

  return calculateDomains(
    performState.withMutations(s => {
      s.setIn(['histories', action.index], assignHistoryRecord);
      s.set('currentChildIndex', newChildIndex);
      s.set('variablesHovered', Immutable.Map());
    })
  );
};
