import LoadHistoryRecord from 'records/LoadHistoryRecord';
import LoadAction from 'models/LoadAction';
import PuzzleModel from 'models/PuzzleModel';
import calculateDomains from './util/calculateDomains';

export default (state, action) => {
  let performState = state;

  let error = !action.puzzleValues.split('').every(x => !isNaN(parseInt(x, 10)));
  error = error || action.puzzleValues.length !== 81;
  if (error) return performState;

  const currentIndex = state.get('currentIndex');
  const variables = state.get('variables');
  const histories = state.get('histories');
  if (currentIndex > 1) {
    for (let i = currentIndex; i > 1; i--) {
      performState = histories.get(i - 1).get('actions').reduce((s, a) => a.undo(s), performState);
    }
  }

  const values = [];
  const nums = action.puzzleValues.split('').map(v => parseInt(v, 10));
  nums.forEach((num, i) => {
    const x = i % 9;
    const y = (i - x) / 9;
    if (values[y] == null) {
      values[y] = [];
    }
    values[y][x] = num;
  });

  const loadedPuzzle = new PuzzleModel({
    name: 'Loaded from String',
    values,
  });

  const loadaction = new LoadAction(variables, loadedPuzzle);
  performState = loadaction.perform(performState);
  const assignments = {};
  loadedPuzzle.values.forEach((line, y) => {
    line.forEach((num, x) => {
      if (num !== 0) {
        const variable = variables[y][x];
        assignments[variable.id] = [variable, num];
      }
    });
  });

  return calculateDomains(performState.withMutations(s => {
    s.set('histories', s.get('histories')
      .slice(0, 1)
      .push(new LoadHistoryRecord({
        actions: [loadaction],
        assignments,
        puzzleName: loadedPuzzle.name,
        consistency: loadedPuzzle.consistency,
      }))
    );
    s.set('currentIndex', s.get('histories').size);
    s.set('currentChildIndex', 1);
  }));
};
