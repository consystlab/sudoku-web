import LoadHistoryRecord from 'records/LoadHistoryRecord';
import LoadAction from 'models/LoadAction';
import calculateDomains from './util/calculateDomains';

export default (state, action) => {
  let performState = state;
  const currentIndex = state.get('currentIndex');
  const variables = state.get('variables');
  const histories = state.get('histories');
  if (currentIndex > 1) {
    for (let i = currentIndex; i > 1; i--) {
      performState = histories.get(i - 1).get('actions').reduce((s, a) => a.undo(s), performState);
    }
  }
  const loadaction = new LoadAction(variables, action.puzzle);
  performState = loadaction.perform(performState);
  const assignments = {};
  action.puzzle.values.forEach((line, y) => {
    line.forEach((num, x) => {
      if (num !== 0) {
        const variable = variables[y][x];
        assignments[variable.id] = [variable, num];
      }
    });
  });

  return calculateDomains(performState.withMutations(s => {
    s.set('histories', s.get('histories')
      .slice(0, 1)
      .push(new LoadHistoryRecord({
        actions: [loadaction],
        assignments,
        puzzleName: action.puzzle.name,
        consistency: action.puzzle.consistency,
      }))
    );
    s.set('currentIndex', s.get('histories').size);
    s.set('currentChildIndex', 1);
  }));
};
