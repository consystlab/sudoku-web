import AssignAction from 'models/AssignAction';
import AssignHistoryRecord from 'records/AssignHistoryRecord';
import calculateDomains from './util/calculateDomains';

export default (state, action) => {
  let performState = state;
  const currentIndex = state.get('currentIndex');
  const variables = state.get('variables');
  const histories = state.get('histories');

  // Undo previous states
  if (currentIndex > 1) {
    for (let i = currentIndex; i > 1; i--) {
      performState = histories.get(i - 1).get('actions').reduce((s, a) => a.undo(s), performState);
    }
  }

  // Add new puzzle assignments
  const assignActions = [];
  const variablesAffected = [];
  for (let y = 0; y < 9; y++) {
    for (let x = 0; x < 9; x++) {
      const num = parseInt(action.array[y][x], 10);
      if (!isNaN(num)) {
        const assignAction = new AssignAction(variables[y][x], num);
        assignActions.push(assignAction);
        performState = assignAction.perform(performState);
        variablesAffected.push(variables[y][x]);
      }
    }
  }

  return calculateDomains(performState.withMutations(s => {
    s.set('histories', s.get('histories')
      .slice(0, 1)
      .push(new AssignHistoryRecord({
        actions: assignActions,
        assignActions,
        variablesAffected,
      }))
    );
    s.set('currentIndex', s.get('histories').size);
    s.set('currentChildIndex', assignActions.length);
  }));
};
