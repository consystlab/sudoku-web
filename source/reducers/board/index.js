import Immutable from 'immutable';
import { Propagations } from 'consyst';

import {
  ADD_BEGIN_HISTORY,
  GENERATE_BOARD,
  RESET_CELL_DOMAINS,
  ADD_HISTORY,
  HISTORY_SEEK,
  SET_SOLVING_METHOD,
} from 'actions/atomic/boardActions';

import {
  SET_ROW_HOVERED,
  SET_COL_HOVERED,
  SET_GROUP_HOVERED,
  SET_VARIABLES_HOVERED,
  SET_FOCUSED_CELL_ID,
} from 'actions/atomic/boardUIActions';

import {
  HISTORY_MOUSE_OVER,
  HISTORY_MOUSE_OUT,
  TOGGLE_CHILDREN_EXPANDED,
  CONVERT_LOAD_HISTORY,
} from 'actions/atomic/historyActions';

import {
  LOAD_PUZZLE,
} from 'actions/atomic/puzzleActions';

import {
  SET_PUZZLE_VALUES,
} from 'actions/atomic/submitPuzzleActions';

import {
  SUDOKU_IMAGE_UPLOADED,
} from 'actions/atomic/uploadActions';

import {
  CLEAR_ALL,
} from 'actions/atomic/globalActions';

import GenericHistoryRecord from 'records/GenericHistoryRecord';

import calculateDomains from 'reducers/board/util/calculateDomains';

import historySeek from './historySeek';
import generateBoard from './generateBoard';
import loadPuzzle from './loadPuzzle';
import loadPuzzleString from './loadPuzzleString';
import convertLoadHistory from './convertLoadHistory';
import uploadImage from './uploadImage';

const initialState = Immutable.Map({
  assignments: Immutable.Map(),
  domains: Immutable.Map(),
  domainValueStates: Immutable.Map(),
  cellStates: Immutable.Map(),

  arcRows: Immutable.Map(),
  arcCols: Immutable.Map(),
  arcGroups: Immutable.Map(),
  gacRows: Immutable.Map(),
  gacCols: Immutable.Map(),
  gacGroups: Immutable.Map(),
  sudoku: null,
  variables: null,

  width: 9,
  height: 9,
  groupx: 3,
  groupy: 3,

  // History Info
  histories: Immutable.List(),
  currentIndex: 0,
  currentChildIndex: 0,

  // UI State
  rowHovered: -1,
  colHovered: -1,
  groupHovered: -1,
  focusedCellId: '',
  focusedCellLevel: '',
  variablesHovered: Immutable.Map(),
  errorVariables: Immutable.List(),

  solverMethod: Propagations.None,
});

const board = (state = initialState, action) => {
  switch (action.type) {

  case ADD_BEGIN_HISTORY:
    return state.withMutations(s => {
      s.set('histories', state.get('histories').push(new GenericHistoryRecord({
        title: 'Begin',
      })));
      s.set('currentIndex', s.get('histories').size);
    });

  case GENERATE_BOARD:
    return generateBoard(state, action);

  case RESET_CELL_DOMAINS:
    return state;

  // History Stuff

  case HISTORY_SEEK:
    return historySeek(state, action);

  case HISTORY_MOUSE_OVER:
    return state.set('hoveredIndex', action.index);

  case HISTORY_MOUSE_OUT:
    return state.set('hoveredIndex', null);

  case TOGGLE_CHILDREN_EXPANDED:
    const expanded = !state.get('histories').get(action.index).get('childrenExpanded');
    const newHistory = state.get('histories').get(action.index).set('childrenExpanded', expanded);
    return state.set('histories', state.get('histories').set(action.index, newHistory));

  case LOAD_PUZZLE:
    return loadPuzzle(state, action);

  case ADD_HISTORY:
    return calculateDomains(action.history.get('actions')
      .reduce((s, a) => a.perform(s), state)
      .withMutations(s => {
        s.set('histories', s.get('histories').slice(0, s.get('currentIndex')).push(action.history));
        s.set('currentIndex', s.get('histories').size);
        s.set('currentChildIndex', action.history.get('actions').length);
      }));

  case SUDOKU_IMAGE_UPLOADED:
    return uploadImage(state, action);

  case CONVERT_LOAD_HISTORY:
    return convertLoadHistory(state, action);

  // UI Stuff
  case SET_ROW_HOVERED:
    return state.set('rowHovered', action.row);

  case SET_COL_HOVERED:
    return state.set('colHovered', action.col);

  case SET_GROUP_HOVERED:
    return state.set('groupHovered', action.group);

  case SET_VARIABLES_HOVERED:
    return state.set('variablesHovered', action.variables);

  case SET_FOCUSED_CELL_ID:
    return state.withMutations(s => {
      const currentLevel = s.get('focusedCellLevel');
      if (currentLevel === 'click') {
        if (action.level === 'blur') {
          s.set('focusedCellId', action.cellId);
          s.set('focusedCellLevel', action.level);
        }
      } else {
        s.set('focusedCellId', action.cellId);
        s.set('focusedCellLevel', action.level);
      }
    });

  case SET_SOLVING_METHOD:
    return state.set('solverMethod', action.method);

  case SET_PUZZLE_VALUES:
    return loadPuzzleString(state, action);

  case CLEAR_ALL:
    return generateBoard(initialState, action);

  default:
    return state;
  }
};

export default board;
