import Immutable from 'immutable';
import { Sudoku,
  Variable,
  SparseSet,
  BinaryAllDiff,
  GeneralAllDiff,
} from 'consyst';
import DomainValueStates from 'enumerations/DomainValueStates';
import CellStates from 'enumerations/CellStates';

export default(state, action) => {
  const width = action.width || 9;
  const height = action.height || 9;
  const groupx = action.groupx || 3;
  const groupy = action.groupy || 3;
  const variables = [];
  const binaryConstraints = [];
  const generalConstraints = [];
  const arcCols = [];
  const arcRows = [];
  const arcGroups = [];
  const gacCols = [];
  const gacRows = [];
  const gacGroups = [];

  for (let i = 0; i < height; i++) {
    variables[i] = [];
    for (let j = 0; j < width; j++) {
      const variable = new Variable(new SparseSet([
        1, 2, 3, 4, 5, 6, 7, 8, 9,
      ]), `${i}_${j}`);
      variables[i][j] = variable;
    }
  }

  // Add binary constraints (vertical)
  for (let i = 0; i < width; i++) {
    arcCols[i] = [];
    for (let j = 0; j < height; j++) {
      arcCols[i].push(variables[j][i]);
    }
    for (let j = 0; j < height - 1; j++) {
      for (let k = j + 1; k < height; k++) {
        binaryConstraints.push(new BinaryAllDiff([variables[j][i], variables[k][i]]));
      }
    }
  }
  // Add binary constraints (horizantal)
  for (let i = 0; i < height; i++) {
    arcRows[i] = [];
    for (let j = 0; j < width; j++) {
      arcRows[i].push(variables[i][j]);
    }
    for (let j = 0; j < width - 1; j++) {
      for (let k = j + 1; k < width; k++) {
        binaryConstraints.push(new BinaryAllDiff([variables[i][j], variables[i][k]]));
      }
    }
  }

  // Variable groups
  const xgroups = width / groupx;
  const ygroups = height / groupy;
  for (let i = 0; i < xgroups * ygroups; i++) {
    const group = [];
    for (let j = 0; j < groupx * groupy; j++) {
      const x = (i % xgroups) * groupx + j % groupx;
      const y = Math.floor(i / xgroups) * groupy + Math.floor(j / groupx);
      group.push(variables[y][x]);
    }
    arcGroups[i] = group;
  }

  // Add binary constraints (group)
  for (let i = 0; i < arcGroups.length; i++) {
    for (let j = 0; j < arcGroups[i].length - 1; j++) {
      for (let k = j + 1; k < arcGroups[i].length; k++) {
        binaryConstraints.push(
          new BinaryAllDiff([arcGroups[i][j], arcGroups[i][k]]),
        );
      }
    }
  }

  // Add general constraints (vertical)
  for (let i = 0; i < width; i++) {
    const scope = [];
    for (let j = 0; j < height; j++) {
      scope.push(variables[j][i]);
    }
    const gconstraint = new GeneralAllDiff(scope);
    generalConstraints.push(gconstraint);
    gacCols[i] = gconstraint;
  }

  // Add general constraints (horizantal)
  for (let i = 0; i < height; i++) {
    const scope = [];
    for (let j = 0; j < width; j++) {
      scope.push(variables[i][j]);
    }
    const gconstraint = new GeneralAllDiff(scope);
    generalConstraints.push(gconstraint);
    gacRows[i] = gconstraint;
  }

  // Add general constraints (group)
  for (let i = 0; i < arcGroups.length; i++) {
    const gconstraint = new GeneralAllDiff(arcGroups[i]);
    generalConstraints.push(gconstraint);
    gacGroups.push(gconstraint);
  }

  const variableList = variables.reduce((acc, line) => { acc.push(...line); return acc; }, []);
  const sudoku = new Sudoku(variableList, binaryConstraints, generalConstraints);
  const domains = variableList.reduce((acc, variable) =>
    acc.set(variable.id, Immutable.List(variable.domain.current()))
  , Immutable.Map());

  const domainValueStates = {};
  const cellStates = {};
  for (let i = 0; i < width; i++) {
    for (let j = 0; j < height; j++) {
      domainValueStates[`${i}_${j}`] = {};
      for (let k = 1; k <= 9; k++) {
        domainValueStates[`${i}_${j}`][k] = DomainValueStates.Possible;
      }
      cellStates[`${i}_${j}`] = CellStates.Normal;
    }
  }

  return state.withMutations(s => {
    s.set('domains', domains);
    s.set('arcRows', Immutable.List(arcRows));
    s.set('arcCols', Immutable.List(arcCols));
    s.set('arcGroups', Immutable.List(arcGroups));
    s.set('gacRows', Immutable.List(gacRows));
    s.set('gacCols', Immutable.List(gacCols));
    s.set('gacGroups', Immutable.List(gacGroups));
    s.set('sudoku', sudoku);
    s.set('variables', variables);
    s.set('domainValueStates', Immutable.fromJS(domainValueStates));
    s.set('cellStates', Immutable.Map(cellStates));
  });
};
