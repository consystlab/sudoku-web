import Immutable from 'immutable';
import _ from 'lodash';

import DomainValueStates from 'enumerations/DomainValueStates';

export default state => {
  const domains = state.get('sudoku').variables.reduce((acc, variable) =>
    acc.set(variable.id, Immutable.List(variable.domain.current()))
  , Immutable.Map());
  const assignments = state.get('sudoku').variables.reduce((acc, variable) => {
    if (variable.isAssigned()) {
      return acc.set(variable.id, variable.getAssignment());
    }
    return acc;
  }, Immutable.Map());

  // Calculate DomainValueStates
  let performState = state;
  const currentIndex = performState.get('currentIndex');
  const maxChildIndex = performState.get('histories').get(currentIndex - 1).get('actions').length;
  if (performState.get('currentChildIndex') < maxChildIndex) {
    const removedThisStep = performState.get('histories')
      .get(currentIndex - 1)
      .get('actions')
      .reduce((acc, a) => {
        if (!a.eliminated) {
          return acc;
        }
        a.eliminated.forEach((variable, removed) => {
          if (!acc[variable.id]) acc[variable.id] = {};
          removed.forEach(v => {
            acc[variable.id][v] = true;
          });
        });
        return acc;
      }, {});

    performState = performState.withMutations(s => {
      _.range(0, 9).forEach(row => {
        _.range(0, 9).forEach(col => {
          const variable = s.get('variables')[row][col];
          const removedThisStepVariable = removedThisStep[variable.id];
          let domainStates = s.get('domainValueStates').get(variable.id);
          _.range(1, 10).forEach(v => {
            if (!variable.domain.contains(v)) {
              domainStates = domainStates.set(v, DomainValueStates.Removed);
            } else if (removedThisStepVariable && removedThisStepVariable[v]) {
              domainStates = domainStates.set(v, DomainValueStates.ToBeRemoved);
            } else {
              domainStates = domainStates.set(v, DomainValueStates.Possible);
            }
          });
          s.set('domainValueStates', s.get('domainValueStates').set(variable.id, domainStates));
        });
      });

      const childIndex = s.get('currentChildIndex');
      const eliminated = s.get('histories').get(currentIndex - 1).get('actions')[childIndex].eliminated || [];
      eliminated.forEach((variable, removed) => {
        let domainValueStates = s.get('domainValueStates').get(variable.id);
        removed.forEach(v => {
          domainValueStates = domainValueStates.set(v, DomainValueStates.BeingRemoved);
        });
        s.set('domainValueStates', s.get('domainValueStates').set(variable.id, domainValueStates));
      });
    });
  } else {
    performState = performState.withMutations(s => {
      _.range(0, 9).forEach(row => {
        _.range(0, 9).forEach(col => {
          const variable = s.get('variables')[row][col];
          let domainStates = s.get('domainValueStates').get(variable.id);
          _.range(1, 10).forEach(v => {
            if (!variable.domain.contains(v)) {
              domainStates = domainStates.set(v, DomainValueStates.Removed);
            } else {
              domainStates = domainStates.set(v, DomainValueStates.Possible);
            }
          });
          s.set('domainValueStates', s.get('domainValueStates').set(variable.id, domainStates));
        });
      });
    });
  }

  return performState.withMutations(s => {
    s.set('domains', domains);
    s.set('assignments', assignments);
  });
};
