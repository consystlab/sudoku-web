import Immutable from 'immutable';
import moment from 'moment';

import SolutionsHistoryRecord from 'records/SolutionsHistoryRecord';

import {
  SET_PUZZLE_NAME,
  SET_SUDOKU_TYPE,
  SET_SOURCE,
  SET_MIN_DIFF_LVL,
  SET_DIFF_LVL,
  SET_MAX_DIFF_LVL,
  SET_ORIG_NAME,
  SET_DESCRIPTION,
  SET_PUZZLE_VALUES,
  SET_SUBMIT_SOLUTIONS_RECORD,
  SET_CONSISTENCY,
  SET_SUBMIT_MAX_SOLUTIONS,
  SET_SUBMIT_STATE,
  PRELOAD_SUBMIT_PUZZLE,
} from 'actions/atomic/submitPuzzleActions';

import {
  LOAD_PUZZLE,
} from 'actions/atomic/puzzleActions';

import {
  SUDOKU_IMAGE_UPLOADED,
} from 'actions/atomic/uploadActions';

import {
  CLEAR_ALL,
} from 'actions/atomic/globalActions';

import SubmitStates from 'enumerations/SubmitStates';

let EMPTY_PUZZLE = '';
for (let i = 0; i < 81; i++) EMPTY_PUZZLE += '0';

const initialState = Immutable.Map({
  puzzleName: '',
  sudokuType: 'Base',
  source: '',
  minDiffLvl: '1',
  minDiffLvlError: false,
  diffLvl: '1',
  diffLvlError: false,
  maxDiffLvl: '5',
  maxDiffLvlError: false,
  origName: '',
  description: '',
  puzzleValues: EMPTY_PUZZLE,
  puzzleValuesError: false,
  solutionsRecord: new SolutionsHistoryRecord(),
  maxSolutions: 50,
  consistency: '',
  submitState: SubmitStates['NOTHING'],
});

const controlPanel = (state = initialState, action) => {
  switch (action.type) {

  case SET_PUZZLE_NAME:
    return state.set('puzzleName', action.name);

  case SET_SUDOKU_TYPE:
    return state.set('sudokuType', action.sudokuType);

  case SET_SOURCE:
    return state.set('source', action.source);

  case SET_MIN_DIFF_LVL:
    return state.withMutations(s => {
      s.set('minDiffLvl', action.level);
      const num = parseInt(action.level, 10);
      if (!isNaN(num) && `${num}` === action.level && num > 0) {
        s.set('minDiffLvlError', false);
      } else {
        s.set('minDiffLvlError', true);
      }
    });

  case SET_DIFF_LVL:
    return state.withMutations(s => {
      s.set('diffLvl', action.level);
      const num = parseInt(action.level, 10);
      if (!isNaN(num) && `${num}` === action.level && num > 0) {
        s.set('diffLvlError', false);
      } else {
        s.set('diffLvlError', true);
      }
    });

  case SET_MAX_DIFF_LVL:
    return state.withMutations(s => {
      s.set('maxDiffLvl', action.level);
      const num = parseInt(action.level, 10);
      if (!isNaN(num) && `${num}` === action.level && num > 0) {
        s.set('maxDiffLvlError', false);
      } else {
        s.set('maxDiffLvlError', true);
      }
    });

  case SET_ORIG_NAME:
    return state.set('origName', action.name);

  case SET_DESCRIPTION:
    return state.set('description', action.description);

  case SET_PUZZLE_VALUES:
    return state.withMutations(s => {
      s.set('puzzleValues', action.puzzleValues);
      let error = !action.puzzleValues.split('').every(x => !isNaN(parseInt(x, 10)));
      error = error || action.puzzleValues.length !== 81;
      s.set('puzzleValuesError', error);
    });

  case SET_SUBMIT_SOLUTIONS_RECORD:
    return state.set('solutionsRecord', action.solutionsRecord);

  case SET_CONSISTENCY:
    return state.set('consistency', action.consistency);

  case SET_SUBMIT_MAX_SOLUTIONS:
    return state.set('maxSolutions', action.max);

  case SET_SUBMIT_STATE:
    return state.withMutations(s => {
      s.set('submitState', action.state);
    });

  case PRELOAD_SUBMIT_PUZZLE:
    return state.withMutations(s => {
      s.set('puzzleName', action.puzzle.name);
      s.set('sudokuType', action.puzzle.type);
      s.set('source', action.puzzle.source);
      s.set('minDiffLvl', `${action.puzzle.mindifflvl}`);
      s.set('minDiffLvlError', false);
      s.set('diffLvl', `${action.puzzle.difflvl}`);
      s.set('diffLvlError', false);
      s.set('maxDiffLvl', `${action.puzzle.maxdifflvl}`);
      s.set('maxDiffLvlError', false);
      s.set('origName', action.puzzle.origname);
      s.set('description', action.puzzle.description);
      s.set('consistency', action.puzzle.consistency);
    });

  case LOAD_PUZZLE:
    return state.withMutations(s => {
      const values = action.puzzle.values.map(row => row.join('')).join('');
      s.set('puzzleName', action.puzzle.name);
      s.set('sudokuType', action.puzzle.type);
      s.set('source', action.puzzle.source);
      s.set('minDiffLvl', `${action.puzzle.mindifflvl}`);
      s.set('minDiffLvlError', false);
      s.set('diffLvl', `${action.puzzle.difflvl}`);
      s.set('diffLvlError', false);
      s.set('maxDiffLvl', `${action.puzzle.maxdifflvl}`);
      s.set('maxDiffLvlError', false);
      s.set('origName', action.puzzle.origname);
      s.set('description', action.puzzle.description);
      s.set('puzzleValues', values);
      s.set('puzzleValuesError', false);
      s.set('consistency', action.puzzle.consistency);

      s.set('solutionsRecord', new SolutionsHistoryRecord());
    });

  case SUDOKU_IMAGE_UPLOADED:
    return initialState.withMutations(s => {
      s.set('puzzleName', `${moment().format('MM-DD-YYYY-HHmmss')}-OCR`);
      s.set('puzzleValues', state.get('puzzleValues'));
    });

  case CLEAR_ALL:
    return initialState;

  default:
    return state;
  }
};

export default controlPanel;
