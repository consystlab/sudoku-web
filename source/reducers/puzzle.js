import Immutable from 'immutable';

import {
  PARSE_PUZZLES,
  SET_PUZZLE_FILTER,
  UPDATE_SORT_FIELD,
  SET_SELECTOR_VISIBILITY,
  SET_COLUMN_VISIBILITY,
} from 'actions/atomic/puzzleActions';

import parsePuzzles from 'util/parsePuzzles';

/* eslint-disable */
const consistencyOrder = {
  'AC': 0,
  'GAC': 1,
  'SAC': 2,
  'SAC + GAC': 3,
  'SGAC': 4,
  'POAC': 5,
  'SGAC + POAC': 6,
  'POGAC': 7,
  'BiSAC': 8,
  'POGAC + BiSAC': 9,
  'BiSGAC': 10,
  'SSAC': 11,
  'BiSGAC + SSAC': 12,
  'SSGAC': 13,
  'Unsolved': 14,
};
/* eslint-enable */

const initialState = Immutable.Map({
  puzzles: Immutable.List(),
  types: Immutable.List(),
  maxLevels: Immutable.List(),
  levels: Immutable.List(),
  numbersOfClues: Immutable.List(),
  numbersOfSolutions: Immutable.List(),
  consistencies: Immutable.List(),

  type: null,
  maxLevel: null,
  level: null,
  numberOfClues: null,
  numberOfSolutions: null,
  consistency: null,

  sortField: null,
  sortDirection: null,

  showColumnSelector: false,
  nameVisible: true,
  consistencyVisible: true,
  cluesVisible: true,
  solutionsVisible: true,
  levelVisible: true,
  sourceVisible: true,
});

const puzzle = (state = initialState, action) => {
  switch (action.type) {

  case PARSE_PUZZLES:
    const {
      puzzles,
      types,
      consistencies,
      numbersOfClues,
      numbersOfSolutions,
      maxLevels,
      levels,
    } = parsePuzzles(action.lines);
    types.sort();
    maxLevels.sort((a, b) => a - b);
    levels.sort((a, b) => a - b);
    numbersOfClues.sort((a, b) => a - b);
    numbersOfSolutions.sort((a, b) => a - b);
    consistencies.sort((a, b) => consistencyOrder[a] - consistencyOrder[b]);
    return state.withMutations(s => {
      s.set('puzzles', Immutable.List(puzzles));
      s.set('types', Immutable.List(types));
      s.set('maxLevels', Immutable.List(maxLevels));
      s.set('levels', Immutable.List(levels));
      s.set('numbersOfClues', Immutable.List(numbersOfClues));
      s.set('numbersOfSolutions', Immutable.List(numbersOfSolutions));
      s.set('consistencies', Immutable.List(consistencies));
    });

  case SET_PUZZLE_FILTER:
    return state.set(action.puzzleFilter, action.value);

  case UPDATE_SORT_FIELD:
    return state.withMutations(s => {
      let direction = 1;
      if (s.get('sortField') === action.field) {
        direction = (s.get('sortDirection') + 1) % 3;
      }
      s.set('sortField', action.field);
      s.set('sortDirection', direction);
    });

  case SET_SELECTOR_VISIBILITY:
    return state.set('showColumnSelector', action.visible);

  case SET_COLUMN_VISIBILITY:
    return state.set(`${action.column}Visible`, action.visible);

  default:
    return state;
  }
};

export default puzzle;
