import Immutable from 'immutable';
import Cookie from 'js-cookie';

import {
  TOGGLE_SHOW_EXPLANATIONS,
  SET_MAX_SAVE_RESULTS_TEXT,
  SET_MAX_SEARCH_RESULTS_TEXT,
  SET_SOLUTIONS_DIFF,
} from 'actions/atomic/solverSettingsActions';

const MAX_SAVE_RESULTS = 100;

const showExplanations = Cookie.get('showExplanations') === undefined ? true :
  (Cookie.get('showExplanations') === 'true');
const maxSearchResults = Cookie.get('maxSearchResults') === undefined ? 50 :
  parseInt(Cookie.get('maxSearchResults'), 10);
const maxSaveResults = Cookie.get('maxSaveResults') === undefined ? 50 :
  parseInt(Cookie.get('maxSaveResults'), 10);
const solutionsDiff = Cookie.get('solutionsDiff') === undefined ? '2' :
  Cookie.get('solutionsDiff');

const initialState = Immutable.Map({
  showExplanations,

  maxSearchResults,
  maxSearchResultsText: `${maxSearchResults}`,
  maxSearchResultsError: false,

  maxSaveResults,
  maxSaveResultsText: `${maxSaveResults}`,
  maxSaveResultsError: false,

  solutionsDiff,
});

const setCookies = (state) => {
  Cookie.set('showExplanations', state.get('showExplanations'), { expires: 100 });
  Cookie.set('maxSearchResults', state.get('maxSearchResults'), { expires: 100 });
  Cookie.set('maxSaveResults', state.get('maxSaveResults'), { expires: 100 });
  Cookie.set('solutionsDiff', state.get('solutionsDiff'), { expires: 100 });
  return state;
};

const controlPanel = (state = initialState, action) => {
  switch (action.type) {

  case TOGGLE_SHOW_EXPLANATIONS:
    return setCookies(state.set('showExplanations', !state.get('showExplanations')));

  case SET_MAX_SAVE_RESULTS_TEXT:
    return setCookies(state.withMutations(s => {
      s.set('maxSaveResultsText', action.maxSaveResultsText);
      const num = parseInt(action.maxSaveResultsText, 10);
      if (!isNaN(num) && num > 0 && num <= MAX_SAVE_RESULTS) {
        s.set('maxSaveResults', num);
        s.set('maxSaveResultsError', false);
      } else {
        s.set('maxSaveResultsError', true);
      }
      return state;
    }));

  case SET_MAX_SEARCH_RESULTS_TEXT:
    return setCookies(state.withMutations(s => {
      s.set('maxSearchResultsText', action.maxSearchResultsText);
      const num = parseInt(action.maxSearchResultsText, 10);
      if (!isNaN(num) && num > 0) {
        s.set('maxSearchResults', num);
        s.set('maxSearchResultsError', false);
      } else {
        s.set('maxSearchResultsError', true);
      }
      return state;
    }));

  case SET_SOLUTIONS_DIFF:
    return setCookies(state.set('solutionsDiff', action.diff));

  default:
    return setCookies(state);
  }
};

export default controlPanel;
