import { combineReducers } from 'redux';

import puzzle from 'reducers/puzzle';

import board from 'reducers/board';
import controlPanel from 'reducers/controlPanel';
import propagations from 'reducers/propagations';
import submitPuzzle from 'reducers/submitPuzzle';
import solverSettings from 'reducers/solverSettings';
import upload from 'reducers/upload';

const sudokuApp = combineReducers({
  puzzle,

  board,
  controlPanel,
  propagations,
  submitPuzzle,
  solverSettings,
  upload,
});
export default sudokuApp;
