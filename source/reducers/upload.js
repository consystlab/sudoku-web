import Immutable from 'immutable';

import {
  SET_CANVAS_DATA,
} from 'actions/atomic/uploadActions';

const initialState = Immutable.Map({
  canvasData: null,
});

const controlPanel = (state = initialState, action) => {
  switch (action.type) {

  case SET_CANVAS_DATA:
    return state.set('canvasData', action.data);

  default:
    return state;
  }
};

export default controlPanel;
