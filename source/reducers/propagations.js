import Immutable from 'immutable';

import {
  SET_PROPAGATION_POSITION,
  SET_PROPAGATION_STYLE,
} from 'actions/atomic/propagationActions';

const initialState = Immutable.Map({
  positions: Immutable.Map(),
  styles: Immutable.Map(),
});

const propagations = (state = initialState, action) => {
  switch (action.type) {

  case SET_PROPAGATION_POSITION:
    return state.setIn(['positions', action.propagation], action.position);

  case SET_PROPAGATION_STYLE:
    return state.setIn(['styles', action.propagation], action.style);
  default:
    return state;
  }
};

export default propagations;
