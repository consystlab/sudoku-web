import Immutable from 'immutable';
import { Propagations } from 'consyst';

import {
  SET_CONTROL_SECTION,
  TOGGLE_MINIS_VISIBLE,
  CALL_REMOVE_DIALOG,
  HIDE_REMOVE_DIALOG,
  TOGGLE_AUTO_SINGLETON,
} from 'actions/atomic/controlPanelActions';

const initialState = Immutable.Map({
  selectedTab: 0,
  minisVisible: true,
  autoSingleton: false,
  removeDialogRect: null,
  removeDialogRow: null,
  removeDialogCol: null,
  removeDialogVisible: false,

  solverMethod: Propagations.None,
});

const controlPanel = (state = initialState, action) => {
  switch (action.type) {

  case SET_CONTROL_SECTION:
    return state.set('selectedTab', action.index);

  case TOGGLE_MINIS_VISIBLE:
    return state.set('minisVisible', !state.get('minisVisible'));

  case TOGGLE_AUTO_SINGLETON:
    return state.set('autoSingleton', !state.get('autoSingleton'));

  case CALL_REMOVE_DIALOG:
    return state.withMutations(s => {
      s.set('removeDialogRect', action.clientRect);
      s.set('removeDialogRow', action.row);
      s.set('removeDialogCol', action.col);
      s.set('removeDialogVisible', true);
    });

  case HIDE_REMOVE_DIALOG:
    return state.withMutations(s => {
      s.set('removeDialogVisible', false);
      s.set('removeDialogRect', {});
      s.set('removeDialogRow', null);
      s.set('removeDialogCol', null);
    });

  default:
    return state;
  }
};

export default controlPanel;
