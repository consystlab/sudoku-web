import React from 'react';
import PropTypes from 'prop-types';

import cn from 'classnames';
import styles from './style.scss';

function SimpleCell(props) {
  return (
    <div key={`cell-${props.id}`}
      className={cn(styles['cell-holder'], props.className)}
    >
      <div className={styles['cell']}>
        {props.assignment}
      </div>
    </div>
  );
}

SimpleCell.propTypes = {
  id: PropTypes.string,
  assignment: PropTypes.number,
  className: PropTypes.string,
};

export default SimpleCell;
