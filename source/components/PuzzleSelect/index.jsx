import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';

import Button from 'components/Button';

import Puzzle from 'components/Puzzle';
import cn from 'classnames';
import styles from './style.scss';

export default class PuzzleSelect extends Component {
  static propTypes = {
    puzzles: ImmutablePropTypes.list,
    type: PropTypes.string,
    maxLevel: PropTypes.number,
    level: PropTypes.number,
    numberOfSolutions: PropTypes.number,
    numberOfClues: PropTypes.number,
    consistency: PropTypes.string,

    types: ImmutablePropTypes.list,
    maxLevels: ImmutablePropTypes.list,
    levels: ImmutablePropTypes.list,
    numbersOfSolutions: ImmutablePropTypes.list,
    numbersOfClues: ImmutablePropTypes.list,
    consistencies: ImmutablePropTypes.list,

    sortField: PropTypes.string,
    sortDirection: PropTypes.number,

    showColumnSelector: PropTypes.bool,
    nameVisible: PropTypes.bool,
    consistencyVisible: PropTypes.bool,
    cluesVisible: PropTypes.bool,
    solutionsVisible: PropTypes.bool,
    levelVisible: PropTypes.bool,
    sourceVisible: PropTypes.bool,

    loadPuzzle: PropTypes.func.isRequired,
    updateSortField: PropTypes.func.isRequired,
    setPuzzleFilter: PropTypes.func.isRequired,

    setSelectorVisibility: PropTypes.func.isRequired,
    setColumnVisibility: PropTypes.func.isRequired,
  };

  componentWillReceiveProps = (newProps) => {
    if (!this.props.showColumnSelector && newProps.showColumnSelector) {
      window.addEventListener('click', this.hideColumnSelector);
    }
  }

  hideColumnSelector = (e) => {
    if (!$(e.target).parents('#column-selector-container').length) {
      window.removeEventListener('click', this.hideColumnSelector);
      this.props.setSelectorVisibility(false);
    }
  }

  typeChangeHandler = (e) => this.props.setPuzzleFilter('type', e.target.value);
  maxLevelChangeHandler = (e) => this.props.setPuzzleFilter('maxLevel', parseInt(e.target.value, 10));
  levelChangeHandler = (e) => this.props.setPuzzleFilter('level', parseInt(e.target.value, 10));
  solutionsChangeHandler = (e) => this.props.setPuzzleFilter('numberOfSolutions', parseInt(e.target.value, 10));
  cluesChangeHandler = (e) => this.props.setPuzzleFilter('numberOfClues', parseInt(e.target.value, 10));
  consistencyChangeHandler = (e) => this.props.setPuzzleFilter('consistency', e.target.value);

  render() {
    let nameClass = 'minus-sign';
    let levelClass = 'minus-sign';
    let sourceClass = 'minus-sign';
    let solutionsClass = 'minus-sign';
    let cluesClass = 'minus-sign';
    let consistencyClass = 'minus-sign';

    if (this.props.sortDirection === 1) {
      switch (this.props.sortField) {
      case 'name': nameClass = 'circle-arrow-up'; break;
      case 'level': levelClass = 'circle-arrow-up'; break;
      case 'source': sourceClass = 'circle-arrow-up'; break;
      case 'solutions': solutionsClass = 'circle-arrow-up'; break;
      case 'clues': cluesClass = 'circle-arrow-up'; break;
      case 'consistency': consistencyClass = 'circle-arrow-up'; break;
      default:
      }
    } else if (this.props.sortDirection === 2) {
      switch (this.props.sortField) {
      case 'name': nameClass = 'circle-arrow-down'; break;
      case 'level': levelClass = 'circle-arrow-down'; break;
      case 'source': sourceClass = 'circle-arrow-down'; break;
      case 'solutions': solutionsClass = 'circle-arrow-down'; break;
      case 'clues': solutionsClass = 'circle-arrow-down'; break;
      case 'consistency': consistencyClass = 'circle-arrow-down'; break;
      default:
      }
    }
    return (
      <div className={styles['puzzle-select-panel']}>
        <div className={styles['header-container']}>
          <div
            className={styles['column-selector-container']}
            id="column-selector-container"
          >
            <div className={styles['select-button-container']}>
              <Button
                className={styles['column-select-button']}
                clickHandler={() => { this.props.setSelectorVisibility(!this.props.showColumnSelector); }}
              >
                Columns
              </Button>
            </div>
            {
              this.props.showColumnSelector ? (
                <div className={styles['column-selector']}>
                  <div className={styles['attribute-selector']}>
                    <input
                      type="checkbox"
                      checked={this.props.nameVisible}
                      onChange={() => { this.props.setColumnVisibility('name', !this.props.nameVisible); }}
                    />
                    <div className={styles['check-text']}>
                      Name
                    </div>
                  </div>
                  <div className={styles['attribute-selector']}>
                    <input
                      type="checkbox"
                      checked={this.props.consistencyVisible}
                      onChange={() => {
                        this.props.setColumnVisibility('consistency', !this.props.consistencyVisible);
                      }}
                    />
                    <div className={styles['check-text']}>
                      Consistency
                    </div>
                  </div>
                  <div className={styles['attribute-selector']}>
                    <input
                      type="checkbox"
                      checked={this.props.cluesVisible}
                      onChange={() => { this.props.setColumnVisibility('clues', !this.props.cluesVisible); }}
                    />
                    <div className={styles['check-text']}>
                      #Clues
                    </div>
                  </div>
                  <div className={styles['attribute-selector']}>
                    <input
                      type="checkbox"
                      checked={this.props.solutionsVisible}
                      onChange={() => { this.props.setColumnVisibility('solutions', !this.props.solutionsVisible); }}
                    />
                    <div className={styles['check-text']}>
                      #Solutions
                    </div>
                  </div>
                  <div className={styles['attribute-selector']}>
                    <input
                      type="checkbox"
                      checked={this.props.levelVisible}
                      onChange={() => { this.props.setColumnVisibility('level', !this.props.levelVisible); }}
                    />
                    <div className={styles['check-text']}>
                      Level
                    </div>
                  </div>
                  <div className={styles['attribute-selector']}>
                    <input
                      type="checkbox"
                      checked={this.props.sourceVisible}
                      onChange={() => { this.props.setColumnVisibility('source', !this.props.sourceVisible); }}
                    />
                    <div className={styles['check-text']}>
                      Source
                    </div>
                  </div>
                </div>
              ) : null
            }
          </div>
        </div>
        <div className={styles['select-table-container']}>
          <div className={styles['select-table']}>
            <div className={styles['table-header']}>
              {
                this.props.nameVisible ? (
                  <div className={styles['col-name']}>
                    <div className={styles['field-title']}
                      onClick={() => { this.props.updateSortField('name'); }}
                    >
                      Puzzle Name
                      <div className={cn(styles['sort-glyph'], 'glyphicon', `glyphicon-${nameClass}`)}
                        aria-hidden="true"
                      />
                    </div>
                    <div className={styles['field-filter']}>
                      Puzzles {this.props.puzzles.size}
                      <select
                        className={styles['type-select']}
                        onChange={this.typeChangeHandler}
                        value={this.props.type || ''}
                      >
                        <option value={null}>*</option>
                        {
                          this.props.types.map((type) => (
                            <option key={`type-${type}`} value={type}>{type}</option>
                          ))
                        }
                      </select>
                    </div>
                  </div>
                ) : null
              }
              {
                this.props.consistencyVisible ? (
                  <div className={styles['col-const']}>
                    <div className={styles['field-title']}
                      onClick={() => { this.props.updateSortField('consistency'); }}
                    >
                      Const
                      <div className={cn(styles['sort-glyph'], 'glyphicon', `glyphicon-${consistencyClass}`)}
                        aria-hidden="true"
                      />
                    </div>
                    <div className={styles['field-filter']}>
                      <select onChange={this.consistencyChangeHandler} value={this.props.consistency || ''}>
                        <option value={null}>*</option>
                        {
                          this.props.consistencies.map((consistency) => (
                            <option key={`consistency-${consistency}`} value={consistency}>{consistency}</option>
                          ))
                        }
                      </select>
                    </div>
                  </div>
                ) : null
              }
              {
                this.props.cluesVisible ? (
                  <div className={styles['col-clues']}>
                    <div className={styles['field-title']}
                      onClick={() => { this.props.updateSortField('clues'); }}
                    >
                      #Clues
                      <div className={cn(styles['sort-glyph'], 'glyphicon', `glyphicon-${cluesClass}`)}
                        aria-hidden="true"
                      />
                    </div>
                    <div className={styles['field-filter']}>
                      <select onChange={this.cluesChangeHandler} value={this.props.numberOfClues || ''}>
                        <option value={null}>*</option>
                        {
                          this.props.numbersOfClues.map((clues) => (
                            <option key={`clues-${clues}`} value={clues}>{clues}</option>
                          ))
                        }
                      </select>
                    </div>
                  </div>
                ) : null
              }
              {
                this.props.solutionsVisible ? (
                  <div className={styles['col-solutions']}>
                    <div className={styles['field-title']}
                      onClick={() => { this.props.updateSortField('solutions'); }}
                    >
                      #Sol
                      <div className={cn(styles['sort-glyph'], 'glyphicon', `glyphicon-${solutionsClass}`)}
                        aria-hidden="true"
                      />
                    </div>
                    <div className={styles['field-filter']}>
                      <select onChange={this.solutionsChangeHandler} value={this.props.numberOfSolutions || ''}>
                        <option value={null}>*</option>
                        {
                          this.props.numbersOfSolutions.map((solutions) => (
                            <option key={`solutions-${solutions}`} value={solutions}>{solutions}</option>
                          ))
                        }
                      </select>
                    </div>
                  </div>
                ) : null
              }
              {
                this.props.levelVisible ? (
                  <div className={styles['col-level']}>
                    <div className={styles['field-title']}
                      onClick={() => { this.props.updateSortField('level'); }}
                    >
                      Level
                      <div className={cn(styles['sort-glyph'], 'glyphicon', `glyphicon-${levelClass}`)}
                        aria-hidden="true"
                      />
                    </div>
                    <div className={styles['field-filter']}>
                      <select onChange={this.levelChangeHandler} value={this.props.level || ''}>
                        <option value={null}>*</option>
                        {
                          this.props.levels.map((level) => (
                            <option key={`level-${level}`} value={level}>{level}</option>
                          ))
                        }
                      </select>
                      /
                      <select onChange={this.maxLevelChangeHandler} value={this.props.maxLevel || ''}>
                        <option value={null}>*</option>
                        {
                          this.props.maxLevels.map((maxLevel) => (
                            <option key={`maxLevel-${maxLevel}`} value={maxLevel}>{maxLevel}</option>
                          ))
                        }
                      </select>
                    </div>
                  </div>
                ) : null
              }
              {
                this.props.sourceVisible ? (
                  <div className={styles['col-source']}>
                    <div className={styles['field-title']}
                      onClick={() => { this.props.updateSortField('source'); }}
                    >
                      Source
                      <div className={cn(styles['sort-glyph'], 'glyphicon', `glyphicon-${sourceClass}`)}
                        aria-hidden="true"
                      />
                    </div>
                  </div>
                ) : null
              }
            </div>
            <div className={styles['table-body']}>
              <div className={styles['table-list']}>
                {
                  this.props.puzzles.map((puzzle, i) => (
                    <Puzzle key={`puzzle-${puzzle.name}-${i}`}
                      puzzle={puzzle}
                      nameVisible={this.props.nameVisible}
                      consistencyVisible={this.props.consistencyVisible}
                      cluesVisible={this.props.cluesVisible}
                      solutionsVisible={this.props.solutionsVisible}
                      levelVisible={this.props.levelVisible}
                      sourceVisible={this.props.sourceVisible}
                      clickHandler={() => { this.props.loadPuzzle(puzzle); }}
                    />
                  ))
                }
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
