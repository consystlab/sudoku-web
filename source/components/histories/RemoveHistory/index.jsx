import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Immutable from 'immutable';
import { Glyphicon } from 'react-bootstrap';

import Button from 'components/Button';
import Slider from 'components/Slider';
import propagationText from 'util/propagationText';

import cn from 'classnames';
import styles from './style.scss';

export default class RemoveHistory extends Component {
  static propTypes = {
    index: PropTypes.number.isRequired,
    currentIndex: PropTypes.number.isRequired,
    currentChildIndex: PropTypes.number,
    removeHistoryModel: PropTypes.object,

    historySeek: PropTypes.func.isRequired,
    setVariablesHovered: PropTypes.func.isRequired,
    childrenExpanderClickHandler: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);

    this.totalRemoved = props.removeHistoryModel.removeActions.reduce((aRemoved, action) =>
      aRemoved + action.eliminated.reduceEntries((eRemoved, eliminated) =>
        eRemoved + eliminated.removed.size, 0)
      , 0);
  }

  componentWillReceiveProps(newProps) {
    if (newProps.removeHistoryModel !== this.props.removeHistoryModel) {
      this.totalRemoved = newProps.removeHistoryModel.removeActions.reduce((aRemoved, action) =>
        aRemoved + action.eliminated.reduceEntries((eRemoved, eliminated) =>
          eRemoved + eliminated.removed.size, 0)
        , 0);
    }
  }

  setHoveredVariables = (childIndex) => {
    if (childIndex < this.props.removeHistoryModel.actions.length) {
      const eliminated = this.props.removeHistoryModel.actions[childIndex].eliminated;
      if (eliminated) {
        const constraintVariables = eliminated.getConstraintVariables();
        let hoveredVariables;
        if (constraintVariables) {
          hoveredVariables = Array.from(constraintVariables).reduce(
            (acc, variable) =>
              acc.set(variable.id, true)
            , Immutable.Map()
          );
        } else {
          hoveredVariables = eliminated.reduceEntries(
            (acc, { variable }) =>
              acc.set(variable.id, true)
            , Immutable.Map()
          );
        }
        this.props.setVariablesHovered(hoveredVariables);
      }
    } else {
      this.historyMouseOverHandler();
    }
  }

  childrenExpanderClickHandler = (e) => {
    e.stopPropagation();
    this.props.childrenExpanderClickHandler(this.props.index);
  }

  historyClickHandler = () => {
    this.props.historySeek(this.props.index);
  }

  historyMouseOverHandler = () => {
    this.props.setVariablesHovered(this.props.removeHistoryModel.variablesAffected.reduce((acc, val) =>
      acc.set(val.id, true)
    , Immutable.Map()));
  }

  historyMouseOutHandler = () => {
    this.props.setVariablesHovered(Immutable.Map());
  }

  sliderMouseOverHandler = () => {
    this.setHoveredVariables(this.props.currentChildIndex);
  }

  sliderMouseOutHandler = () => {
    this.props.setVariablesHovered(Immutable.Map());
  }

  moveLocalIndexTo = (newIndex) => {
    this.props.historySeek(this.props.index, newIndex);
    this.setHoveredVariables(newIndex);
  }

  render() {
    const seekBefore = this.props.currentIndex < this.props.index;
    const methodString = propagationText(
      this.props.removeHistoryModel.method,
      this.props.removeHistoryModel.rowColOrGroup
    );

    let progressIndex;
    if (this.props.currentIndex < this.props.index) {
      progressIndex = 0;
    } else if (this.props.currentIndex > this.props.index) {
      progressIndex = this.props.removeHistoryModel.actions.length;
    } else {
      progressIndex = this.props.currentChildIndex;
    }

    const glyphSign = this.props.removeHistoryModel.childrenExpanded ? 'minus-sign' : 'plus-sign';
    return (
      <div className={styles['history-container']}>
        <div className={cn(styles['history'], { [styles['not-in-use']]: seekBefore })}
          onMouseOver={this.historyMouseOverHandler}
          onMouseOut={this.historyMouseOutHandler}
          onClick={this.historyClickHandler}
        >
          <div className={styles['history-title']}>
            {methodString} removed {this.totalRemoved} values
          </div>
          {
            this.totalRemoved !== 0 ? (
              <Glyphicon glyph={glyphSign}
                className={styles['expansion-glyph']}
                value="false"
                onClick={this.childrenExpanderClickHandler}
              />
            ) : null
          }
        </div>
        {
          this.totalRemoved !== 0 ? (
            <div className={cn(
                styles['history-children'],
                { [styles.hide]: !this.props.removeHistoryModel.childrenExpanded }
              )}
              onClick={this.historyClickHandler}
              onMouseOver={this.sliderMouseOverHandler}
              onMouseOut={this.sliderMouseOutHandler}
            >
              {
                this.props.removeHistoryModel.actions.length === 1 ? (
                  <div className={styles['ba-container']}>
                    <Button
                      className={styles['ba-button']}
                      clickHandler={() => { this.moveLocalIndexTo(0); }}
                    >
                      Before
                    </Button>
                    <Button
                      className={styles['ba-button']}
                      clickHandler={() => { this.moveLocalIndexTo(1); }}
                    >
                      After
                    </Button>
                  </div>
                ) : (
                  <Slider
                    min={0}
                    max={this.props.removeHistoryModel.actions.length}
                    now={progressIndex}
                    active={this.props.index === this.props.currentIndex}

                    mouseOverHandler={this.sliderMouseOverHandler}
                    mouseOutHandler={this.sliderMouseOutHandler}
                    nowChangeHandler={this.moveLocalIndexTo}
                  />
                )
              }
            </div>
          ) : null
        }
      </div>
    );
  }
}
