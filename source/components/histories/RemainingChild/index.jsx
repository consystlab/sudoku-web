import React from 'react';
import PropTypes from 'prop-types';
import Immutable from 'immutable';

import styles from './style.scss';

function RemainingChild(props) {
  const childHistoryMouseOverHandler = () => {
    props.setVariablesHovered(Immutable.Map({ [props.variable.id]: true }));
  };

  const childHistoryMouseOutHandler = () => {
    props.setVariablesHovered(Immutable.Map());
  };

  const [col, row] = props.variable.id.split('_').map(v => parseInt(v, 10));
  const pos = String.fromCharCode(65 + col) + (row + 1);
  const getValues = Array.from(props.domain).sort().join(', ');

  return (
    <div className={styles['history-child']}
      onMouseOver={childHistoryMouseOverHandler}
      onMouseOut={childHistoryMouseOutHandler}
    >
      {pos}<img src="img/gets.svg" height="8px" />{getValues}
    </div>
  );
}

RemainingChild.propTypes = {
  variable: PropTypes.object,
  domain: PropTypes.object,

  setVariablesHovered: PropTypes.func.isRequired,
};

export default RemainingChild;
