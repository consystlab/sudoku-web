import React from 'react';
import PropTypes from 'prop-types';
import Immutable from 'immutable';
import cn from 'classnames';

import styles from './style.scss';

function SingletonAssignHistory(props) {
  const historyClickHandler = () => {
    props.historySeek(props.index);
  };

  const historyMouseOverHandler = () => {
    props.setVariablesHovered(props.singletonAssignHistoryModel.variablesAffected.reduce((acc, val) =>
      acc.set(val.id, true)
    , Immutable.Map()));
  };

  const historyMouseOutHandler = () => {
    props.setVariablesHovered(Immutable.Map());
  };

  const assigned = Object.keys(props.singletonAssignHistoryModel.assignments).length;
  const seekBefore = props.currentIndex < props.index;
  return (
    <div className={styles['history-container']}>
      <div className={cn(styles['history'], { [styles['not-in-use']]: seekBefore })}
        onMouseOver={historyMouseOverHandler}
        onMouseOut={historyMouseOutHandler}
        onClick={historyClickHandler}
      >
        {assigned} {assigned === 1 ? ' singleton' : ' singletons'} assigned
      </div>
    </div>
  );
}

SingletonAssignHistory.propTypes = {
  currentIndex: PropTypes.number.isRequired,
  singletonAssignHistoryModel: PropTypes.object,
  index: PropTypes.number.isRequired,

  historySeek: PropTypes.func.isRequired,
  setVariablesHovered: PropTypes.func.isRequired,
};

export default SingletonAssignHistory;
