import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import numberToWords from 'number-to-words';
import capitalize from 'capitalize';
import { Glyphicon } from 'react-bootstrap';

import BoardContainer from 'containers/BoardContainer';
import SimpleCell from 'components/SimpleCell';

import cn from 'classnames';
import styles from './style.scss';

export default class SolutionHistory extends PureComponent {
  static propTypes = {
    index: PropTypes.number.isRequired,
    currentIndex: PropTypes.number.isRequired,
    solutionsHistoryModel: PropTypes.object.isRequired,
    solutionsDiff: PropTypes.string.isRequired,

    historySeek: PropTypes.func.isRequired,
    childrenExpanderClickHandler: PropTypes.func.isRequired,
  };

  historyClickHandler = () => {
    this.props.historySeek(this.props.index);
  }

  childrenExpanderClickHandler = (e) => {
    e.stopPropagation();
    this.props.childrenExpanderClickHandler(this.props.index);
  }

  render() {
    const solutions = this.props.solutionsHistoryModel.solutions.map((solution, resultNum) =>
      solution.get('assignments').mapEntries((entry) => {
        let differingCells = false;
        switch (this.props.solutionsDiff) {
        case '2':
          if (resultNum > 0) {
            differingCells = this.props.solutionsHistoryModel.differingPairwiseCells[resultNum - 1][entry[0]];
          }
          break;

        case 'ALL':
          differingCells = this.props.solutionsHistoryModel.differingAllCells[entry[0]];
          break;

        default:
        }
        return [entry[0], (
          <SimpleCell key={`result-${resultNum}-${entry[0]}`}
            id={`result-${resultNum}_${entry[0]}`}
            assignment={entry[1][1]}
            className={cn({
              [styles['differing-cell']]: differingCells,
              [styles['original-assignment']]: this.props.solutionsHistoryModel.originalAssignments[entry[0]],
            })}
          />
        )];
      })
    );

    const glyphSign = this.props.solutionsHistoryModel.childrenExpanded ? 'minus-sign' : 'plus-sign';
    const seekBefore = this.props.currentIndex < this.props.index;

    const numSolutions = this.props.solutionsHistoryModel.solutions.length;
    let solutionText = '';

    if (numSolutions === 1) {
      solutionText = 'One solution found';
    } else if (numSolutions > this.props.solutionsHistoryModel.maxSolutions) {
      solutionText = `More than ${this.props.solutionsHistoryModel.maxSolutions} solutions found`;
    } else if (numSolutions > 0) {
      const solutionWords = numSolutions <= 10 ? capitalize.words(numberToWords.toWords(numSolutions)) : numSolutions;
      solutionText = `${solutionWords} solutions found`;
    } else {
      solutionText = 'No solutions found';
    }

    return (
      <div className={styles['history-container']}>
        <div className={cn(styles['history'], { [styles['not-in-use']]: seekBefore })}
          onClick={this.historyClickHandler}
        >
          <div className={styles['history-title']}>
            {solutionText}
          </div>
          {
            this.props.solutionsHistoryModel.solutions.length !== 0 ? (
              <Glyphicon glyph={glyphSign}
                className={styles['expansion-glyph']}
                value="false"
                onClick={this.childrenExpanderClickHandler}
              />
            ) : null
          }
        </div>
        {
          this.props.solutionsHistoryModel.solutions.length > 0 ? (
            <div className={cn(
                styles['history-children'],
                { [styles.hide]: !this.props.solutionsHistoryModel.childrenExpanded }
              )}
              onClick={this.historyClickHandler}
            >
              <div className={styles['solution-list']}>
                {
                  solutions.map((result, i) => (
                    <div
                      key={`solution-${this.props.index}-${i}`}
                      className={styles['solution']}
                    >
                      <div className={'title'}>
                        Solution {i + 1}
                      </div>
                      <BoardContainer
                        cells={result}
                        size="small"
                      />
                    </div>
                  ))
                }
              </div>
            </div>
          ) : null
        }
      </div>
    );
  }
}
