import React from 'react';
import PropTypes from 'prop-types';
import Immutable from 'immutable';

import Button from 'components/Button';

import cn from 'classnames';
import styles from './style.scss';

function LoadHistory(props) {
  const historyClickHandler = () => {
    props.historySeek(props.index);
  };

  const historyMouseOverHandler = () => {
    props.setVariablesHovered(Object.keys(props.loadHistoryModel.assignments).reduce((acc, key) =>
      acc.set(key, true), Immutable.Map()));
  };

  const historyMouseOutHandler = () => {
    props.setVariablesHovered(Immutable.Map());
  };

  const onConvertClick = () => {
    props.convertLoadHistory();
  };

  const seekBefore = props.currentIndex < props.index;
  return (
    <div className={styles['history-container']}>
      <div className={cn(styles['history'], { [styles['not-in-use']]: seekBefore })}
        onMouseOver={historyMouseOverHandler}
        onMouseOut={historyMouseOutHandler}
        onClick={historyClickHandler}
      >
        <div className={styles['history-line']}>
          <div className={styles['puzzle-name']}>
            {props.loadHistoryModel.puzzleName}
          </div>
        </div>
        {
          props.loadHistoryModel.consistency ? (
            <div className={styles['sub-line']}>
              <div>
              Solved by: {props.loadHistoryModel.consistency}
              </div>
              <Button
                className={styles['convert-button']}
                clickHandler={onConvertClick}
              >
                Clues as assignments
              </Button>
            </div>
          ) : null
        }
      </div>
    </div>
  );
}

LoadHistory.propTypes = {
  index: PropTypes.number.isRequired,
  currentIndex: PropTypes.number.isRequired,
  loadHistoryModel: PropTypes.object.isRequired,

  historySeek: PropTypes.func.isRequired,
  setVariablesHovered: PropTypes.func.isRequired,
  convertLoadHistory: PropTypes.func.isRequired,
};

export default LoadHistory;
