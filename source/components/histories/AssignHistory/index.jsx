import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import Immutable from 'immutable';
import { Propagations } from 'consyst';

import Slider from 'components/Slider';
import { Glyphicon } from 'react-bootstrap';

import propagationText from 'util/propagationText';

import cn from 'classnames';
import styles from './style.scss';

export default class AssignHistory extends PureComponent {
  static propTypes = {
    index: PropTypes.number.isRequired,
    currentIndex: PropTypes.number.isRequired,
    currentChildIndex: PropTypes.number,
    assignHistoryModel: PropTypes.object.isRequired,

    childrenExpanderClickHandler: PropTypes.func.isRequired,
    setVariablesHovered: PropTypes.func.isRequired,
    historySeek: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);

    this.totalRemoved = props.assignHistoryModel.removeActions.reduce((aRemoved, action) =>
    aRemoved + action.eliminated.reduceEntries((eRemoved, eliminated) =>
      eRemoved + eliminated.removed.size, 0)
    , 0);
  }

  componentWillReceiveProps(newProps) {
    this.totalRemoved = newProps.assignHistoryModel.removeActions.reduce((aRemoved, action) =>
      aRemoved + action.eliminated.reduceEntries((eRemoved, eliminated) =>
        eRemoved + eliminated.removed.size, 0)
      , 0);
  }

  setHoveredVariables = (childIndex) => {
    if (childIndex < this.props.assignHistoryModel.actions.length) {
      const eliminated = this.props.assignHistoryModel.actions[childIndex].eliminated;
      if (eliminated) {
        const constraintVariables = eliminated.getConstraintVariables();
        let hoveredVariables;
        if (constraintVariables) {
          hoveredVariables = Array.from(constraintVariables).reduce(
            (acc, variable) =>
              acc.set(variable.id, true)
            , Immutable.Map()
          );
        } else {
          hoveredVariables = eliminated.reduceEntries(
            (acc, { variable }) =>
              acc.set(variable.id, true)
            , Immutable.Map()
          );
        }
        this.props.setVariablesHovered(hoveredVariables);
      }
    } else {
      this.historyMouseOverHandler();
    }
  }

  historyClickHandler = () => {
    this.props.historySeek(this.props.index);
  }

  historyMouseOverHandler = () => {
    this.props.setVariablesHovered(this.props.assignHistoryModel.variablesAffected.reduce((acc, val) =>
      acc.set(val.id, true)
    , Immutable.Map()));
  }

  historyMouseOutHandler = () => {
    this.props.setVariablesHovered(Immutable.Map());
  }

  childrenExpanderClickHandler = () => {
    this.props.childrenExpanderClickHandler(this.props.index);
  }

  sliderMouseOverHandler = () => {
    this.setHoveredVariables(this.props.currentChildIndex);
  }

  sliderMouseOutHandler = () => {
    this.props.setVariablesHovered(Immutable.Map());
  }

  moveLocalIndexTo = (newIndex) => {
    this.props.historySeek(this.props.index, newIndex);
    this.setHoveredVariables(newIndex);
  }

  render() {
    const seekBefore = this.props.currentIndex < this.props.index;
    const methodString = propagationText(
      this.props.assignHistoryModel.method,
      this.props.assignHistoryModel.rowColOrGroup
    );
    // const variable = this.props.assignHistoryModel.assignAction.variable;
    // const pos = String.fromCharCode(65 + parseInt(variable.id.split('_')[0], 10))
    //   + variable.id.split('_')[1];

    let progressIndex;
    if (this.props.currentIndex < this.props.index) {
      progressIndex = 0;
    } else if (this.props.currentIndex > this.props.index) {
      progressIndex = this.props.assignHistoryModel.actions.length;
    } else {
      progressIndex = this.props.currentChildIndex;
    }

    const glyphSign = this.props.assignHistoryModel.childrenExpanded ? 'minus-sign' : 'plus-sign';
    return (
      <div className={styles['history-container']}>
        <div className={cn(styles['history'], { [styles['not-in-use']]: seekBefore })}
          onMouseOver={this.historyMouseOverHandler}
          onMouseOut={this.historyMouseOutHandler}
          onClick={this.historyClickHandler}
        >
          <div className={cn(styles['history-title'])}>
            {/* {pos}<img className={styles['gets']} src="img/gets.svg" height="8px" />
            {this.props.assignHistoryModel.assignAction.assigned} */}
            User assigned variables
            {
              (
                this.props.assignHistoryModel.method !== Propagations.None
                && this.props.assignHistoryModel.method !== Propagations.BC
              ) ? (
                `: ${methodString} removed ${this.totalRemoved} values`
              ) : ''
            }
          </div>
          {
            this.totalRemoved !== 0 ? (
              <Glyphicon glyph={glyphSign}
                className={styles['expansion-glyph']}
                value="false"
                onClick={this.childrenExpanderClickHandler}
              />
            ) : null
          }
        </div>
        {
          this.totalRemoved !== 0 ? (
            <div className={cn(
                styles['history-children'],
                { [styles.hide]: !this.props.assignHistoryModel.childrenExpanded }
              )}
              onClick={this.historyClickHandler}
              onMouseOver={this.sliderMouseOverHandler}
              onMouseOut={this.sliderMouseOutHandler}
            >
              <Slider
                min={0}
                max={this.props.assignHistoryModel.actions.length}
                now={progressIndex}
                active={this.props.index === this.props.currentIndex}

                mouseOverHandler={this.sliderMouseOverHandler}
                mouseOutHandler={this.sliderMouseOutHandler}
                nowChangeHandler={this.moveLocalIndexTo}
              />
            </div>
          ) : null
        }
      </div>
    );
  }
}
