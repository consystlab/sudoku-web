import React from 'react';
import PropTypes from 'prop-types';

import cn from 'classnames';
import styles from './style.scss';

function MethodHistory(props) {
  const historyClickHandler = () => {
    props.historyClickHandler(props.index);
  };

  const seekBefore = props.currentIndex < props.index;
  return (
    <div className={styles['history-holder']}>
      <div className={cn(styles.history, { [styles['not-in-use']]: seekBefore })}
        onClick={historyClickHandler}
      >
        Setting on {props.methodHistoryModel.setAction.getMethodString()}
      </div>
    </div>
  );
}

MethodHistory.propTypes = {
  index: PropTypes.number.isRequired,
  currentIndex: PropTypes.number.isRequired,
  methodHistoryModel: PropTypes.object.isRequired,

  historyClickHandler: PropTypes.func.isRequired,
};

export default MethodHistory;
