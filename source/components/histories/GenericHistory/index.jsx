import React from 'react';
import PropTypes from 'prop-types';

import cn from 'classnames';
import styles from './style.scss';

function GenericHistory(props) {
  const historyClickHandler = () => {
    props.historyClickHandler(props.index);
  };

  const seekBefore = props.currentIndex < props.index;
  return (
    <div className={styles['history-holder']}>
      <div className={cn(styles.history, { [styles['not-in-use']]: seekBefore })}
        onClick={historyClickHandler}
      >
        {props.genericHistoryModel.title}
      </div>
    </div>
  );
}

GenericHistory.propTypes = {
  index: PropTypes.number.isRequired,
  currentIndex: PropTypes.number.isRequired,
  genericHistoryModel: PropTypes.object.isRequired,
  historyClickHandler: PropTypes.func.isRequired,
};

export default GenericHistory;
