import React, { Component } from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';

import cn from 'classnames';
import styles from './style.scss';

export default class RemoveDialog extends Component {
  static propTypes = {
    variable: PropTypes.object,
    domain: PropTypes.array,
    cellRect: PropTypes.object,
    visible: PropTypes.bool,

    removeValue: PropTypes.func.isRequired,
    hideRemoveDialog: PropTypes.func.isRequired,
  }

  componentWillReceiveProps = (newProps) => {
    if (!this.props.visible && newProps.visible) {
      window.addEventListener('click', this.hideRemoveDialog);
    }
  }

  hideRemoveDialog = (e) => {
    if (!$(e.target).parents('#remove-value-dialog').length) {
      window.removeEventListener('click', this.hideRemoveDialog);
      this.props.hideRemoveDialog();
    }
  }

  removeValueClickHandler = (e) => {
    const value = parseInt($(e.target).attr('value'), 10);
    this.props.removeValue(value, this.props.variable);
  }

  render() {
    if (this.props.variable && this.props.visible) {
      return (
        <div id="remove-value-dialog"
          className={cn(styles['remove-value-dialog'], { [styles.hide]: !this.props.visible })}
          style={{
            left: this.props.cellRect.left + 3 / 4 * this.props.cellRect.width,
            top: this.props.cellRect.top + 3 / 4 * this.props.cellRect.height,
          }}
        >
          <div className={cn(styles['cell-group'])} id="group_dialog">
            {_.range(0, 9).map((i) => (
              <div className={styles['cell-holder']}
                id={`cell_${i % 3}${(i - i % 3) / 3}`}
                key={`Remove cell ${i}`}
              >
                <button className={cn(styles['cell'], { [styles.removed]: this.props.domain.indexOf(i + 1) === -1 })}
                  value={`${i + 1}`}
                  onClick={this.removeValueClickHandler}
                >
                  {i + 1}
                </button>
              </div>
              )
            )}
          </div>
        </div>
      );
    }
    return <div />;
  }
}
