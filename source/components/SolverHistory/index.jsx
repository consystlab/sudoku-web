import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';

import UndoRedoButtonsContainer from 'containers/UndoRedoButtonsContainer';

import GenericHistory from 'components/histories/GenericHistory';
import MethodHistory from 'components/histories/MethodHistory';
import RemoveHistory from 'components/histories/RemoveHistory';
import AssignHistory from 'components/histories/AssignHistory';
import LoadHistory from 'components/histories/LoadHistory';
import SingletonAssignHistory from 'components/histories/SingletonAssignHistory';
import SolutionsHistory from 'components/histories/SolutionsHistory';

import cn from 'classnames';
import styles from './style.scss';

export default class SolverHistory extends PureComponent {
  static propTypes = {
    className: PropTypes.string,
    histories: ImmutablePropTypes.list.isRequired,
    currentIndex: PropTypes.number.isRequired,
    currentChildIndex: PropTypes.number.isRequired,
    hoveredIndex: PropTypes.number,
    solutionsDiff: PropTypes.string,

    historySeek: PropTypes.func.isRequired,
    historyMouseOverHandler: PropTypes.func.isRequired,
    historyMouseOutHandler: PropTypes.func.isRequired,
    childrenExpanderClickHandler: PropTypes.func.isRequired,
    setVariablesHovered: PropTypes.func.isRequired,
    convertLoadHistory: PropTypes.func.isRequired,
  };

  componentDidUpdate = (prevProps) => {
    if (this.props.histories.size > prevProps.histories.size) {
      this.historyHolder.scrollTop = this.historyHolder.scrollHeight;
    }
  }

  render() {
    return (
      <div className={cn(this.props.className, ['solver-history'])}>
        <div className={styles['title-bar']}>
          <div className={styles['title']}>History</div>
          <UndoRedoButtonsContainer />
        </div>
        <div className={styles['history-panel-content']} ref={(el) => { this.historyHolder = el; }}>
          <div className={styles['history-holder']}>
            {
              this.props.histories.map((historyRecord, i) => {
                switch (historyRecord.name) {
                case 'MethodHistoryRecord':
                  return (
                    <MethodHistory
                      index={i}
                      key={`history-${i}`}
                      currentIndex={this.props.currentIndex}
                      methodHistoryModel={historyRecord.toJS()}
                      historyClickHandler={this.props.historySeek}
                      historyMouseOverHandler={this.props.historyMouseOverHandler}
                      historyMouseOutHandler={this.props.historyMouseOutHandler}
                    />
                  );

                case 'RemoveHistoryRecord':
                  return (
                    <RemoveHistory key={`history-${i}`}
                      index={i}
                      currentIndex={this.props.currentIndex}
                      currentChildIndex={this.props.currentChildIndex}
                      hoveredIndex={this.props.hoveredIndex}
                      removeHistoryModel={historyRecord.toJS()}

                      historySeek={this.props.historySeek}
                      setVariablesHovered={this.props.setVariablesHovered}
                      childrenExpanderClickHandler={this.props.childrenExpanderClickHandler}
                    />
                  );

                case 'AssignHistoryRecord':
                  return (
                    <AssignHistory key={`history-${i}`}
                      index={i}
                      currentIndex={this.props.currentIndex}
                      currentChildIndex={this.props.currentChildIndex}
                      assignHistoryModel={historyRecord.toJS()}

                      historySeek={this.props.historySeek}
                      setVariablesHovered={this.props.setVariablesHovered}
                      childrenExpanderClickHandler={this.props.childrenExpanderClickHandler}
                    />
                  );

                case 'GenericHistoryRecord':
                  return (
                    <GenericHistory key={`history-${i}`}
                      index={i}
                      currentIndex={this.props.currentIndex}
                      genericHistoryModel={historyRecord.toJS()}
                      historyClickHandler={this.props.historySeek}
                      historyMouseOverHandler={this.props.historyMouseOverHandler}
                      historyMouseOutHandler={this.props.historyMouseOutHandler}
                    />
                  );

                case 'LoadHistoryRecord':
                  return (
                    <LoadHistory key={`history-${i}`}
                      index={i}
                      currentIndex={this.props.currentIndex}
                      loadHistoryModel={historyRecord.toJS()}

                      historySeek={this.props.historySeek}
                      setVariablesHovered={this.props.setVariablesHovered}
                      convertLoadHistory={() => { this.props.convertLoadHistory(i); }}
                    />
                  );

                case 'SingletonAssignHistoryRecord':
                  return (
                    <SingletonAssignHistory key={`history-${i}`}
                      index={i}
                      currentIndex={this.props.currentIndex}
                      singletonAssignHistoryModel={historyRecord.toJS()}

                      historySeek={this.props.historySeek}
                      setVariablesHovered={this.props.setVariablesHovered}
                      childrenExpanderClickHandler={this.props.childrenExpanderClickHandler}
                    />
                  );

                case 'SolutionsHistoryRecord':
                  return (
                    <SolutionsHistory key={`history-${i}`}
                      index={i}
                      currentIndex={this.props.currentIndex}
                      solutionsHistoryModel={historyRecord.toJS()}
                      solutionsDiff={this.props.solutionsDiff}

                      historySeek={this.props.historySeek}
                      setVariablesHovered={this.props.setVariablesHovered}
                      childrenExpanderClickHandler={this.props.childrenExpanderClickHandler}
                    />
                  );

                default:
                }
                return null;
              })
            }
          </div>
        </div>
      </div>
    );
  }
}
