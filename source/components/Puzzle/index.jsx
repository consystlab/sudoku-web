import React from 'react';
import PropTypes from 'prop-types';

import styles from './style.scss';

function Puzzle(props) {
  return (
    <div className={styles['puzzle']} onClick={props.clickHandler}>
      {
        props.nameVisible ? (
          <div className={styles['col-name']}>
            {props.puzzle.name}
          </div>
        ) : null
      }
      {
        props.consistencyVisible ? (
          <div className={styles['col-const']}>
            {props.puzzle.consistency}
          </div>
        ) : null
      }
      {
        props.cluesVisible ? (
          <div className={styles['col-clues']}>
            {props.puzzle.clues}
          </div>
        ) : null
      }
      {
        props.solutionsVisible ? (
          <div className={styles['col-solutions']}>
            {props.puzzle.solutions}
          </div>
        ) : null
      }
      {
        props.levelVisible ? (
          <div className={styles['col-level']}>
            {props.puzzle.difflvl}/{props.puzzle.maxdifflvl}
          </div>
        ) : null
      }
      {
        props.sourceVisible ? (
          <div className={styles['col-source']}>
            {props.puzzle.source}
          </div>
        ) : null
      }
    </div>
  );
}

Puzzle.propTypes = {
  puzzle: PropTypes.object.isRequired,
  nameVisible: PropTypes.bool.isRequired,
  consistencyVisible: PropTypes.bool.isRequired,
  cluesVisible: PropTypes.bool.isRequired,
  solutionsVisible: PropTypes.bool.isRequired,
  levelVisible: PropTypes.bool.isRequired,
  sourceVisible: PropTypes.bool.isRequired,

  clickHandler: PropTypes.func.isRequired,
};

export default Puzzle;
