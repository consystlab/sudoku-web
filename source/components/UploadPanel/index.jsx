import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import {
  ImportZone,
  ResultCanvas,
} from 'sudoku-ocr';

import styles from './style.scss';

export default class UploadPanel extends PureComponent {
  static propTypes = {
    /* eslint-disable */
    canvasData: PropTypes.object,
    /* eslint-enable */

    onImageProcessed: PropTypes.func.isRequired,
  };

  render() {
    return (
      <div className={styles['upload-panel']}>
        <div className={styles['upload-card']}>
          <ImportZone
            className={styles['file-dropzone']}
            onImageProcessed={this.props.onImageProcessed}
          >
            <div className={styles['drop-inner']}>
              Drop a file or click to upload
            </div>
          </ImportZone>
          <div className={styles['result-canvas-container']}>
            <ResultCanvas
              className={styles['result-canvas']}
              canvasData={this.props.canvasData}
            />
          </div>
        </div>
      </div>
    );
  }
}
