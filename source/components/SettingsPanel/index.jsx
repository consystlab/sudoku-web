import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';

import cn from 'classnames';
import styles from './style.scss';

export default class SettingsPanel extends PureComponent {
  static propTypes = {
    showExplanations: PropTypes.bool,
    maxSaveResultsText: PropTypes.string,
    maxSaveResultsError: PropTypes.bool,
    maxSearchResultsText: PropTypes.string,
    maxSearchResultsError: PropTypes.bool,
    solutionsDiff: PropTypes.string,

    setMaxSaveResults: PropTypes.func.isRequired,
    setMaxSearchResults: PropTypes.func.isRequired,
    toggleShowExplanations: PropTypes.func.isRequired,
    setSolutionsDiff: PropTypes.func.isRequired,
  };

  onMaxSaveResultsChange = (e) => {
    this.props.setMaxSaveResults(e.target.value);
  }

  onMaxSearchResultsChange = (e) => {
    this.props.setMaxSearchResults(e.target.value);
  }

  onDiffSolutionChange = (e) => {
    this.props.setSolutionsDiff(e.target.value);
  }

  render() {
    return (
      <div className={styles['settings-panel']}>
        <div className={styles['settings-card']}>
          <div className={styles['title']}>
            Settings
          </div>
          <div className={styles['settings-list']}>
            <div className={styles['setting']}>
              <input type="checkbox"
                className={styles['input']}
                onChange={this.props.toggleShowExplanations}
                checked={!this.props.showExplanations}
              />
              Hide tooltip explanations
            </div>
            <div className={styles['setting']}>
              Stop search after
              <input type="text"
                className={cn(styles['input'], { [styles['error']]: this.props.maxSearchResultsError })}
                onChange={this.onMaxSearchResultsChange}
                value={this.props.maxSearchResultsText}
              />
              solutions
            </div>
            <div className={styles['setting']}>
              Store puzzles with a maximum of
              <input type="text"
                className={cn(styles['input'], { [styles['error']]: this.props.maxSaveResultsError })}
                onChange={this.onMaxSaveResultsChange}
                value={this.props.maxSaveResultsText}
              />
              solutions
            </div>
            <div className={styles['setting']}>
              Solution cell diff style:
              <select
                onChange={this.onDiffSolutionChange}
                value={this.props.solutionsDiff}
              >
                <option value="2">2</option>
                <option value="ALL">ALL</option>
              </select>
            </div>
          </div>
        </div>
        <div className={styles['acknowledgements-card']}>
          <div className={styles['title']}>
            Acknowledgements
          </div>
          <div className={styles['acknowledgements']}>
            Version 5.0 is a major redesign of a Java Applet first deployed in 2007.<br />
            Students with major contributions: Mitchell DeHaven, Jason Gaare, Ian Howell,
            Christoper Reeson, Robert Woodward.<br />
            BiSAC/BiSGAC suggested by Christian Bessiere.<br />
            Faculty advisor: Berthe Y. Choueiry<br />
            Constraint Systems Laboratory © 2018<br />
            Supported by multiple NSF REU supplements and UNL UCARE grants.<br />
          </div>
        </div>
      </div>
    );
  }
}
