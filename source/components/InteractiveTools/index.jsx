import React from 'react';
import PropTypes from 'prop-types';
import { Propagations } from 'consyst';

import Radio from 'components/Radio';

import cn from 'classnames';
import styles from './style.scss';

function InteractiveTools(props) {
  const solvingMethodClickHandler = (index) => {
    props.performMethodSet(index);
  };

  return (
    <div className={cn(props.className, styles['interactive-tools'])}>
      <div className={styles['title']}>
        Filter
      </div>
      <div className={styles['vertical-button-group']}>
        <Radio className={cn(styles['propagation-radio'], styles['hover-button'])}
          value={Propagations.None}
          checked={props.solvingMethodSelected === Propagations.None}
          changeHandler={solvingMethodClickHandler}
        >
          None
          {
            props.showExplanations ? (
              <div className={cn(styles['hover-info'], styles['bottom'])}>
                No interactive solving method will be used.
              </div>
            ) : null
          }
        </Radio>
        <Radio className={styles['hover-button']}
          value={Propagations.BC}
          checked={props.solvingMethodSelected === Propagations.BC}
          changeHandler={solvingMethodClickHandler}
        >
          Back checking
          {
            props.showExplanations ? (
              <div className={cn(styles['propagation-radio'], styles['hover-info'], styles['bottom'])}>
                Backchecking: All current decisions (i.e., assignments) are consistent. A new assignment is
                checked against past ones. If it is inconsistent, the broken constraint is highlighted in red.
              </div>
            ) : null
          }
        </Radio>
        <div className={styles['binary-card']}>
          <Radio className={cn(styles['propagation-radio'], styles['hover-button'])}
            value={Propagations.BFC}
            checked={props.solvingMethodSelected === Propagations.BFC}
            changeHandler={solvingMethodClickHandler}
          >
            FC Binary
            {
              props.showExplanations ? (
                <div className={cn(styles['hover-info'], styles['bottom'])}>
                  Forward Checking: After each decision (i.e., assignment), the assigned value is removed from
                  the cells in the same row, column, and block. FC is a weak form of look-ahead.
                </div>
              ) : null
            }
          </Radio>
          <Radio className={cn(styles['propagation-radio'], styles['hover-button'])}
            value={Propagations.BRFL}
            checked={props.solvingMethodSelected === Propagations.BRFL}
            changeHandler={solvingMethodClickHandler}
          >
            RFL Binary
            {
              props.showExplanations ? (
                <div className={cn(styles['hover-info'], styles['bottom'])}>
                  Real Full Lookahead: After every decision (i.e., assignment), AC is enforced. At
                  the end of the process, every combination of two cells have consistent combinations of values.
                  RFL is sometimes called MAC, for Maintaining Arc Consistency.
                </div>
              ) : null
            }
          </Radio>
        </div>
        <div className={styles['non-binary-card']}>
          <Radio className={cn(styles['propagation-radio'], styles['hover-button'])}
            value={Propagations.NBFC}
            checked={props.solvingMethodSelected === Propagations.NBFC}
            changeHandler={solvingMethodClickHandler}
          >
            FC Non-Binary
            {
              props.showExplanations ? (
                <div className={cn(styles['hover-info'], styles['bottom'])}>
                  The GAC version of Forward Checking.
                </div>
              ) : null
            }
          </Radio>
          <Radio className={cn(styles['propagation-radio'], styles['hover-button'])}
            value={Propagations.NBRFL}
            checked={props.solvingMethodSelected === Propagations.NBRFL}
            changeHandler={solvingMethodClickHandler}
          >
            RFL Non-Binary
            {
              props.showExplanations ? (
                <div className={cn(styles['hover-info'], styles['bottom'])}>
                  Essentially running gac after every assignment.
                </div>
              ) : null
            }
          </Radio>
        </div>
      </div>
    </div>
  );
}

InteractiveTools.propTypes = {
  className: PropTypes.string,
  showExplanations: PropTypes.bool,
  solvingMethodSelected: PropTypes.number,

  performMethodSet: PropTypes.func.isRequired,
};

export default InteractiveTools;
