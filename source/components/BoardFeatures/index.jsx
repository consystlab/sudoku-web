import React from 'react';
import PropTypes from 'prop-types';

import Button from 'components/Button';

import styles from './style.scss';

function BoardFeatures(props) {
  return (
    <div className={styles['board-features']}>
      <div className={styles['title']}>
        Board
      </div>
      <div className={styles['section-holder']}>
        <div className={styles['section']}>
          <div className={styles['section-content']}>
            <div className={styles['show-domains']}>
              <input type="checkbox"
                className={styles['feature-checkbox']}
                onChange={props.toggleMinisVisible}
                checked={props.minisVisible}
              />
              Domains
            </div>
            <Button
              className={styles['feature-button']}
              clickHandler={props.resetGrid}
            >
              Reset Grid
            </Button>
          </div>
        </div>
        <div className={styles['section']}>
          <div className={styles['title']}>Assign Singletons</div>
          <div className={styles['section-content']}>
            <div className={styles['singleton-buttons']}>
              <Button className={styles['singleton-button']}
                clickHandler={props.toggleAutoSingleton}
                selected={props.autoSingleton}
              >
                Auto
              </Button>
              <Button className={styles['singleton-button']}
                clickHandler={props.assignSingletons}
              >
                Now
              </Button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

BoardFeatures.propTypes = {
  minisVisible: PropTypes.bool,
  autoSingleton: PropTypes.bool,

  assignSingletons: PropTypes.func.isRequired,
  resetGrid: PropTypes.func.isRequired,
  toggleAutoSingleton: PropTypes.func.isRequired,
  toggleMinisVisible: PropTypes.func.isRequired,
};

export default BoardFeatures;
