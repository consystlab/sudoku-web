import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';

import Button from 'components/Button';

import cn from 'classnames';
import styles from './style.scss';

export default class SolverHistory extends PureComponent {
  static propTypes = {
    className: PropTypes.string,
    buttonClass: PropTypes.string,
    histories: ImmutablePropTypes.list.isRequired,
    currentIndex: PropTypes.number.isRequired,

    historySeek: PropTypes.func.isRequired,
  };

  undoClickHandler = () => {
    if (this.props.currentIndex > 0) {
      this.props.historySeek(
        this.props.currentIndex - 1,
        this.props.histories.get(this.props.currentIndex - 1)
      );
    }
  };

  redoClickHandler = () => {
    if (this.props.currentIndex + 1 < this.props.histories.size) {
      this.props.historySeek(
        this.props.currentIndex + 1,
        this.props.histories.get(this.props.currentIndex + 1)
      );
    }
  };

  render() {
    return (
      <div className={cn(this.props.className, styles['undo-redo-buttons'])}>
        <Button className={cn(styles['history-button'], this.props.buttonClass)}
          clickHandler={this.undoClickHandler}
        >
          Undo
        </Button>
        <Button className={cn(styles['history-button'], this.props.buttonClass)}
          clickHandler={this.redoClickHandler}
        >
          Redo
        </Button>
      </div>
    );
  }
}
