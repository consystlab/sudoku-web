import React, { PureComponent } from 'react';
import Immutable from 'immutable';
import PropTypes from 'prop-types';
import _ from 'lodash';

import BoardContainer from 'containers/BoardContainer';
import BoardFeaturesContainer from 'containers/BoardFeaturesContainer';
import CellContainer from 'containers/CellContainer';
import ControlPanelContainer from 'containers/ControlPanelContainer';
import RemoveDialogContainer from 'containers/RemoveDialogContainer';

import BinaryColumnIcon from 'components/icons/BinaryColumnIcon';
import NonbinaryColumnIcon from 'components/icons/NonbinaryColumnIcon';
import BinaryRowIcon from 'components/icons/BinaryRowIcon';
import NonbinaryRowIcon from 'components/icons/NonbinaryRowIcon';

import styles from './style.scss';

export default class App extends PureComponent {
  static propTypes = {
    arcRowClickHandler: PropTypes.func.isRequired,
    arcColClickHandler: PropTypes.func.isRequired,
    gacRowClickHandler: PropTypes.func.isRequired,
    gacColClickHandler: PropTypes.func.isRequired,

    rowMouseOverHandler: PropTypes.func.isRequired,
    rowMouseOutHandler: PropTypes.func.isRequired,
    colMouseOverHandler: PropTypes.func.isRequired,
    colMouseOutHandler: PropTypes.func.isRequired,

    startUpHandler: PropTypes.func.isRequired,
  };

  componentWillMount() {
    this.props.startUpHandler();
  }

  render() {
    const cells = Immutable.Map().withMutations(s => {
      _.range(0, 9).forEach(row =>
        _.range(0, 9).forEach(col => {
          s.set(`${row}_${col}`, (
            <CellContainer key={`cell-container-${row}_${col}`}
              row={row}
              col={col}
            />
          ));
        })
      );
    });

    return (
      <div className={styles['app-container']}>
        <div className={styles['app']}>
          <table>
            <tbody>
              <tr>
                <td />
                <td>
                  <div className={styles['col-names']} key="col-names">
                    {
                      _.range(0, 3).map((i) => (
                        <div className={styles['hgroup']} key={`name-col-${i}`}>
                          {
                            _.range(0, 3).map((j) => (
                              <div key={`name-col-${3 * i + j}`}
                                className={styles['col-name']}
                              >
                                {3 * i + j + 1}
                              </div>
                            ))
                          }
                        </div>
                      ))
                    }
                  </div>
                </td>
              </tr>
              <tr>
                <td>
                  <div className={styles['row-names']} key="row-names">
                    {
                      _.range(0, 3).map((i) => (
                        <div className={styles['vgroup']} key={`name-row-${i}`}>
                          {
                            _.range(0, 3).map((j) => (
                              <div key={`name-row-${3 * i + j}`}
                                className={styles['row-name']}
                              >
                                {String.fromCharCode('A'.charCodeAt(0) + 3 * i + j)}
                              </div>
                            ))
                          }
                        </div>
                      ))
                    }
                  </div>
                </td>
                <td>
                  <BoardContainer
                    cells={cells}
                  >
                    <RemoveDialogContainer />
                  </BoardContainer>
                </td>
                <td className={styles['row-consistencies']}>
                  <div className={styles['row-consistency']} key="arc-row">
                    {
                      _.range(0, 3).map((i) => (
                        <div className={styles['vgroup']} key={`arc-row-group-${i}`}>
                          {
                            _.range(0, 3).map((j) => (
                              <div key={`arc-row-${3 * i + j}`}
                                id={`arc-row-${3 * i + j}`}
                                className={styles['row-prop']}
                                onClick={() => { this.props.arcRowClickHandler(3 * i + j); }}
                                onMouseOver={() => { this.props.rowMouseOverHandler(3 * i + j); }}
                                onMouseOut={this.props.rowMouseOutHandler}
                              >
                                <BinaryRowIcon />
                              </div>
                            ))
                          }
                        </div>
                      ))
                    }
                  </div>
                  <div className={styles['row-consistency']} key="gac-row">
                    {
                      _.range(0, 3).map((i) => (
                        <div className={styles['vgroup']} key={`gac-row-group-${i}`}>
                          {
                            _.range(0, 3).map((j) => (
                              <div key={`gac-row-${3 * i + j}`}
                                id={`gac-row-${3 * i + j}`}
                                className={styles['row-prop']}
                                onClick={() => { this.props.gacRowClickHandler(3 * i + j); }}
                                onMouseOver={() => { this.props.rowMouseOverHandler(3 * i + j); }}
                                onMouseOut={this.props.rowMouseOutHandler}
                              >
                                <NonbinaryRowIcon />
                              </div>
                            ))
                          }
                        </div>
                      ))
                    }
                  </div>
                </td>
              </tr>
              <tr>
                <td />
                <td>
                  <div className={styles['col-consistency']} key="arc-col">
                    {
                      _.range(0, 3).map((i) => (
                        <div className={styles['hgroup']} key={`arc-col-group-${i}`}>
                          {
                            _.range(0, 3).map((j) => (
                              <div key={`arc-col-${3 * i + j}`}
                                id={`arc-col-${3 * i + j}`}
                                className={styles['col-prop']}
                                onClick={() => { this.props.arcColClickHandler(3 * i + j); }}
                                onMouseOver={() => { this.props.colMouseOverHandler(3 * i + j); }}
                                onMouseOut={this.props.colMouseOutHandler}
                              >
                                <BinaryColumnIcon />
                              </div>
                            ))
                          }
                        </div>
                      ))
                    }
                  </div>
                  <div className={styles['col-consistency']} key="gac-col">
                    {
                      _.range(0, 3).map((i) => (
                        <div className={styles['hgroup']} key={`gac-col-group-${i}`}>
                          {
                            _.range(0, 3).map((j) => (
                              <div key={`gac-col-${3 * i + j}`}
                                id={`gac-col-${3 * i + j}`}
                                className={styles['col-prop']}
                                onClick={() => { this.props.gacColClickHandler(3 * i + j); }}
                                onMouseOver={() => { this.props.colMouseOverHandler(3 * i + j); }}
                                onMouseOut={this.props.colMouseOutHandler}
                              >
                                <NonbinaryColumnIcon />
                              </div>
                            ))
                          }
                        </div>
                      ))
                    }
                  </div>
                </td>
              </tr>
              <tr>
                <td />
                <td>
                  <BoardFeaturesContainer />
                </td>
              </tr>
            </tbody>
          </table>
          <ControlPanelContainer />
        </div>
      </div>
    );
  }
}
