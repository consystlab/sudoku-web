import React from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import _ from 'lodash';

import cn from 'classnames';
import styles from './style.scss';

function Board(props) {
  if (props.cells.size !== props.width * props.height) return null;

  const xgroups = props.width / props.groupx;
  const ygroups = props.height / props.groupy;
  const cellGroups = _.range(0, xgroups * ygroups).map((i) => {
    const cells = _.range(0, props.groupx * props.groupy).map((j) => {
      const x = (i % xgroups) * props.groupx + j % props.groupx;
      const y = Math.floor(i / xgroups) * props.groupy + Math.floor(j / props.groupx);
      return props.cells.get(`${y}_${x}`);
    });
    return (
      <div key={`cell-group_${i}`}
        id={`cell-group_${i}`}
        className={cn(styles['cell-group'], styles[props.size])}
      >
        {cells}
      </div>
    );
  });
  return (
    <div className={cn(styles['board'], styles[props.size])}>
      {cellGroups}
      {props.children}
    </div>
  );
}

Board.propTypes = {
  width: PropTypes.number.isRequired,
  height: PropTypes.number.isRequired,
  groupx: PropTypes.number.isRequired,
  groupy: PropTypes.number.isRequired,

  cells: ImmutablePropTypes.map,
  size: PropTypes.oneOf(['small', 'regular']),
  children: PropTypes.node,
};

export default Board;
