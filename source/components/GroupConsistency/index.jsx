import React from 'react';
import PropTypes from 'prop-types';
import { Glyphicon } from 'react-bootstrap';
import _ from 'lodash';

import BinaryGroupIcon from 'components/icons/BinaryGroupIcon';
import NonbinaryGroupIcon from 'components/icons/NonbinaryGroupIcon';

import cn from 'classnames';
import styles from './style.scss';

function GroupConsistency(props) {
  const arcGroupClickHandler = props.arcGroupClickHandler;
  const gacGroupClickHandler = props.gacGroupClickHandler;
  const groupMouseOverHandler = props.groupMouseOverHandler;
  const groupMouseOutHandler = props.groupMouseOutHandler;

  return (
    <div className={cn(props.className, styles['group-consistency'])}>
      <div className={styles['title']}>
        Enforce
      </div>
      <div className={styles['inner-button-space']}>
        <div className={styles['vertical-button-group']}>
          <div className={styles['prop-qualifier']}>
            AC
            {
              props.showExplanations ? (
                <span className={styles['hover-button']}>
                  <Glyphicon glyph="info-sign" className={styles['consistency-info-icon']} />
                  <div className={styles['hover-info']}>
                    The buttons below and the ones surrounding the board locally enforce arc consistency on the
                    highlighted cells.
                  </div>
                </span>
              ) : null
            }
          </div>
          <div className={styles['cell-group']}>
            {
              _.range(0, 9).map((i) => (
                <div key={`arc-g-${i}`}
                  className={styles['cell']}
                  onClick={() => { arcGroupClickHandler(i); }}
                  onMouseOver={() => { groupMouseOverHandler(i); }}
                  onMouseOut={groupMouseOutHandler}
                >
                  <BinaryGroupIcon />
                </div>
              )
            )}
          </div>
        </div>
        <div className={styles['vertical-button-group']}>
          <div className={styles['prop-qualifier']}>
            GAC
            {
              props.showExplanations ? (
                <span className={styles['hover-button']}>
                  <Glyphicon glyph="info-sign" className={styles['consistency-info-icon']} />
                  <div className={styles['hover-info']}>
                    The buttons below and the ones surrounding the board locally enforce generalized arc consistency
                    on the highlighted cells.
                  </div>
                </span>
              ) : null
            }
          </div>
          <div className={styles['cell-group']}>
            {
              _.range(0, 9).map((i) => (
                <div key={`gac-g-${i}`}
                  className={styles['cell']}
                  onClick={() => { gacGroupClickHandler(i); }}
                  onMouseOver={() => { groupMouseOverHandler(i); }}
                  onMouseOut={groupMouseOutHandler}
                >
                  <NonbinaryGroupIcon />
                </div>
              )
            )}
          </div>
        </div>
      </div>
    </div>
  );
}

GroupConsistency.propTypes = {
  className: PropTypes.string,
  showExplanations: PropTypes.bool,

  arcGroupClickHandler: PropTypes.func.isRequired,
  gacGroupClickHandler: PropTypes.func.isRequired,
  groupMouseOverHandler: PropTypes.func.isRequired,
  groupMouseOutHandler: PropTypes.func.isRequired,
};

export default GroupConsistency;
