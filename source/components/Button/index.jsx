import React, { Component } from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import styles from './style.scss';

export default class Button extends Component {
  static propTypes = {
    children: PropTypes.node,
    className: PropTypes.string,
    value: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
    ]),
    selected: PropTypes.bool,

    clickHandler: PropTypes.func,
    mouseOverHandler: PropTypes.func,
    mouseOutHandler: PropTypes.func,
    reference: PropTypes.func,
  };

  refHandler = (el) => {
    if (this.props.reference) {
      this.props.reference(el);
    }
  }

  clickHandler = () => {
    if (this.props.clickHandler) {
      this.props.clickHandler(this.props.value);
    }
  };

  mouseOverHandler = () => {
    if (this.props.mouseOverHandler) {
      this.props.mouseOverHandler(this.props.value);
    }
  };

  mouseOutHandler = () => {
    if (this.props.mouseOutHandler) {
      this.props.mouseOutHandler(this.props.value);
    }
  };

  render() {
    const className = cn(styles['button'], { [styles.selected]: this.props.selected });

    return (
      <div
        ref={this.refHandler}
        className={cn(className, this.props.className)}
        onClick={this.clickHandler}
        onMouseOver={this.mouseOverHandler}
        onMouseOut={this.mouseOutHandler}
      >
        {this.props.children}
      </div>
    );
  }
}
