import React from 'react';
import PropTypes from 'prop-types';

import Solver from 'components/Solver';
import Tab from 'components/Tab';
import Content from 'components/Content';
import CogWheelIcon from 'components/icons/CogWheelIcon';

import PuzzleSelectContainer from 'containers/PuzzleSelectContainer';
import SubmitPuzzleContainer from 'containers/SubmitPuzzleContainer';
import UploadContainer from 'containers/UploadContainer';
import SettingsContainer from 'containers/SettingsContainer';

import ControlPanelTabsEnumeration from 'enumerations/ControlPanelTabs';

import styles from './style.scss';

function ControlPanel(props) {
  return (
    <div className={styles['control-panel-container']}>
      <div className={styles['control-panel']}>
        <div className={styles['control-panel-header']}>
          <Tab className={styles['top-button']}
            value={ControlPanelTabsEnumeration.Load}
            selected={props.selectedTab === ControlPanelTabsEnumeration.Load}
            clickHandler={props.tabButtonClickHandler}
          >
            LOAD
          </Tab>
          <Tab className={styles['top-button']}
            value={ControlPanelTabsEnumeration.Upload}
            selected={props.selectedTab === ControlPanelTabsEnumeration.Upload}
            clickHandler={props.tabButtonClickHandler}
          >
            UPLOAD
          </Tab>
          <Tab className={styles['top-button']}
            value={ControlPanelTabsEnumeration.Solve}
            selected={props.selectedTab === ControlPanelTabsEnumeration.Solve}
            clickHandler={props.tabButtonClickHandler}
          >
            SOLVE
          </Tab>
          <Tab className={styles['top-button']}
            value={ControlPanelTabsEnumeration.Submit}
            selected={props.selectedTab === ControlPanelTabsEnumeration.Submit}
            clickHandler={props.tabButtonClickHandler}
          >
            SUBMIT
          </Tab>
          <Tab className={styles['settings-button']}
            value={ControlPanelTabsEnumeration.Settings}
            selected={props.selectedTab === ControlPanelTabsEnumeration.Settings}
            clickHandler={props.tabButtonClickHandler}
          >
            <CogWheelIcon className={styles['cog-wheel']} />
          </Tab>
        </div>
        <div className={styles['control-panel-body']}>
          <Content selected={props.selectedTab === ControlPanelTabsEnumeration.Load}>
            <PuzzleSelectContainer />
          </Content>
          <Content selected={props.selectedTab === ControlPanelTabsEnumeration.Upload}>
            <UploadContainer />
          </Content>
          <Content selected={props.selectedTab === ControlPanelTabsEnumeration.Solve}>
            <Solver />
          </Content>
          <Content selected={props.selectedTab === ControlPanelTabsEnumeration.Submit}>
            <SubmitPuzzleContainer />
          </Content>
          <Content selected={props.selectedTab === ControlPanelTabsEnumeration.Settings}>
            <SettingsContainer />
          </Content>
        </div>
      </div>
    </div>
  );
}

ControlPanel.propTypes = {
  selectedTab: PropTypes.number.isRequired,

  tabButtonClickHandler: PropTypes.func.isRequired,
};

export default ControlPanel;
