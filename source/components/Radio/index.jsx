import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import styles from './style.scss';

function Radio(props) {
  const changeHandler = () => {
    if (props.changeHandler) {
      props.changeHandler(props.value);
    }
  };

  const mouseOverHandler = () => {
    if (props.mouseOverHandler) {
      props.mouseOverHandler(props.value);
    }
  };

  const mouseOutHandler = () => {
    if (props.mouseOutHandler) {
      props.mouseOutHandler(props.value);
    }
  };

  return (
    <div
      className={cn(styles['radio'], props.className)}
      onMouseOver={mouseOverHandler}
      onMouseOut={mouseOutHandler}
    >
      <input type="radio" checked={props.checked} onChange={changeHandler} />
      <span onClick={changeHandler} className={styles['radio-label']}>{props.children}</span>
    </div>
  );
}

Radio.propTypes = {
  children: PropTypes.node,
  checked: PropTypes.bool,
  className: PropTypes.string,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),

  changeHandler: PropTypes.func,
  mouseOverHandler: PropTypes.func,
  mouseOutHandler: PropTypes.func,
};

export default Radio;
