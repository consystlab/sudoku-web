import React from 'react';

const NonbinaryRowIcon = () => (
  <svg viewBox="0 0 8 24" width="8px" height="24px">
    <path d="M 0 0 L 8 24 z" />
    <path d="M 4 0 L 4 24 z" />
    <path d="M 8 0 L 0 24 z" />
    <path d="M 4 6 L 1 12 L 4 18 L 7 12 z" />
  </svg>
);

export default NonbinaryRowIcon;
