import React from 'react';

const BinaryRowIcon = () => (
  <svg viewBox="0 0 8 24" width="8px" height="24px" shapeRendering="crispEdges">
    <path d="M 4 0 L 4 24 z" />
    <rect x="1" y="9" width="6" height="6" />
  </svg>
);

export default BinaryRowIcon;
