import React from 'react';

const BinaryColumnIcon = () => (
  <svg viewBox="0 0 24 8" width="24px" height="8px" shapeRendering="crispEdges">
    <path d="M 0 4 L 24 4 z" />
    <rect x="9" y="1" width="6" height="6" />
  </svg>
);

export default BinaryColumnIcon;
