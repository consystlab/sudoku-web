import React from 'react';

const NonbinaryGroupIcon = () => (
  <svg viewBox="0 0 18 18" width="18px" height="18px" shapeRendering="geometricPrecision">
    <path d="M 0 9 L 18 9 z" />
    <path d="M 0 0 L 18 18 z" />
    <path d="M 9 0 L 9 18 z" />
    <path d="M 18 0 L 0 18 z" />
    <path d="M 4 9 L 9 4 L 14 9 L 9 14 z" />
  </svg>
);

export default NonbinaryGroupIcon;
