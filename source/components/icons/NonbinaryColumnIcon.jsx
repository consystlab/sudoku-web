import React from 'react';

const NonbinaryColumnIcon = () => (
  <svg viewBox="0 0 24 8" width="24px" height="8px" shapeRendering="geometricPrecision">
    <path d="M 0 0 L 24 8 z" />
    <path d="M 0 4 L 24 4 z" />
    <path d="M 0 8 L 24 0 z" />
    <path d="M 6 4 L 12 1 L 18 4 L 12 7 z" />
  </svg>
);

export default NonbinaryColumnIcon;
