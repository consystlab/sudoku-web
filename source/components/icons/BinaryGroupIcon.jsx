import React from 'react';

const BinaryGroupIcon = () => (
  <svg viewBox="0 0 18 18" width="18px" height="18px" shapeRendering="crispEdges">
    <path d="M 9 0 L 9 18 z" />
    <rect x="6" y="6" width="6" height="6" />
  </svg>
);

export default BinaryGroupIcon;
