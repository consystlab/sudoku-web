import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Propagations } from 'consyst';

import Button from 'components/Button';

import cn from 'classnames';
import styles from './style.scss';

export default class PropagationTools extends Component {
  static propTypes = {
    className: PropTypes.string,
    showExplanations: PropTypes.bool,
    positions: PropTypes.object,
    styles: PropTypes.object,

    performPropagation: PropTypes.func.isRequired,
    setPosition: PropTypes.func.isRequired,
    setStyle: PropTypes.func.isRequired,
  }

  setElement = (id, el) => {
    if (el) {
      this.props.setPosition(id, el.getBoundingClientRect());
      const computed = window.getComputedStyle(el);
      const style = {
        marginLeft: parseInt(computed['margin-left'], 10),
        marginRight: parseInt(computed['margin-right'], 10),
        marginBottom: parseInt(computed['margin-bottom'], 10),
        marginTop: parseInt(computed['margin-top'], 10),
      };
      this.props.setStyle(id, style);
    }
  }

  propagationClickHandler = (level) => {
    this.props.performPropagation(level);
  };

  /*
   * make edge makes an edge from the `from` location to the `to` location.
   * In addition, a from position and to postition may be specified in the
   * following box format:
   * 0 1 2
   * 3 4 5
   * 6 7 8
   */
  makeEdge = (baseString, fromString, toString, fromPos = 4, toPos = 4) => {
    const base = this.props.positions.get(baseString);
    const baseStyle = this.props.styles.get(baseString);
    const from = this.props.positions.get(fromString);
    const to = this.props.positions.get(toString);
    const fromX = from.x + from.width / 2 * (fromPos % 3) - base.x;
    const fromY = from.y + from.height / 2 * (fromPos - fromPos % 3) / 3 - base.y + baseStyle.marginTop;
    const toX = to.x + to.width / 2 * (toPos % 3) - base.x;
    const toY = to.y + to.height / 2 * (toPos - toPos % 3) / 3 - base.y + baseStyle.marginTop;
    return (<line
      key={`${fromString}->${toString}`}
      className={styles['hasse-line']}
      x1={fromX}
      y1={fromY}
      x2={toX}
      y2={toY}
    />);
  }

  render() {
    const hasseLines = [];
    if (this.props.positions.size) {
      const baseString = 'SSGAC';
      hasseLines.push(this.makeEdge(baseString, 'AC', 'GAC', 4, 7));
      hasseLines.push(this.makeEdge(baseString, 'AC', 'SAC', 4, 7));
      hasseLines.push(this.makeEdge(baseString, 'GAC', 'SGAC'));
      hasseLines.push(this.makeEdge(baseString, 'SAC', 'SGAC', 0, 8));
      hasseLines.push(this.makeEdge(baseString, 'SAC', 'POAC'));
      hasseLines.push(this.makeEdge(baseString, 'SGAC', 'POGAC'));
      hasseLines.push(this.makeEdge(baseString, 'POAC', 'POGAC', 0, 8));
      hasseLines.push(this.makeEdge(baseString, 'POAC', 'BiSAC'));
      hasseLines.push(this.makeEdge(baseString, 'POGAC', 'BiSGAC'));
      hasseLines.push(this.makeEdge(baseString, 'BiSAC', 'SSAC'));
      hasseLines.push(this.makeEdge(baseString, 'BiSAC', 'BiSGAC', 0, 8));
      hasseLines.push(this.makeEdge(baseString, 'BiSGAC', 'SSGAC', 1, 4));
      hasseLines.push(this.makeEdge(baseString, 'SSAC', 'SSGAC', 1, 4));
    }

    return (
      <div className={cn(this.props.className, styles['propagation-tools-container'])}>
        <div className={styles['propagation-tools']}>
          <div className={styles['title']}>
            Propagate
          </div>
          <div className={styles['vertical-button-group']}>
            <svg
              className={styles['hasse-diagram']}
            >
              {hasseLines}
            </svg>
            <Button className={cn(styles['content-button'], styles['hover-button'])}
              value={Propagations.SSGAC}
              reference={(el) => this.setElement('SSGAC', el)}
              clickHandler={this.propagationClickHandler}
            >
              SSGAC
              {
                this.props.showExplanations ? (
                  <div className={cn(styles['hover-info'], styles['mid'])}>
                    Double Singleton Generalized Arc Consistency (SSGAC): Assuming a value for a given cell, SGAC is
                    enforced on the problem. If SGAC detects inconsistency, the considered value is removed from the
                    cell. The mechanism is repeated for each value in each cell until a fixpoint. SSGAC is strictly
                    stronger than SGAC, although its true reasoning level is undetermined.
                  </div>
                ) : null
              }
            </Button>
            <div className={styles['horizontal-button-group']}>
              <Button className={cn(styles['content-button'], styles['hover-button'])}
                value={Propagations.BiSGAC}
                reference={(el) => this.setElement('BiSGAC', el)}
                clickHandler={this.propagationClickHandler}
              >
                BiSGAC
                {
                  this.props.showExplanations ? (
                    <div className={cn(styles['hover-info'], styles['mid'])}>
                      Double Singleton Generalized Arc Consistency (SSGAC): Assuming a value for a given cell, SGAC is
                      enforced on the problem. If SGAC detects inconsistency, the considered value is removed from the
                      cell. The mechanism is repeated for each value in each cell until a fixpoint. SSGAC is strictly
                      stronger than SGAC, although its true reasoning level is undetermined.
                    </div>
                  ) : null
                }
              </Button>
              <Button className={cn(styles['content-button'], styles['hover-button'])}
                value={Propagations.SSAC}
                reference={(el) => this.setElement('SSAC', el)}
                clickHandler={this.propagationClickHandler}
              >
                SSAC
                {
                  this.props.showExplanations ? (
                    <div className={cn(styles['hover-info'], styles['mid'])}>
                      Double Singleton Arc Consistency (SSAC): Assuming a value for a given cell, SAC is
                      enforced on the problem. If SAC detects inconsistency, the considered value is removed from the
                      cell. The mechanism is repeated for each value in each cell until a fixpoint. SSAC is strictly
                      stronger than SAC, although its true reasoning level is undetermined.
                    </div>
                  ) : null
                }
              </Button>
            </div>
            <div className={styles['horizontal-button-group']}>
              <Button className={cn(styles['content-button'], styles['hover-button'])}
                value={Propagations.POGAC}
                reference={(el) => this.setElement('POGAC', el)}
                clickHandler={this.propagationClickHandler}
              >
                POGAC
                {
                  this.props.showExplanations ? (
                    <div className={cn(styles['hover-info'], styles['mid'])}>
                      Double Singleton Generalized Arc Consistency (SSGAC): Assuming a value for a given cell, SGAC is
                      enforced on the problem. If SGAC detects inconsistency, the considered value is removed from the
                      cell. The mechanism is repeated for each value in each cell until a fixpoint. SSGAC is strictly
                      stronger than SGAC, although its true reasoning level is undetermined.
                    </div>
                  ) : null
                }
              </Button>
              <Button className={cn(styles['content-button'], styles['hover-button'])}
                value={Propagations.BiSAC}
                reference={(el) => this.setElement('BiSAC', el)}
                clickHandler={this.propagationClickHandler}
              >
                BiSAC
                {
                  this.props.showExplanations ? (
                    <div className={cn(styles['hover-info'], styles['mid'])}>
                      Bidirectional Singleton Generalized Arc Consistency (SSGAC):
                    </div>
                  ) : null
                }
              </Button>
            </div>
            <div className={styles['horizontal-button-group']}>
              <Button className={cn(styles['content-button'], styles['hover-button'])}
                value={Propagations.SGAC}
                reference={(el) => this.setElement('SGAC', el)}
                clickHandler={this.propagationClickHandler}
              >
                SGAC
                {
                  this.props.showExplanations ? (
                    <div className={cn(styles['hover-info'], styles['mid'])}>
                      Singleton Generalized Arc Consistency (SGAC): Assuming a value for a given cell, GAC is enforced
                      on the problem. If GAC detects inconsistency, the considered value is removed from the cell. The
                      mechanism is repeated for each value in each cell until a fixpoint. SGAC is strictly stronger
                      than GAC and SAC. It solves all known hardest Sudoku instances except three.
                    </div>
                  ) : null
                }
              </Button>
              <Button className={cn(styles['content-button'], styles['hover-button'])}
                value={Propagations.POAC}
                reference={(el) => this.setElement('POAC', el)}
                clickHandler={this.propagationClickHandler}
              >
                POAC
                {
                  this.props.showExplanations ? (
                    <div className={cn(styles['hover-info'], styles['mid'])}>
                      Bidirectional Singleton Generalized Arc Consistency (SSGAC):
                    </div>
                  ) : null
                }
              </Button>
            </div>
            <div className={styles['horizontal-button-group']}>
              <Button className={cn(styles['content-button'], styles['hover-button'])}
                value={Propagations.GAC}
                reference={(el) => this.setElement('GAC', el)}
                clickHandler={this.propagationClickHandler}
              >
                GAC
                {
                  this.props.showExplanations ? (
                    <div className={cn(styles['hover-info'], styles['mid'])}>
                      Generalized Arc Consistency (GAC): For the cells in a row, column, or block, values that
                      cannot participate in a consistent solution for the row, column, or block are removed. The
                      process is repeated until a fixpoint. GAC is strictly stronger than AC. However, SAC and GAC
                      are not comparable, that is, one may do more pruning than the other.
                    </div>
                  ) : null
                }
              </Button>
              <Button className={cn(styles['content-button'], styles['hover-button'])}
                value={Propagations.SAC}
                reference={(el) => this.setElement('SAC', el)}
                clickHandler={this.propagationClickHandler}
              >
                SAC
                {
                  this.props.showExplanations ? (
                    <div className={cn(styles['hover-info'], styles['mid'])}>
                      Singleton Arc Consistency (SAC): Assuming a value for a given cell, AC is enforced on the
                      problem. If AC detects inconsistency, the considered value is removed from the cell. The
                      mechanism is repeated for each value in each cell until a fixpoint. SAC is strictly stronger
                      than AC. However, SAC and GAC are not comparable, that is, one may do more pruning than the
                      other.
                    </div>
                  ) : null
                }
              </Button>
            </div>
            <Button className={cn(styles['content-button'], styles['hover-button'])}
              value={Propagations.AC}
              reference={(el) => this.setElement('AC', el)}
              clickHandler={this.propagationClickHandler}
            >
              AC
              {
                this.props.showExplanations ? (
                  <div className={cn(styles['hover-info'], styles['mid'])}>
                    {'Arc Consistency (AC): Given two \'adjacent\' cells, a value for one cell that is not'}
                    {'supported in the other cell is removed. Combinations of two cells are considered'}
                    {'repeatedly until a fixpoint.'}
                  </div>
                ) : null
              }
            </Button>
          </div>
        </div>
      </div>
    );
  }
}
