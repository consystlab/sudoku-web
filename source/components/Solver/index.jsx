import React, { PureComponent } from 'react';

import SolverHistoryContainer from 'containers/SolverHistoryContainer';
import GroupConsistencyContainer from 'containers/GroupConsistencyContainer';
import PropagationToolsContainer from 'containers/PropagationToolsContainer';
import InteractiveToolsContainer from 'containers/InteractiveToolsContainer';
import SearchToolsContainer from 'containers/SearchToolsContainer';

import styles from './style.scss';

export default class Solver extends PureComponent {
  render() {
    return (
      <div className={styles['solve-panel']}>
        <GroupConsistencyContainer
          className={styles['consistency-section']}
        />
        <PropagationToolsContainer
          className={styles['propagation-section']}
        />
        <InteractiveToolsContainer
          className={styles['interactive-section']}
        />
        <SearchToolsContainer
          className={styles['search-section']}
        />
        <SolverHistoryContainer
          className={styles['solver-history-section']}
        />
      </div>
    );
  }
}
