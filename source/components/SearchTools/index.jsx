import React from 'react';
import PropTypes from 'prop-types';

import Button from 'components/Button';

import cn from 'classnames';
import styles from './style.scss';

function SearchTools(props) {
  return (
    <div className={cn(styles['search-tools'], props.className)}>
      <div className={styles['title']}>
        Find
      </div>
      <div className={styles['vertical-button-row']}>
        <Button
          className={styles['find-solution-button']}
          clickHandler={props.findAllSolutions}
        >
          Solvable?
        </Button>
      </div>
    </div>
  );
}

SearchTools.propTypes = {
  className: PropTypes.string,

  findAllSolutions: PropTypes.func.isRequired,
};

export default SearchTools;
