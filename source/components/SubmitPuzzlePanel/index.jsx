import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';

import numberToWords from 'number-to-words';
import capitalize from 'capitalize';

import Button from 'components/Button';
import BoardContainer from 'containers/BoardContainer';
import SimpleCell from 'components/SimpleCell';
import SolutionsHistoryRecord from 'records/SolutionsHistoryRecord';
import UndoRedoButtonsContainer from 'containers/UndoRedoButtonsContainer';

import submitString from 'util/submitString';

import moment from 'moment';
import cn from 'classnames';
import styles from './style.scss';

export default class SubmitPuzzlePanel extends PureComponent {
  static propTypes = {
    puzzleName: PropTypes.string,
    sudokuType: PropTypes.string,
    source: PropTypes.string,
    minDiffLvl: PropTypes.string,
    minDiffLvlError: PropTypes.bool,
    diffLvl: PropTypes.string,
    diffLvlError: PropTypes.bool,
    maxDiffLvl: PropTypes.string,
    maxDiffLvlError: PropTypes.bool,
    origName: PropTypes.string,
    description: PropTypes.string,
    puzzleValues: PropTypes.string,
    puzzleValuesError: PropTypes.bool,
    numberOfClues: PropTypes.number,
    solutionsRecord: ImmutablePropTypes.recordOf(SolutionsHistoryRecord),
    maxSolutions: PropTypes.number,
    solutionsDiff: PropTypes.string,
    consistency: PropTypes.string,
    submitState: PropTypes.number,

    findMinimumConsistency: PropTypes.func.isRequired,
    findSolutions: PropTypes.func.isRequired,
    setPuzzleName: PropTypes.func.isRequired,
    setSudokuType: PropTypes.func.isRequired,
    setSource: PropTypes.func.isRequired,
    setMinDiffLvl: PropTypes.func.isRequired,
    setDiffLvl: PropTypes.func.isRequired,
    setMaxDiffLvl: PropTypes.func.isRequired,
    setOrigName: PropTypes.func.isRequired,
    setDescription: PropTypes.func.isRequired,
    setPuzzleValues: PropTypes.func.isRequired,
    submitPuzzle: PropTypes.func.isRequired,
    checkPuzzle: PropTypes.func.isRequired,
    clearAll: PropTypes.func.isRequired,
  };

  componentDidMount() {
    this.props.checkPuzzle();
    if (this.props.puzzleName === '') {
      this.props.setPuzzleName(moment().format('MM-DD-YYYY-HHmmss'));
    }
  }

  onSudokuTypeChange = (event) => {
    this.props.setSudokuType(event.target.value);
  }

  extractTargetValue = (func) => (event) => func(event.target.value);

  render() {
    const solutions = this.props.solutionsRecord.solutions.map((solution, resultNum) =>
      solution.get('assignments').mapEntries((entry) => {
        let differingCells = false;
        switch (this.props.solutionsDiff) {
        case '2':
          if (resultNum > 0) {
            differingCells = this.props.solutionsRecord.differingPairwiseCells.get(resultNum - 1).get(entry[0]);
          }
          break;

        case 'ALL':
          differingCells = this.props.solutionsRecord.differingAllCells.get(entry[0]);
          break;

        default:
        }
        return [entry[0], (
          <SimpleCell key={`result-${resultNum}-${entry[0]}`}
            id={`result-${resultNum}_${entry[0]}`}
            assignment={entry[1][1]}
            className={cn({
              [styles['differing-cell']]: differingCells,
              [styles['original-assignment']]: this.props.solutionsRecord.originalAssignments.get(entry[0]),
            })}
          />
        )];
      })
    );

    let solutionText = '';
    const numSolutions = this.props.solutionsRecord.solutions.length;

    if (numSolutions === 1) {
      solutionText = 'One solution found';
    } else if (numSolutions > this.props.maxSolutions) {
      solutionText = `More than ${this.props.maxSolutions} solutions found`;
    } else if (numSolutions > 0) {
      const solutionWords = numSolutions <= 10 ? capitalize.words(numberToWords.toWords(numSolutions)) : numSolutions;
      solutionText = `${solutionWords} solutions found`;
    } else if (numSolutions === 0) {
      solutionText = 'No solutions found';
    }

    return (
      <div className={styles['submit-panel']}>
        <div className={styles['submit-card']}>
          <div className={styles['title-bar']}>
            <div className={styles['actions-row']}>
              <Button
                className={styles['action-button']}
                clickHandler={this.props.clearAll}
              >
                Clear
              </Button>
              <Button
                className={styles['action-button']}
                clickHandler={this.props.submitPuzzle}
              >
                Submit
              </Button>
              <div className={styles['action-text']}>
                {submitString(this.props.submitState)}
              </div>
            </div>
            <UndoRedoButtonsContainer
              buttonClass={styles['history-button']}
            />
          </div>
          <div className={styles['instance-attribute']}>
            <div className={styles['attribute-title']}>
              Sudoku type
            </div>
            <select selected={this.props.sudokuType}>
              <option>Base</option>
            </select>
          </div>
          <div className={styles['instance-attribute']}>
            <div className={styles['attribute-title']}>
              <b>Name</b>
            </div>
            <input
              type="text"
              value={this.props.puzzleName}
              onChange={this.extractTargetValue(this.props.setPuzzleName)}
            />
          </div>
          <div className={styles['instance-attribute']}>
            <div className={styles['attribute-title']}>
              <b>Source</b> (e.g., URL, your name)
            </div>
            <input
              type="text"
              value={this.props.source}
              onChange={this.extractTargetValue(this.props.setSource)}
            />
          </div>
          <div className={styles['instance-attribute']}>
            <div className={styles['attribute-title']}>
              <b>Name</b> in original source
            </div>
            <input
              type="text"
              value={this.props.origName}
              onChange={this.extractTargetValue(this.props.setOrigName)}
            />
          </div>
          <div className={styles['instance-attribute']}>
            <div className={styles['attribute-title']}>
              <b>Description</b>
            </div>
            <textarea
              rows="4"
              value={this.props.description}
              onChange={this.extractTargetValue(this.props.setDescription)}
            />
          </div>
          <div className={styles['horizontal-rule']} />
          <div className={styles['instance-attribute']}>
            <div className={styles['attribute-title']}>
              <b>Values</b>
            </div>
            <input
              type="text"
              className={cn(
                styles['puzzle-values'],
                { [styles['error']]: this.props.puzzleValuesError }
              )}
              value={this.props.puzzleValues}
              onChange={this.extractTargetValue(this.props.setPuzzleValues)}
            />
          </div>
          <div className={styles['attribute-set-title']}>
            <b>Difficulty</b>
          </div>
          <div className={styles['attribute-row']}>
            <div className={styles['difficulty-section']}>
              Min
              <input
                type="text"
                className={cn({ [styles['error']]: this.props.minDiffLvlError })}
                value={this.props.minDiffLvl}
                onChange={this.extractTargetValue(this.props.setMinDiffLvl)}
              />
            </div>
            <div className={styles['difficulty-section']}>
              This instance
              <input
                type="text"
                className={cn({ [styles['error']]: this.props.diffLvlError })}
                value={this.props.diffLvl}
                onChange={this.extractTargetValue(this.props.setDiffLvl)}
              />
            </div>
            <div className={styles['difficulty-section']}>
              Max
              <input
                type="text"
                className={cn({ [styles['error']]: this.props.maxDiffLvlError })}
                value={this.props.maxDiffLvl}
                onChange={this.extractTargetValue(this.props.setMaxDiffLvl)}
              />
            </div>
          </div>
          <div className={styles['actions-row']}>
            <Button
              className={styles['action-button']}
              clickHandler={this.props.findMinimumConsistency}
            >
              Lowest Consistency
            </Button>
            <div className={styles['action-text']}>
              {this.props.consistency}
            </div>
          </div>
          <div className={styles['instance-attribute']}>
            <div className={styles['attribute-title']}>
              <b>Clues:</b> {this.props.numberOfClues}
            </div>
          </div>
          <div className={styles['actions-row']}>
            <Button
              className={styles['action-button']}
              clickHandler={this.props.findSolutions}
            >
              # Solutions
            </Button>
            <div className={styles['action-text']}>
              {solutionText}
            </div>
          </div>
          {
            this.props.solutionsRecord.solutions.length > 0 ? (
              <div className={styles['solution-list']}>
                {
                  solutions.map((result, i) => (
                    <div
                      key={`submit-solution-${i}`}
                      className={styles['solution']}
                    >
                      <div className={'title'}>
                        Solution {i + 1}
                      </div>
                      <BoardContainer
                        cells={result}
                        size="small"
                      />
                    </div>
                  ))
                }
              </div>
            ) : null
          }
        </div>
      </div>
    );
  }
}
