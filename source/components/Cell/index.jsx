import React, { Component } from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import _ from 'lodash';

import DomainValueStates from 'enumerations/DomainValueStates';
import CellStates from 'enumerations/CellStates';

import cn from 'classnames';
import styles from './style.scss';

export default class Cell extends Component {
  static propTypes = {
    id: PropTypes.string,
    assignment: PropTypes.number,
    minisVisible: PropTypes.bool,
    hovered: PropTypes.bool,
    domainValueStates: ImmutablePropTypes.map.isRequired,
    cellState: PropTypes.number,
    focusedCellId: PropTypes.string,
    focusedCellLevel: PropTypes.string,

    setCellValue: PropTypes.func.isRequired,
    removeCellValue: PropTypes.func.isRequired,
    contextMenuHandler: PropTypes.func.isRequired,
    setFocusedCellId: PropTypes.func.isRequired,
  };

  cellFocusHandler = () => {
    this.props.setFocusedCellId(this.props.id, 'click');
  }

  cellBlurHandler = () => {
    this.props.setFocusedCellId('', 'blur');
  }

  valueChangeHandler = (e) => {
    const cellValue = e.target.value;
    const parsedValue = parseInt(cellValue, 10);
    if (parsedValue >= 1 && parsedValue <= 9) {
      this.props.setCellValue(this.props.id, parsedValue);
      $(e.target).blur();
    } else if (cellValue === '') {
      this.props.removeCellValue(this.props.id);
    }
  }

  overlayClickHandler = (e) => {
    $(document).unbind('keydown');
    $(e.target.parentNode).find('input').get(0).focus();
  }

  mouseOverHandler = () => {
    if (this.props.focusedCellLevel !== 'click') {
      $(document).bind('keydown', (e) => {
        if (e.keyCode >= 49 && e.keyCode <= 57) {
          this.props.setCellValue(this.props.id, e.keyCode - 48);
        } else if (e.keyCode === 8) {
          this.props.removeCellValue(this.props.id);
        }
      });
    }
  }

  mouseOutHandler = () => {
    $(document).unbind('keydown');
  }

  contextMenuHandler = (e) => {
    e.preventDefault();
    this.props.contextMenuHandler(this.cell.getBoundingClientRect());
  }

  render() {
    const hideMinisClass = {
      [styles.hide]: !this.props.minisVisible
        || this.props.assignment
        || this.props.focusedCellId === this.props.id,
    };
    const miniNumbers = _.range(1, 10).map((i) => {
      let cellValueClass = '';
      switch (this.props.domainValueStates.get(i)) {
      case DomainValueStates.Possible:
        break;
      case DomainValueStates.ToBeRemoved:
        cellValueClass = styles['to-be-removed'];
        break;
      case DomainValueStates.BeingRemoved:
        cellValueClass = styles['being-removed'];
        break;
      case DomainValueStates.Removed:
        cellValueClass = styles.removed;
        break;
      default:
      }
      return (
        <div key={`mini-${this.props.id}:${i}`}
          className={cn(styles['mini-number'], cellValueClass)}
          value={i}
        >
          {i}
        </div>
      );
    });
    let assignStyle = '';
    let cellStyle = '';
    switch (this.props.cellState) {
    case CellStates.PuzzleLoaded:
      assignStyle = 'assigned';
      break;
    case CellStates.Inconsistent:
      cellStyle = 'inconsistent';
      break;
    default:
    }
    return (
      <div key={`cell-${this.props.id}`}
        id={`holder-${this.props.id}`}
        ref={(el) => { this.cell = el; }}
        className={cn(
          styles['cell-holder'],
          {
            [styles['hover']]: this.props.hovered,
            [styles[cellStyle]]: cellStyle !== '',
          }
        )}
        onContextMenu={this.contextMenuHandler}
      >
        <input type="number"
          min="1"
          max="9"
          step="1"
          id={this.props.id}
          className={cn(styles['cell'], styles[assignStyle])}
          value={this.props.assignment || ''}
          onChange={this.valueChangeHandler}
          onMouseOver={this.mouseOverHandler}
          onMouseOut={this.mouseOutHandler}
          onFocus={this.cellFocusHandler}
          onBlur={this.cellBlurHandler}
        />
        <div className={cn(styles['mini-numbers'], hideMinisClass)}>{miniNumbers}</div>
        <div className={cn(styles['overlay'], hideMinisClass)}
          onClick={this.overlayClickHandler}
          onMouseOver={this.mouseOverHandler}
          onMouseOut={this.mouseOutHandler}
        />
      </div>
    );
  }
}
