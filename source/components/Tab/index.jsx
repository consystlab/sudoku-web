import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import styles from './style.scss';

function Tab(props) {
  const clickHandler = () => {
    if (props.clickHandler) {
      props.clickHandler(props.value);
    }
  };

  const mouseOverHandler = () => {
    if (props.mouseOverHandler) {
      props.mouseOverHandler(props.value);
    }
  };

  const mouseOutHandler = () => {
    if (props.mouseOutHandler) {
      props.mouseOutHandler(props.value);
    }
  };

  const className = cn(styles['tab'], { [styles.selected]: props.selected });

  return (
    <div
      className={cn(className, props.className)}
      onClick={clickHandler}
      onMouseOver={mouseOverHandler}
      onMouseOut={mouseOutHandler}
    >
      {props.children}
    </div>
  );
}

Tab.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  value: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number,
  ]),
  selected: PropTypes.bool,

  clickHandler: PropTypes.func,
  mouseOverHandler: PropTypes.func,
  mouseOutHandler: PropTypes.func,
};

export default Tab;
