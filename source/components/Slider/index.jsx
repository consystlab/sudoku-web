import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ProgressBar, Glyphicon } from 'react-bootstrap';

import styles from './style.scss';

export default class Slider extends Component {
  static propTypes = {
    min: PropTypes.number.isRequired,
    max: PropTypes.number.isRequired,
    now: PropTypes.number.isRequired,
    active: PropTypes.bool,

    mouseOverHandler: PropTypes.func,
    mouseOutHandler: PropTypes.func,
    nowChangeHandler: PropTypes.func.isRequired,
  };

  constructor(props) {
    super(props);

    this.dragging = false;
    this.mousePressTimer = 0;
    this.keyPressTimer = 0;
    this.autoInterval = 0;
    this.isArrowPressed = false;
  }

  componentDidMount() {
    if (this.props.active) {
      window.addEventListener('keydown', this.sliderKeyDownHandler);
      window.addEventListener('keyup', this.sliderKeyUpHandler);
    }
  }

  componentWillReceiveProps(newProps) {
    if (newProps.active && !this.props.active) {
      window.addEventListener('keydown', this.sliderKeyDownHandler);
      window.addEventListener('keyup', this.sliderKeyUpHandler);
    } else if (!newProps.active && this.props.active) {
      window.removeEventListener('keydown', this.sliderKeyDownHandler);
      window.removeEventListener('keyup', this.sliderKeyUpHandler);
    }
  }

  componentWillUnmount() {
    window.removeEventListener('keydown', this.sliderKeyDownHandler);
    window.removeEventListener('keyup', this.sliderKeyUpHandler);
  }

  sliderMouseOverHandler = e => {
    this.props.mouseOverHandler(e);
  }

  sliderMouseOutHandler = e => {
    this.props.mouseOutHandler(e);
  }

  sliderKeyDownHandler = e => {
    if ((e.key === 'ArrowLeft' || e.key === 'ArrowRight') && !this.isArrowPressed) {
      this.isArrowPressed = true;
      const amount = e.key === 'ArrowLeft' ? -1 : 1;
      let newIndex = this.props.now + amount;
      if (newIndex > this.props.max) {
        newIndex = this.props.max;
      } else if (newIndex < this.props.min) {
        newIndex = this.props.min;
      }
      this.props.nowChangeHandler(newIndex);

      this.keyPressTimer = window.setTimeout(() => {
        clearTimeout(this.keyPressTimer);
        clearTimeout(this.autoInterval);
        this.autoInterval = window.setInterval(() => {
          let nIndex = this.props.now + amount;
          if (nIndex > this.props.max) {
            nIndex = this.props.max;
          } else if (nIndex < this.props.min) {
            nIndex = this.props.min;
          }
          this.props.nowChangeHandler(nIndex);
        }, 100);
      }, 750);
    }
  }

  sliderKeyUpHandler = e => {
    if (e.key === 'ArrowLeft' || e.key === 'ArrowRight') {
      this.isArrowPressed = false;
      clearTimeout(this.keyPressTimer);
      clearTimeout(this.mousePressTimer);
      clearInterval(this.autoInterval);
    }
  }

  indexClickHandler = (amount) => {
    if (amount !== 1 && amount !== -1) {
      throw new TypeError(`Slider::indexClickHandler: Incorrect amount: ${amount}`);
    }
    let newIndex = this.props.now + amount;
    if (newIndex > this.props.max) {
      newIndex = this.props.max;
    } else if (newIndex < this.props.min) {
      newIndex = this.props.min;
    }
    this.props.nowChangeHandler(newIndex);
  }

  stepperMouseDownHandler = (amount) => {
    if (amount !== 1 && amount !== -1) {
      throw new TypeError(`RemoveHistoryContainer::moveLocalIndexClickHandler: Incorrect amount: ${amount}`);
    }
    let newIndex = this.props.now + amount;
    if (newIndex > this.props.max) {
      newIndex = this.props.max;
    } else if (newIndex < this.props.min) {
      newIndex = this.props.min;
    }
    this.props.nowChangeHandler(newIndex);

    this.mousePressTimer = window.setTimeout(() => {
      this.autoInterval = window.setInterval(() => {
        let nIndex = this.props.now + amount;
        if (nIndex > this.props.max) {
          nIndex = this.props.max;
        } else if (nIndex < this.props.min) {
          nIndex = this.props.min;
        }
        this.props.nowChangeHandler(nIndex);
      }, 100);
    }, 1000);
  }

  stepperMouseUpHandler = () => {
    clearTimeout(this.keyPressTimer);
    clearTimeout(this.mousePressTimer);
    clearInterval(this.autoInterval);
  }

  progressClickHandler = (e) => {
    let progressBar = e.target;
    if (progressBar.className === 'progress-bar') progressBar = progressBar.parentElement;
    let deltax = e.clientX - $(progressBar).offset().left;
    if (deltax < 0) deltax = 0;
    if (deltax > progressBar.offsetWidth) deltax = progressBar.offsetWidth;
    const step = Math.round(deltax / progressBar.offsetWidth * (this.props.max - this.props.min)) + this.props.min;
    this.props.nowChangeHandler(step);
  }

  progressMouseDownHandler = () => {
    this.dragging = true;
  }

  progressMouseMoveHandler = (e) => {
    if (this.dragging) {
      let progressBar = e.target;
      if (progressBar.className === 'progress-bar') progressBar = progressBar.parentElement;
      let deltax = e.clientX - $(progressBar).offset().left;
      if (deltax < 0) deltax = 0;
      if (deltax > progressBar.offsetWidth) deltax = progressBar.offsetWidth;
      const step = Math.round(deltax / progressBar.offsetWidth * (this.props.max - this.props.min)) + this.props.min;
      this.props.nowChangeHandler(step);
    }
  }

  progressMouseUpHandler = () => {
    this.dragging = false;
  }

  progressMouseOutHandler = (e) => {
    if (e.className === 'progress') {
      this.dragging = false;
    }
  }

  render() {
    return (
      <div className={styles['slider-container']}>
        <Glyphicon glyph="triangle-left"
          className={styles['navigation-icon']}
          onMouseDown={() => { this.stepperMouseDownHandler(-1); }}
          onMouseUp={this.stepperMouseUpHandler}
        />
        <ProgressBar className={styles['progress-bar']}
          min={this.props.min}
          max={this.props.max}
          now={this.props.now}
          label={this.props.now}
          onClick={this.progressClickHandler}
          onMouseDown={this.progressMouseDownHandler}
          onMouseMove={this.progressMouseMoveHandler}
          onMouseUp={this.progressMouseUpHandler}
          onMouseOut={this.progressMouseOutHandler}
        />
        <Glyphicon glyph="triangle-right"
          className={styles['navigation-icon']}
          onMouseDown={() => { this.stepperMouseDownHandler(1); }}
          onMouseUp={this.stepperMouseUpHandler}
        />
        <div className={styles.left}
          style={{ paddingTop: '2px' }}
        >
          {this.props.max}
        </div>
      </div>
    );
  }
}
