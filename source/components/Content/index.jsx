import React from 'react';
import PropTypes from 'prop-types';
import cn from 'classnames';
import styles from './style.scss';

function Content(props) {
  return props.selected ? (
    <div className={cn({ [styles.hidden]: !props.selected })}>
      {props.children}
    </div>
  ) : null;
}

Content.propTypes = {
  selected: PropTypes.bool,

  children: PropTypes.node,
};

export default Content;
