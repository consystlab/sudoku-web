import { connect } from 'react-redux';
import Immutable from 'immutable';
import SolverHistory from 'components/SolverHistory';

import {
  historySeek,
} from 'actions/atomic/boardActions';

import {
  setVariablesHovered,
} from 'actions/atomic/boardUIActions';

import {
  toggleChildrenExpanded,
  convertLoadHistory,
} from 'actions/atomic/historyActions';

const mapStateToProps = (state, ownProps) => ({
  className: ownProps.className,
  histories: state.board.get('histories'),
  currentIndex: state.board.get('currentIndex') - 1,
  currentChildIndex: state.board.get('currentChildIndex'),
  hoveredIndex: state.board.get('hoveredIndex'),
  solutionsDiff: state.solverSettings.get('solutionsDiff'),
});

const mapDispatchToProps = dispatch => ({
  historySeek: (index, childIndex) => {
    dispatch(historySeek(index + 1, childIndex));
  },
  historyMouseOverHandler: variables => {
    dispatch(setVariablesHovered(variables));
  },
  historyMouseOutHandler: () => {
    dispatch(setVariablesHovered(Immutable.Map()));
  },
  childrenExpanderClickHandler: index => {
    dispatch(toggleChildrenExpanded(index));
  },
  setVariablesHovered: variables => {
    dispatch(setVariablesHovered(variables));
  },
  convertLoadHistory: (index) => {
    dispatch(convertLoadHistory(index));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SolverHistory);
