import { connect } from 'react-redux';
import SubmitPuzzlePanel from 'components/SubmitPuzzlePanel';

import {
  setPuzzleName,
  setSudokuType,
  setSource,
  setMinDiffLvl,
  setDiffLvl,
  setMaxDiffLvl,
  setOrigName,
  setDescription,
  setPuzzleValues,
} from 'actions/atomic/submitPuzzleActions';
import {
  clearAll,
} from 'actions/atomic/globalActions';
import {
  submitSearch,
} from 'actions/sequence/searchActions';
import {
  findMinimumConsistency,
} from 'actions/sequence/propagationActions';
import {
  submitPuzzle,
  checkPuzzle,
} from 'actions/sequence/submitActions';

import {
  getRealPuzzleValueString,
  getRealNumberOfClues,
} from 'selectors/puzzleValuesSelector';

const mapStateToProps = (state) => ({
  puzzleName: state.submitPuzzle.get('puzzleName'),
  sudokuType: state.submitPuzzle.get('sudokuType'),
  source: state.submitPuzzle.get('source'),
  minDiffLvl: state.submitPuzzle.get('minDiffLvl'),
  minDiffLvlError: state.submitPuzzle.get('minDiffLvlError'),
  diffLvl: state.submitPuzzle.get('diffLvl'),
  diffLvlError: state.submitPuzzle.get('diffLvlError'),
  maxDiffLvl: state.submitPuzzle.get('maxDiffLvl'),
  maxDiffLvlError: state.submitPuzzle.get('maxDiffLvlError'),
  origName: state.submitPuzzle.get('origName'),
  description: state.submitPuzzle.get('description'),
  puzzleValues: getRealPuzzleValueString(state),
  puzzleValuesError: state.submitPuzzle.get('puzzleValuesError'),
  numberOfClues: getRealNumberOfClues(state),

  solutionsRecord: state.submitPuzzle.get('solutionsRecord'),
  maxSolutions: state.submitPuzzle.get('maxSolutions'),
  solutionsDiff: state.solverSettings.get('solutionsDiff'),

  consistency: state.submitPuzzle.get('consistency'),

  submitState: state.submitPuzzle.get('submitState'),
});

const mapDispatchToProps = (dispatch) => ({
  findMinimumConsistency: () => {
    dispatch(findMinimumConsistency());
  },
  findSolutions: () => {
    dispatch(submitSearch());
  },
  setPuzzleName: (name) => {
    dispatch(setPuzzleName(name));
  },
  setSudokuType: (sudokuType) => {
    dispatch(setSudokuType(sudokuType));
  },
  setSource: (source) => {
    dispatch(setSource(source));
  },
  setMinDiffLvl: (level) => {
    dispatch(setMinDiffLvl(level));
  },
  setDiffLvl: (level) => {
    dispatch(setDiffLvl(level));
  },
  setMaxDiffLvl: (level) => {
    dispatch(setMaxDiffLvl(level));
  },
  setOrigName: (name) => {
    dispatch(setOrigName(name));
  },
  setDescription: (description) => {
    dispatch(setDescription(description));
  },
  setPuzzleValues: (puzzleValues) => {
    dispatch(setPuzzleValues(puzzleValues));
  },
  submitPuzzle: () => {
    dispatch(submitPuzzle());
  },
  checkPuzzle: () => {
    dispatch(checkPuzzle());
  },
  clearAll: () => {
    dispatch(clearAll());
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SubmitPuzzlePanel);
