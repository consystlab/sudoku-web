import { connect } from 'react-redux';
import { Propagations } from 'consyst';

import PropagationTools from 'components/PropagationTools';

import {
  performArc,
  performGac,
  performSac,
  performSgac,
  performPoac,
  performPogac,
  performBisac,
  performBisgac,
  performSsac,
  performSsgac,
} from 'actions/sequence/propagationActions';

import {
  setPropagationPosition,
  setPropagationStyle,
} from 'actions/atomic/propagationActions';

const mapStateToProps = (state, ownProps) => ({
  showExplanations: state.solverSettings.get('showExplanations'),
  positions: state.propagations.get('positions'),
  styles: state.propagations.get('styles'),

  className: ownProps.className,
});

const mapDispatchToProps = dispatch => ({
  performPropagation: level => {
    // Dispatch the different stuff
    switch (level) {
    case Propagations.AC: dispatch(performArc()); break;
    case Propagations.GAC: dispatch(performGac()); break;
    case Propagations.SAC: dispatch(performSac()); break;
    case Propagations.SGAC: dispatch(performSgac()); break;
    case Propagations.POAC: dispatch(performPoac()); break;
    case Propagations.POGAC: dispatch(performPogac()); break;
    case Propagations.BiSAC: dispatch(performBisac()); break;
    case Propagations.BiSGAC: dispatch(performBisgac()); break;
    case Propagations.SSAC: dispatch(performSsac()); break;
    case Propagations.SSGAC: dispatch(performSsgac()); break;
    default:
      throw new RangeError(`AppContainer.propagationClickHandler: Invalid level given: ${level}`);
    }
  },
  setPosition: (id, position) => {
    dispatch(setPropagationPosition(id, position));
  },
  setStyle: (id, style) => {
    dispatch(setPropagationStyle(id, style));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(PropagationTools);
