import { connect } from 'react-redux';

import UploadPanel from 'components/UploadPanel';

import {
  setCanvasData,
  sudokuImageUploaded,
} from 'actions/atomic/uploadActions';

const mapStateToProps = (state) => ({
  canvasData: state.upload.get('canvasData'),
});

const mapDispatchToProps = (dispatch) => ({
  onImageProcessed: ({ array, canvasData }) => {
    dispatch(sudokuImageUploaded(array));
    dispatch(setCanvasData(canvasData));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(UploadPanel);
