import { connect } from 'react-redux';
import RemoveDialog from 'components/RemoveDialog';

import {
  removeUserValue,
} from 'actions/sequence/propagationActions';
import {
  hideRemoveDialog,
} from 'actions/atomic/controlPanelActions';

import { getVariable,
} from 'selectors/removeDialogSelector';

const mapStateToProps = state => {
  const variable = getVariable(state);
  const domain = variable ? variable.domain.current() : [];

  return {
    variable,
    domain,
    cellRect: state.controlPanel.get('removeDialogRect'),
    visible: state.controlPanel.get('removeDialogVisible'),
  };
};

const mapDispatchToProps = dispatch => ({
  removeValue: (value, variable) => {
    dispatch(removeUserValue(value, variable));
  },
  hideRemoveDialog: () => {
    dispatch(hideRemoveDialog());
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RemoveDialog);
