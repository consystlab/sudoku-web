import { connect } from 'react-redux';

import Cell from 'components/Cell';

import {
  assignVariableValue,
  unassignVariable,
} from 'actions/sequence/propagationActions';
import {
  setFocusedCellId,
} from 'actions/atomic/boardUIActions';
import {
  callRemoveDialog,
  hideRemoveDialog,
} from 'actions/atomic/controlPanelActions';

import {
  isHovered,
} from 'selectors/cellSelector';

/*
ownProps = {
  col: number,
  row: number,
};
*/

const mapStateToProps = (state, ownProps) => {
  const variableId = `${ownProps.row}_${ownProps.col}`;
  return {
    id: variableId,
    domain: state.board.get('domains').get(variableId),
    assignment: state.board.get('assignments').get(variableId),
    minisVisible: state.controlPanel.get('minisVisible'),
    hovered: isHovered(state)[ownProps.row][ownProps.col],
    domainValueStates: state.board.get('domainValueStates').get(variableId),
    cellState: state.board.get('cellStates').get(variableId),
    focusedCellId: state.board.get('focusedCellId'),
    focusedCellLevel: state.board.get('focusedCellLevel'),
  };
};

const mapDispatchToProps = (dispatch, ownProps) => ({
  setCellValue: (variableId, value) => {
    dispatch(assignVariableValue(variableId, value));
  },
  removeCellValue: (variableId) => {
    dispatch(unassignVariable(variableId));
  },
  contextMenuHandler: (clientRect) => {
    dispatch(callRemoveDialog(clientRect, ownProps.row, ownProps.col));

    const removeValueDialogHandler = e => {
      if (!$(e.target).parents('#remove-value-dialog').length) {
        document.body.removeEventListener('click', removeValueDialogHandler);
        dispatch(hideRemoveDialog());
      }
    };

    document.body.addEventListener('click', removeValueDialogHandler);
  },
  setFocusedCellId: (cellId, level) => {
    dispatch(setFocusedCellId(cellId, level));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Cell);
