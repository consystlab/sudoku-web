import { connect } from 'react-redux';
import UndoRedoButtons from 'components/UndoRedoButtons';

import {
  historySeek,
} from 'actions/atomic/boardActions';

const mapStateToProps = (state, ownProps) => ({
  className: ownProps.className,
  histories: state.board.get('histories'),
  currentIndex: state.board.get('currentIndex') - 1,
  buttonClass: ownProps.buttonClass,
});

const mapDispatchToProps = dispatch => ({
  historySeek: (index, childIndex) => {
    dispatch(historySeek(index + 1, childIndex));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(UndoRedoButtons);
