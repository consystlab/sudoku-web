import { connect } from 'react-redux';

import SettingsPanel from 'components/SettingsPanel';

import {
  setMaxSaveResults,
  setMaxSearchResults,
  toggleShowExplanations,
  setSolutionsDiff,
} from 'actions/atomic/solverSettingsActions';


const mapStateToProps = state => ({
  maxSaveResultsText: state.solverSettings.get('maxSaveResultsText'),
  maxSaveResultsError: state.solverSettings.get('maxSaveResultsError'),
  maxSearchResultsText: state.solverSettings.get('maxSearchResultsText'),
  maxSearchResultsError: state.solverSettings.get('maxSearchResultsError'),
  showExplanations: state.solverSettings.get('showExplanations'),
  solutionsDiff: state.solverSettings.get('solutionsDiff'),
});

const mapDispatchToProps = dispatch => ({
  setMaxSaveResults: (max) => {
    dispatch(setMaxSaveResults(max));
  },
  setMaxSearchResults: (max) => {
    dispatch(setMaxSearchResults(max));
  },
  toggleShowExplanations: () => {
    dispatch(toggleShowExplanations());
  },
  setSolutionsDiff: (diff) => {
    dispatch(setSolutionsDiff(diff));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SettingsPanel);
