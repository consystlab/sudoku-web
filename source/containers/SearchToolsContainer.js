import { connect } from 'react-redux';

import SearchTools from 'components/SearchTools';

import {
  performFcSearch,
} from 'actions/sequence/searchActions';

const mapStateToProps = state => ({
  solvingMethodSelected: state.board.get('solverMethod'),
});

const mapDispatchToProps = dispatch => ({
  findAllSolutions: () => {
    dispatch(performFcSearch(false));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(SearchTools);
