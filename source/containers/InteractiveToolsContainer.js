import { connect } from 'react-redux';

import InteractiveTools from 'components/InteractiveTools';

import {
  setSolvingMethod,
} from 'actions/sequence/filteringActions';

const mapStateToProps = (state, ownProps) => ({
  showExplanations: state.solverSettings.get('showExplanations'),
  solvingMethodSelected: state.board.get('solverMethod'),

  className: ownProps.className,
});

const mapDispatchToProps = dispatch => ({
  performMethodSet: (method) => {
    dispatch(setSolvingMethod(method));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(InteractiveTools);
