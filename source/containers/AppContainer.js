import { connect } from 'react-redux';

import App from 'components/App';
import {
  performArcRow,
  performArcCol,
  performGacRow,
  performGacCol,
} from 'actions/sequence/propagationActions';
import {
  startUp,
} from 'actions/sequence/startUpActions';
import {
  setRowHovered,
  setColHovered,
} from 'actions/atomic/boardUIActions';

const mapStateToProps = () => ({

});

const mapDispatchToProps = dispatch => ({
  arcRowClickHandler: row => {
    dispatch(performArcRow(row));
  },
  arcColClickHandler: col => {
    dispatch(performArcCol(col));
  },
  gacRowClickHandler: row => {
    dispatch(performGacRow(row));
  },
  gacColClickHandler: col => {
    dispatch(performGacCol(col));
  },
  rowMouseOverHandler: row => {
    dispatch(setRowHovered(row));
  },
  rowMouseOutHandler: () => {
    dispatch(setRowHovered(null));
  },
  colMouseOverHandler: col => {
    dispatch(setColHovered(col));
  },
  colMouseOutHandler: () => {
    dispatch(setColHovered(null));
  },
  startUpHandler: () => {
    dispatch(startUp());
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(App);
