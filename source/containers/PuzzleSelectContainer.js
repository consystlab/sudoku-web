import { connect } from 'react-redux';
import PuzzleSelect from 'components/PuzzleSelect';

import {
  loadPuzzle,
  updateSortField,
  setPuzzleFilter,
  setSelectorVisibility,
  setColumnVisibility,
} from 'actions/atomic/puzzleActions';

import {
  orderedPuzzles,
} from 'selectors/puzzleSelector';

const mapStateToProps = state => ({
  puzzles: orderedPuzzles(state),
  types: state.puzzle.get('types'),
  maxLevels: state.puzzle.get('maxLevels'),
  levels: state.puzzle.get('levels'),
  numbersOfSolutions: state.puzzle.get('numbersOfSolutions'),
  numbersOfClues: state.puzzle.get('numbersOfClues'),
  consistencies: state.puzzle.get('consistencies'),

  type: state.puzzle.get('type'),
  maxLevel: state.puzzle.get('maxLevel'),
  level: state.puzzle.get('level'),
  numberOfSolutions: state.puzzle.get('numberOfSolutions'),
  numberOfClues: state.puzzle.get('numberOfClues'),
  consistency: state.puzzle.get('consistency'),

  sortField: state.puzzle.get('sortField'),
  sortDirection: state.puzzle.get('sortDirection'),

  showColumnSelector: state.puzzle.get('showColumnSelector'),
  nameVisible: state.puzzle.get('nameVisible'),
  consistencyVisible: state.puzzle.get('consistencyVisible'),
  cluesVisible: state.puzzle.get('cluesVisible'),
  solutionsVisible: state.puzzle.get('solutionsVisible'),
  levelVisible: state.puzzle.get('levelVisible'),
  sourceVisible: state.puzzle.get('sourceVisible'),
});

const mapDispatchToProps = dispatch => ({
  loadPuzzle: puzzle => {
    dispatch(loadPuzzle(puzzle));
  },
  updateSortField: field => {
    dispatch(updateSortField(field));
  },
  setPuzzleFilter: (puzzleFilter, value) => {
    let v = value;
    if (typeof value === 'string') {
      v = value.toLowerCase() === '*' ? null : value;
    } else if (isNaN(v)) {
      v = null;
    }
    dispatch(setPuzzleFilter(puzzleFilter, v));
  },
  setSelectorVisibility: (visible) => {
    dispatch(setSelectorVisibility(visible));
  },
  setColumnVisibility: (column, visible) => {
    dispatch(setColumnVisibility(column, visible));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PuzzleSelect);
