import { connect } from 'react-redux';

import {
  historySeek,
} from 'actions/atomic/boardActions';
import {
  toggleMinisVisible,
  toggleAutoSingleton,
} from 'actions/atomic/controlPanelActions';
import {
  assignSingletons,
} from 'actions/sequence/propagationActions';
import {
  setModalVisible,
} from 'actions/atomic/submitPuzzleActions';

import BoardFeatures from 'components/BoardFeatures';

const mapStateToProps = state => ({
  minisVisible: state.controlPanel.get('minisVisible'),
  autoSingleton: state.controlPanel.get('autoSingleton'),
});

const mapDispatchToProps = dispatch => ({
  assignSingletons: () => {
    dispatch(assignSingletons());
  },
  resetGrid: () => {
    dispatch(historySeek(1, 0));
  },
  toggleMinisVisible: () => {
    dispatch(toggleMinisVisible());
  },
  toggleAutoSingleton: () => {
    dispatch(toggleAutoSingleton());
  },
  submitPuzzle: () => {
    dispatch(setModalVisible(true));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(BoardFeatures);
