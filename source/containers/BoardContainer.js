import { connect } from 'react-redux';

import Board from 'components/Board';

const mapStateToProps = (state, ownProps) => ({
  width: state.board.get('width'),
  height: state.board.get('height'),
  groupx: state.board.get('groupx'),
  groupy: state.board.get('groupy'),

  ...ownProps,
});

const mapDispatchToProps = dispatch => ({
  infoMouseOverhandler: () => {
    dispatch({});
  },
  infoMouseoutHandler: () => {

  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Board);
