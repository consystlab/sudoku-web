import { connect } from 'react-redux';

import ControlPanel from 'components/ControlPanel';
import {
  setControlSection,
} from 'actions/atomic/controlPanelActions';

const mapStateToProps = state => ({
  selectedTab: state.controlPanel.get('selectedTab'),
});

const mapDispatchToProps = dispatch => ({
  tabButtonClickHandler: index => {
    dispatch(setControlSection(index));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ControlPanel);
