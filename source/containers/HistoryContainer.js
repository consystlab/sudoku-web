import { connect } from 'react-redux';
import SolverHistory from 'components/SolverHistory';

import {
  historySeek,
} from 'actions/atomic/boardActions';

import {
  historyMouseOver,
  historyMouseOut,
} from 'actions/atomic/historyActions';

const mapStateToProps = state => ({
  histories: state.board.get('histories'),
  currentIndex: state.board.get('currentIndex'),
  hoveredIndex: state.board.get('hoveredIndex'),
});

const mapDispatchToProps = dispatch => ({
  onSolverHistoryClick: index => {
    dispatch(historySeek(index));
  },
  onHistoryMouseOver: index => {
    dispatch(historyMouseOver(index));
  },
  onHistoryMouseOut: index => {
    dispatch(historyMouseOut(index));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SolverHistory);
