import { connect } from 'react-redux';

import GroupConsistency from 'components/GroupConsistency';

import {
  performArcGroup,
  performGacGroup,
} from 'actions/sequence/propagationActions';
import {
  setGroupHovered,
} from 'actions/atomic/boardUIActions';

const mapStateToProps = (state, ownProps) => ({
  showExplanations: state.solverSettings.get('showExplanations'),

  className: ownProps.className,
});

const mapDispatchToProps = dispatch => ({
  groupMouseOverHandler: group => {
    dispatch(setGroupHovered(group));
  },
  groupMouseOutHandler: () => {
    dispatch(setGroupHovered(null));
  },
  arcGroupClickHandler: group => {
    dispatch(performArcGroup(group));
  },
  gacGroupClickHandler: group => {
    dispatch(performGacGroup(group));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(GroupConsistency);
