import { Record } from 'immutable';

const RemoveHistoryRecord = Record({
  actions: [], // Array<RemoveAction | InconsistentAction>
  removeActions: [], // Array<RemoveAction>
  variablesAffected: [], // Array<Variable>
  remaining: {}, // { [key: string]: [CellContainer, Set<number>] }
  method: -1, // Propagation
  rowColOrGroup: null, // Gives the row, col, or group number if used
  errors: new Set(),

  childrenExpanded: false,
  name: 'RemoveHistoryRecord',
});

export default RemoveHistoryRecord;
