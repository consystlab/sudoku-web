import { Record } from 'immutable';

const SingletonAssignHistoryRecord = Record({
  actions: [], // [SingletonAssignAction]
  variablesAffected: [], // Array<Variable>
  assignments: {},

  childrenExpanded: false,
  name: 'SingletonAssignHistoryRecord',
});

export default SingletonAssignHistoryRecord;
