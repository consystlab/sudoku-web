import { Record } from 'immutable';
import { Propagations } from 'consyst';

const MethodHistoryRecord = Record({
  actions: [], // [MethodAction, ...RemoveActions]
  setAction: null, // MethodAction
  removeActions: [], // Array<RemoveAction>
  method: Propagations.NONE,
  errors: new Set(),

  childrenExpanded: false,
  name: 'MethodHistoryRecord',
});

export default MethodHistoryRecord;
