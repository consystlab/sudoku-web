import { Record } from 'immutable';

const AssignHistoryRecord = Record({
  actions: [],
  assignActions: [], // Array<AssignAction>
  removeActions: [], // Array<RemoveAction>
  variablesAffected: [], // Array<Variable>
  assignment: 0, // number
  method: 0, // Propagation
  remaining: {}, // { [key: string]: [Variable, Set<number>] }
  errors: new Set(), // Set(variable Ids)

  childrenExpanded: false,
  name: 'AssignHistoryRecord', // Do not modifiy
});

export default AssignHistoryRecord;
