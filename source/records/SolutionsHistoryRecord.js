import Immutable, { Record } from 'immutable';
import { SearchResults } from 'consyst';
import SearchCount from 'enumerations/SearchCount';

const SolutionsHistoryRecord = Record({
  actions: [],
  solutions: [],
  searchFor: SearchCount.One,
  status: SearchResults.UNKNOWN,
  maxSolutions: 50,
  differingAllCells: Immutable.Map(),
  differingPairwiseCells: Immutable.List(),
  originalAssignments: Immutable.Map(),

  childrenExpanded: false,
  name: 'SolutionsHistoryRecord',
});

export default SolutionsHistoryRecord;
