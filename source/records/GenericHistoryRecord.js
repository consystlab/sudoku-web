import { Record } from 'immutable';

const GenericHistoryRecord = Record({
  actions: [],
  title: '',

  name: 'GenericHistoryRecord', // Do not modify
});

export default GenericHistoryRecord;
