import Immutable, { Record } from 'immutable';

const SolutionRecord = Record({
  assignments: Immutable.Map(),
});

export default SolutionRecord;
