import { Record } from 'immutable';

const LoadHistoryRecord = Record({
  actions: [], // [LoadAction]
  assignments: {}, // { [key: string]: [Variable, number] }
  puzzleName: '', // string
  consistency: null, // string

  childrenExpanded: false,
  name: 'LoadHistoryRecord',
});

export default LoadHistoryRecord;
