import { Propagations } from 'consyst';
import groupName from 'util/groupName';

const propagationText = (method, rowColOrGroup) => {
  switch (method) {
  case Propagations.None:
    return '';
  case Propagations.AC:
    return 'AC';
  case Propagations.GAC:
    return 'GAC';
  case Propagations.SAC:
    return 'SAC';
  case Propagations.SGAC:
    return 'SGAC';
  case Propagations.POAC:
    return 'POAC';
  case Propagations.POGAC:
    return 'POGAC';
  case Propagations.BiSAC:
    return 'BiSAC';
  case Propagations.BiSGAC:
    return 'BiSGAC';
  case Propagations.SSAC:
    return 'SSAC';
  case Propagations.SSGAC:
    return 'SSGAC';
  case Propagations.COL_AC:
    return `Col ${rowColOrGroup + 1}: AC`;
  case Propagations.ROW_AC:
    return `Row ${String.fromCharCode(65 + rowColOrGroup)}: AC`;
  case Propagations.GRO_AC:
    return `Box ${groupName(rowColOrGroup)}: AC`;
  case Propagations.COL_GAC:
    return `Col ${rowColOrGroup + 1}: GAC`;
  case Propagations.ROW_GAC:
    return `Row ${String.fromCharCode(65 + rowColOrGroup)}: GAC`;
  case Propagations.GRO_GAC:
    return `Box ${groupName(rowColOrGroup)}: GAC`;
  case Propagations.BC:
    return 'BC';
  case Propagations.BFC:
    return 'Binary FC';
  case Propagations.BRFL:
    return 'Binary RFL';
  case Propagations.NBFC:
    return 'Nonbinary FC';
  case Propagations.NBRFL:
    return 'Nonbinary RFL';
  case Propagations.USER:
    return 'User';
  default:
    throw new Error(`util/propagationText bad method given: ${method}`);
  }
};

export default propagationText;
