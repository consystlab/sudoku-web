import SubmitStates from 'enumerations/SubmitStates';

const submitStrings = {
  [SubmitStates['NOTHING']]: ' ',
  [SubmitStates['NO_NAME']]: 'Give this instance a name',
  [SubmitStates['MIN_DIFF_ERROR']]: 'Fix your min difficulty',
  [SubmitStates['DIFF_ERROR']]: 'Fix your instance difficulty',
  [SubmitStates['MAX_DIFF_ERROR']]: 'Fix your max difficulty',
  [SubmitStates['DIFF_LESSER']]: 'Difficulty is greather than maximum',
  [SubmitStates['DIFF_GREATER']]: 'Difficulty is less than minimum',
  [SubmitStates['INCONSISTENT_PUZZLE']]: 'Inconsistent Puzzle',
  [SubmitStates['TOO_MANY_SOLUTIONS']]: 'Too many solutions found',
  [SubmitStates['NAME_EXISTS']]: 'Name in database already',
  [SubmitStates['PUZZLE_EXISTS']]: 'Values exist in database already',
  [SubmitStates['PUZZLE_SUBMITTED']]: 'Submitted puzzle to the database',
  [SubmitStates['PUZZLE_ACCEPTED']]: 'Puzzle accepted by the database',
  [SubmitStates['CONTACT_SERVER_ERROR']]: 'Error contacting server',
  [SubmitStates['UNKNOWN_ERROR']]: 'Unknown error message recieved',
};

const submitString = (submitState) => submitStrings[submitState] || 'Submit state not found error';

export default submitString;
