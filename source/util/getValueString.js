const getValueString = (board) => {
  const puzzleValuesArray = [];
  for (let i = 0; i < 9; i++) {
    for (let j = 0; j < 9; j++) {
      const variable = board.get('variables')[i][j];
      if (variable.isAssigned()) {
        puzzleValuesArray.push(variable.getAssignment());
      } else {
        puzzleValuesArray.push(0);
      }
    }
  }
  return puzzleValuesArray.join('');
};

export default getValueString;
