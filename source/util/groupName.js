const groupName = (groupNumber) => {
  switch (groupNumber) {
  case 0:
    return 'A1-C3';
  case 1:
    return 'A4-C6';
  case 2:
    return 'A7-C9';
  case 3:
    return 'D1-F3';
  case 4:
    return 'D4-F6';
  case 5:
    return 'D7-F9';
  case 6:
    return 'G1-I3';
  case 7:
    return 'G4-I6';
  case 8:
    return 'G7-I9';
  default:
    throw new Error(`util/propagationText/groupName bad groupNumber: ${groupNumber}`);
  }
};

export default groupName;
