import { VariableValuePairs } from 'consyst';

export default sudoku => {
  const errors = new Set();
  sudoku.binaryConstraints.forEach(constraint => {
    if (constraint.scope[0].isAssigned() && constraint.scope[1].isAssigned()) {
      if (!constraint.check(new VariableValuePairs([
        {
          key: constraint.scope[0],
          value: constraint.scope[0].domain.first(),
        }, {
          key: constraint.scope[1],
          value: constraint.scope[1].domain.first(),
        },
      ]))) {
        errors.add(constraint.scope[0]);
        errors.add(constraint.scope[1]);
      }
    }
  });
  return errors;
};
