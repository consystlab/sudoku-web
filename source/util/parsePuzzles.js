import PuzzleModel from 'models/PuzzleModel';

const parsePuzzles = lines => {
  const types = [];
  const maxLevels = [];
  const levels = [];
  const numbersOfClues = [];
  const numbersOfSolutions = [];
  const consistencies = [];
  const puzzles = lines.map(line => {
    const p = PuzzleModel.parse(line);
    if (types.indexOf(p.type) < 0) {
      types.push(p.type);
    }
    if (maxLevels.indexOf(p.maxdifflvl) < 0) {
      maxLevels.push(p.maxdifflvl);
    }
    if (levels.indexOf(p.difflvl) < 0) {
      levels.push(p.difflvl);
    }
    if (numbersOfSolutions.indexOf(p.solutions) < 0) {
      numbersOfSolutions.push(p.solutions);
    }
    if (consistencies.indexOf(p.consistency) < 0) {
      consistencies.push(p.consistency);
    }
    if (numbersOfClues.indexOf(p.clues) < 0) {
      numbersOfClues.push(p.clues);
    }
    return p;
  }).reverse();
  return {
    puzzles,
    types,
    maxLevels,
    levels,
    numbersOfClues,
    numbersOfSolutions,
    consistencies,
  };
};

export default parsePuzzles;
