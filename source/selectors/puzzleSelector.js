import { createSelector } from 'reselect';
import Immutable from 'immutable';

/* eslint-disable */
const consistencyOrder = {
  'AC': 0,
  'GAC': 1,
  'SAC': 2,
  'SAC + GAC': 3,
  'SGAC': 4,
  'POAC': 5,
  'SGAC + POAC': 6,
  'POGAC': 7,
  'BiSAC': 8,
  'POGAC + BiSAC': 9,
  'BiSGAC': 10,
  'SSAC': 11,
  'BiSGAC + SSAC': 12,
  'SSGAC': 13,
  'Unsolved': 14,
};
/* eslint-enable */

const getPuzzles = state => state.puzzle.get('puzzles');
const getType = state => state.puzzle.get('type');
const getMaxLevel = state => state.puzzle.get('maxLevel');
const getLevel = state => state.puzzle.get('level');
const getNumberOfClues = state => state.puzzle.get('numberOfClues');
const getNumberOfSolutions = state => state.puzzle.get('numberOfSolutions');
const getConsistency = state => state.puzzle.get('consistency');

const getSortField = state => state.puzzle.get('sortField');
const getSortDirection = state => state.puzzle.get('sortDirection');

export const filteredPuzzles = createSelector(
  getPuzzles, getType, getMaxLevel, getLevel, getNumberOfClues, getNumberOfSolutions, getConsistency,
  (puzzles, type, maxLevel, level, numberOfClues, numberOfSolutions, consistency) =>
    puzzles.reduce((acc, puzzle) => {
      if (type !== null && type !== puzzle.type) {
        return acc;
      }
      if (maxLevel !== null && maxLevel !== puzzle.maxdifflvl) {
        return acc;
      }
      if (level !== null && level !== puzzle.difflvl) {
        return acc;
      }
      if (numberOfClues !== null && numberOfClues !== puzzle.clues) {
        return acc;
      }
      if (numberOfSolutions !== null && numberOfSolutions !== puzzle.solutions) {
        return acc;
      }
      if (consistency !== null && consistency !== puzzle.consistency) {
        return acc;
      }
      return acc.unshift(puzzle);
    }, Immutable.List())
);

export const orderedPuzzles = createSelector(
  filteredPuzzles, getSortField, getSortDirection,
  (puzzles, sortField, sortDirection) =>
    puzzles.sort((a, b) => {
      if (sortDirection === 1) {
        switch (sortField) {
        case 'name': return a.name.localeCompare(b.name);
        case 'level':
          const levelFractionCompare = a.difflvl / a.maxdifflvl - b.difflvl / b.maxdifflvl;
          if (levelFractionCompare !== 0) return levelFractionCompare;
          const levelCompare = a.maxdifflvl - b.maxdifflvl;
          if (levelCompare !== 0) return levelCompare;
          return a.name.localeCompare(b.name);
        case 'source':
          const sourcecompare = a.source.localeCompare(b.source);
          if (sourcecompare === 0) return a.name.localeCompare(b.name);
          return sourcecompare;
        case 'clues':
          const cluecompare = a.clues - b.clues;
          if (cluecompare === 0) return a.name.localeCompare(b.name);
          return cluecompare;
        case 'solutions':
          const solutioncompare = a.solutions - b.solutions;
          if (solutioncompare === 0) return a.name.localeCompare(b.name);
          return solutioncompare;
        case 'consistency':
          const consistencycompare = consistencyOrder[a.consistency] - consistencyOrder[b.consistency];
          if (consistencycompare === 0) return a.name.localeCompare(b.name);
          return consistencycompare;
        default:
        }
      } else if (sortDirection === 2) {
        switch (sortField) {
        case 'name': return b.name.localeCompare(a.name);
        case 'level':
          const levelFractionCompare = b.difflvl / b.maxdifflvl - a.difflvl / a.maxdifflvl;
          if (levelFractionCompare !== 0) return levelFractionCompare;
          const levelCompare = b.maxdifflvl - a.maxdifflvl;
          if (levelCompare !== 0) return levelCompare;
          return a.name.localeCompare(b.name);
        case 'source':
          const sourcecompare = b.source.localeCompare(a.source);
          if (sourcecompare === 0) return a.name.localeCompare(b.name);
          return sourcecompare;
        case 'clues':
          const cluecompare = b.clues - a.clues;
          if (cluecompare === 0) return b.name.localeCompare(a.name);
          return cluecompare;
        case 'solutions':
          const solutioncompare = b.solutions - a.solutions;
          if (solutioncompare === 0) return a.name.localeCompare(b.name);
          return solutioncompare;
        case 'consistency':
          const consistencycompare = consistencyOrder[b.consistency] - consistencyOrder[a.consistency];
          if (consistencycompare === 0) return a.name.localeCompare(b.name);
          return consistencycompare;
        default:
        }
      }
      return 1;
    })
);
