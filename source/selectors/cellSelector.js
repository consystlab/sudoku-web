import { createSelector } from 'reselect';
import _ from 'lodash';

const getRowHovered = state => state.board.get('rowHovered');
const getColHovered = state => state.board.get('colHovered');
const getGroupHovered = state => state.board.get('groupHovered');
const getVariablesHovered = state => state.board.get('variablesHovered');

export const isHovered = createSelector(
  getRowHovered, getColHovered, getGroupHovered, getVariablesHovered,
  (rowHovered, colHovered, groupHovered, variablesHovered) =>
    _.range(0, 9).map(row =>
      _.range(0, 9).map(col => {
        if (colHovered === col) return true;
        if (rowHovered === row) return true;
        const group = (row - (row % 3)) + (col - (col % 3)) / 3;
        if (groupHovered === group) return true;
        if (variablesHovered.get(`${row}_${col}`)) return true;

        return false;
      })
    )
);
