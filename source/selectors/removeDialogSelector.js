import { createSelector } from 'reselect';

const getVariables = state => state.board.get('variables');

const getRow = state => state.controlPanel.get('removeDialogRow');
const getCol = state => state.controlPanel.get('removeDialogCol');

export const getVariable = createSelector(
  getVariables, getRow, getCol,
  (variables, row, col) => {
    if (row === null || col === null) return null;
    return variables[row][col];
  }
);
