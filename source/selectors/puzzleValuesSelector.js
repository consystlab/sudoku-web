import { createSelector } from 'reselect';
import getValueString from 'util/getValueString';

const getPuzzleValuesString = state => state.submitPuzzle.get('puzzleValues');
const getPuzzleValuesStringError = state => state.submitPuzzle.get('puzzleValuesError');
const getBoardValuesString = state => getValueString(state.board);

export const getRealPuzzleValueString = createSelector(
  getBoardValuesString, getPuzzleValuesString, getPuzzleValuesStringError,
  (boardValuesString, puzzleValuesString, puzzleValuesStringError) => {
    const puzzleString = puzzleValuesStringError ? puzzleValuesString : boardValuesString;
    return puzzleString;
  },
);

export const getRealNumberOfClues = createSelector(
  getRealPuzzleValueString,
  (puzzleValuesString) => puzzleValuesString.replace(/0/g, '').length,
);
