import BaseEnumeration from 'enumerations/BaseEnumeration';

const ControlPanelTabs = [
  'Load',
  'Solve',
  'Submit',
  'Upload',
  'Settings',
];

export default BaseEnumeration(ControlPanelTabs);
