const BaseEnumeration = values => {
  const enumeration = {};
  values.forEach((value, i) => {
    enumeration[value] = i;
    enumeration[i] = value;
  });
  return enumeration;
};

export default BaseEnumeration;
