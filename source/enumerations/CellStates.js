import BaseEnumeration from 'enumerations/BaseEnumeration';

const CellStates = [
  'Normal',
  'PuzzleLoaded',
  'Inconsistent',
];

export default BaseEnumeration(CellStates);
