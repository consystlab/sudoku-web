import BaseEnumeration from 'enumerations/BaseEnumeration';

const SearchCount = [
  'One',
  'All',
];

export default BaseEnumeration(SearchCount);
