import BaseEnumeration from 'enumerations/BaseEnumeration';

const DomainValueStates = [
  'Possible',
  'ToBeRemoved',
  'BeingRemoved',
  'Removed',
];

export default BaseEnumeration(DomainValueStates);
