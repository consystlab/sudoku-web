import BaseEnumeration from 'enumerations/BaseEnumeration';

const SubmitStates = [
  'NOTHING',
  'NO_NAME',
  'MIN_DIFF_ERROR',
  'DIFF_ERROR',
  'MAX_DIFF_ERROR',
  'DIFF_LESSER',
  'DIFF_GREATER',
  'INCONSISTENT_PUZZLE',
  'TOO_MANY_SOLUTIONS',
  'NAME_EXISTS',
  'PUZZLE_EXISTS',
  'PUZZLE_SUBMITTED',
  'PUZZLE_ACCEPTED',
  'CONTACT_SERVER_ERROR',
  'UNKNOWN_ERROR',
];

export default BaseEnumeration(SubmitStates);
